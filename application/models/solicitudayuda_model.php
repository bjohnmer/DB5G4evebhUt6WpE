<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Solicitudayuda_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	public function getAll($campo = null, $valor = null, $order = 'nro_solicitud_a', $torder = "DESC")
	{

		if (!empty($campo) && !empty($valor)) {
			$data = $this->db->join('tipo_ayuda','ayuda_solicitud.id_tipo = tipo_ayuda.id')
											->order_by($order, $torder)
											->where($campo,$valor)
											->get("ayuda_solicitud");
		}
		else
		{
			$data = $this->db->join('tipo_ayuda','ayuda_solicitud.id_tipo = tipo_ayuda.id')->order_by($order, $torder)->get("ayuda_solicitud");
		}

		if ($data->num_rows()) {
			return $data->result();			
		} else {
			return false;
		}

	}

	public function getMy($campo, $valor, $user)
	{
		$data = $this->db->join('tipo_ayuda','ayuda_solicitud.id_tipo = tipo_ayuda.id')->where($campo, $valor)->where("id_estudiante",$user)->get("ayuda_solicitud");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}

	public function getBy($campo, $valor)
	{
		$data = $this->db->join('tipo_ayuda','ayuda_solicitud.id_tipo = tipo_ayuda.id')->where($campo, $valor)->get("ayuda_solicitud");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}

	function maxNroSolicitudA()
	{
		$data = $this->db->order_by("nro_solicitud_a","DESC")->limit(1)->get("ayuda_solicitud");
		$resultado = $data->result(); 
		if ($data->num_rows()) {
			return $resultado[0]->nro_solicitud_a;
		}
		else
		{
			return false;
		}
	}
	
	public function create($data)
	{
		$correlativo = $this->maxNroSolicitudA() + 1;

		$data_solicitud = array (
							'nro_solicitud_a'	=> $correlativo,
							'id_estudiante'		=> $data['id_estudiante'],
							'id_tipo'					=> $data['id_tipo'],
							'motivo'					=> $data['motivo'],
							'fech_solicitud'	=> date("Y-m-d"),
						);
		$solicitud = $this->db->insert('ayuda_solicitud', $data_solicitud);
		
		if ($solicitud) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update($data,$id)
	{

		$solicitud = $this->db->where('id_solicitud_ayuda', $id)->update('ayuda_solicitud', $data);
		
		if ($solicitud) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function updateRecaudos($data)
	{

		$data_solicitud = array(
										'comprobante'		=> $data['comprobante'],
										'carta'				 	=> $data['carta'],
										'informe'				=> $data['informe'],
										'presupuestos'	=> $data['presupuestos'],
										'constancia'		=> $data['constancia'],
										'copiacedula'		=> $data['copiacedula'],
										'status'				=> "En Tránsito"
								);

		$solicitud = $this->db->where('id_solicitud_ayuda', $data['id_solicitud_ayuda'])->update('ayuda_solicitud', $data_solicitud);
		
		if ($solicitud) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	public function destroy($id)
	{
		$data = $this->db->where("id_solicitud_ayuda", $id)->delete("ayuda_solicitud");
		if ($data) {
			return true;
		} else {
			return false;			
		}
	}

}
