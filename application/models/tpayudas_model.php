<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tpayudas_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	public function isUnique($campo, $valor)
	{

		$data = $this->db->where($campo, $valor)->get("tipo_ayuda");

		if ($data->num_rows()) {
			return false;
		} else {
			return true;			
		}

	}

	public function getAll($order = 'descrip_ayuda', $torder = "ASC")
	{

		$data = $this->db->order_by($order, $torder)->get("tipo_ayuda");

		if ($data->num_rows()) {
			return $data->result();			
		} else {
			return false;
		}

	}

}
