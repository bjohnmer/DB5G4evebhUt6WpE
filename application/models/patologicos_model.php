<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Patologicos_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	public function isUnique($campo, $valor)
	{

		$data = $this->db->where($campo, $valor)->get("patologia");

		if ($data->num_rows()) {
			return false;
		} else {
			return true;			
		}

	}

	public function getBy($campo = 'id', $valor = null,$order = "descrip_patologia",$torder = "ASC")
	{

		$data = $this->db->where($campo,$valor)->order_by($order, $torder)->get("patologia");

		if ($data->num_rows()) {
			return $data->result();			
		} else {
			return false;
		}

	}

}
