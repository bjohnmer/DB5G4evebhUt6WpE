<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	public function isUnique($campo, $valor)
	{
		$data = $this->db->where($campo, $valor)->get("usuario");
		if ($data->num_rows()) {
			return false;
		} else {
			return true;			
		}
	}
	public function create($data)
	{

			$usuario = $this->db->insert('usuario', $data);
			
			if ($usuario) {
				return TRUE;
			} else {
				return FALSE;
			}
	}
	public function getBy($campo, $valor)
	{
		$data = $this->db->where($campo, $valor)->get("usuario");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}
}
