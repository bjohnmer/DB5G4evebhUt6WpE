<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autobus_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	public function isUnique($campo, $valor)
	{
		$data = $this->db->where($campo, $valor)->get("bus");
		if ($data->num_rows()) {
			return false;
		} else {
			return true;			
		}
	}
	public function getBy($campo, $valor)
	{
		$data = $this->db->where($campo, $valor)->get("bus");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}

	public function updateStatus()
	{

		$data = array('status' => "Asignado");

		$solicitudes = $this->db
										->select('id_bus')
										->where('(CURDATE() BETWEEN fech_salida AND fech_regreso)')
										->where('status',"Aprobado")
										->get("bus_solicitud");

		$arreglo = array("");
		foreach ($solicitudes->result() as $row)
		{
		  $arreglo[]=$row->id_bus;
		}

		$datos = $this->db
								->where_in('id',$arreglo)
								->update('bus', $data);

		$data = array('status' => "Disponible");

		$solicitudes = $this->db
										->distinct()
										->select('id_bus')
										->where('(CURDATE() > fech_regreso)')
										->where("status","Aprobado")
										->get("bus_solicitud");

		// print_r($solicitudes->result());
		$arreglo = array("");
		foreach ($solicitudes->result() as $row)
		{
		  $arreglo[]=$row->id_bus;
		}

		$datos = $this->db
								->where_in('id',$arreglo)
								->update('bus', $data);

		// $chofer = $this->db->where()->where('CURDATE() > fech_regreso')->update('bus_solicitud', $data);
		
		if ($datos) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

}
