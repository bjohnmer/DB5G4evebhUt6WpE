<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Academico_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	public function isUnique($campo, $valor)
	{
		$data = $this->db->where($campo, $valor)->get("estudiante");
		if ($data->num_rows()) {
			return false;
		} else {
			return true;			
		}
	}

	public function getBy($campo, $valor)
	{
		$data = $this->db->where($campo, $valor)->get("academico");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}

	public function create($data)
	{

			$usuario = $this->db->insert('academico', $data);
			
			if ($usuario) {
				return TRUE;
			} else {
				return FALSE;
			}
	}

	public function update($id,$data)
	{
		$edit = $this->db->where('id_estudiante', $id)->update('academico', $data);
		if ($edit) {
			return true;
		} else {
			return false;			
		}
	}

}
