<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Salud_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	public function isUnique($campo, $valor)
	{
		$data = $this->db->where($campo, $valor)->get("salud");
		if ($data->num_rows()) {
			return false;
		} else {
			return true;			
		}
	}

	public function getBy($campo, $valor)
	{
		$data = $this->db->where($campo, $valor)->get("salud");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}

	public function getByW($campos = array())
	{

		$data = $this->db->where($campos)->join('patologia','salud.id_patologia = patologia.id')->get("salud");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}

	public function getDistinctByW($campos = array())
	{

		$data = $this->db->select('id_patologia')->distinct('id_patologia')->join('patologia','salud.id_patologia = patologia.id')->where($campos)->get("salud");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}


	public function create($data)
	{

			$usuario = $this->db->insert('salud', $data);
			
			if ($usuario) {
				return TRUE;
			} else {
				return FALSE;
			}
	}

	public function create_batch($data)
	{

			$usuario = $this->db->insert_batch('salud', $data);
			
			if ($usuario) {
				return TRUE;
			} else {
				return FALSE;
			}
	}


	public function update($id,$data)
	{
		$edit = $this->db->where('id_estudiante', $id)->update('salud', $data);
		if ($edit) {
			return true;
		} else {
			return false;			
		}
	}

	public function destroyAllBy($campo, $valor)
	{
		$edit = $this->db->where($campo, $valor)->delete('salud');
		if ($edit) {
			return true;
		} else {
			return false;			
		}
	}

	

}
