<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ctabalance_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	public function isUnique($campo, $valor)
	{

		$data = $this->db->where($campo, $valor)->get("cuentas_balance");

		if ($data->num_rows()) {
			return false;
		} else {
			return true;			
		}

	}
}
