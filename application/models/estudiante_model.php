<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estudiante_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	public function isUnique($campo, $valor)
	{
		$data = $this->db->where($campo, $valor)->get("estudiante");
		if ($data->num_rows()) {
			return false;
		} else {
			return true;			
		}
	}

	public function getBy($campo, $valor)
	{
		$data = $this->db->where($campo, $valor)->get("estudiante");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}

	public function getWhere($data = array())
	{
		$data = $this->db->where($data)->get("estudiante");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}
	
	public function update($id,$data)
	{
		$edit = $this->db->where('id', $id)->update('estudiante', $data);
		if ($edit) {
			return true;
		} else {
			return false;			
		}
	}
}
