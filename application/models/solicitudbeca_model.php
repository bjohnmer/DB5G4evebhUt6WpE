<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Solicitudbeca_model extends CI_Model {

	function __construct(){
		parent::__construct();
	}

	public function getAll($campo = null, $valor = null, $order = 'nro_solicitud_b', $torder = "DESC")
	{

		if (!empty($campo) && !empty($valor)) {
			$data = $this->db->join('tipo_becas','beca_solicitud.id_tipo = tipo_becas.id')
											->order_by($order, $torder)
											->where($campo,$valor)
											->get("beca_solicitud");
		}
		else
		{
			$data = $this->db->join('tipo_becas','beca_solicitud.id_tipo = tipo_becas.id')->order_by($order, $torder)->get("beca_solicitud");
		}

		if ($data->num_rows()) {
			return $data->result();			
		} else {
			return false;
		}

	}

	public function getMy($campo, $valor, $user)
	{
		$data = $this->db->join('tipo_becas','beca_solicitud.id_tipo = tipo_becas.id')->where($campo, $valor)->where("id_estudiante",$user)->get("beca_solicitud");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}

	public function getBy($campo, $valor)
	{
		$data = $this->db->join('tipo_becas','beca_solicitud.id_tipo = tipo_becas.id')->where($campo, $valor)->get("beca_solicitud");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}

	public function getWhere($data = array())
	{
		$data = $this->db->join('tipo_becas','beca_solicitud.id_tipo = tipo_becas.id')->where($data)->get("beca_solicitud");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}

	public function maxNroSolicitudB()
	{
		$data = $this->db->order_by("nro_solicitud_b","DESC")->limit(1)->get("beca_solicitud");
		$resultado = $data->result(); 
		if ($data->num_rows()) {
			return $resultado[0]->nro_solicitud_b;
		}
		else
		{
			return false;
		}
	}
	
	public function create($data)
	{
		
		$solicitud = $this->db->insert('beca_solicitud', $data);
		
		if ($solicitud) {
			return TRUE;
		} else {
			return FALSE;
		}
	}

	public function update($data,$id)
	{

		$solicitud = $this->db->where('id_solicitud_b', $id)->update('beca_solicitud', $data);
		
		if ($solicitud) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
	
	public function suspender($id,$data)
	{

		$solicitud = $this->db->where('id_solicitud_b', $id)->update('beca_solicitud', $data);
		
		if ($solicitud) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	public function updateRecaudos($data)
	{

		$solicitud = $this->db->where('id_solicitud_b', $data['id_solicitud_b'])->update('beca_solicitud', $data);
		
		if ($solicitud) {
			return TRUE;
		} else {
			return FALSE;
		}
	}


	public function destroy($id)
	{
		$data = $this->db->where("id_solicitud_b", $id)->delete("beca_solicitud");
		if ($data) {
			return true;
		} else {
			return false;			
		}
	}

}
