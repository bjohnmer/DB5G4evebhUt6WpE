<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Psicosocial_model extends CI_Model {
	function __construct(){
		parent::__construct();
	}
	public function isUnique($campo, $valor)
	{
		$data = $this->db->where($campo, $valor)->get("psicosocial");
		if ($data->num_rows()) {
			return false;
		} else {
			return true;			
		}
	}

	public function getBy($campo, $valor)
	{
		$data = $this->db->where($campo, $valor)->get("psicosocial");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}

	public function getByW($campos = array())
	{

		$data = $this->db->where($campos)->get("psicosocial");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}

	public function getDistinctByW($campos = array())
	{

		$data = $this->db->select('parentesco')->distinct('parentesco')->where($campos)->get("psicosocial");
		if ($data->num_rows()) {
			return $data->result();
		} else {
			return false;			
		}
	}


	public function create($data)
	{

			$usuario = $this->db->insert('psicosocial', $data);
			
			if ($usuario) {
				return TRUE;
			} else {
				return FALSE;
			}
	}

	public function create_batch($data)
	{

			$usuario = $this->db->insert_batch('psicosocial', $data);
			
			if ($usuario) {
				return TRUE;
			} else {
				return FALSE;
			}
	}


	public function update($id,$data)
	{
		$edit = $this->db->where('id_estudiante', $id)->update('psicosocial', $data);
		if ($edit) {
			return true;
		} else {
			return false;			
		}
	}

	public function destroyAllBy($campo, $valor)
	{
		$edit = $this->db->where($campo, $valor)->delete('psicosocial');
		if ($edit) {
			return true;
		} else {
			return false;			
		}
	}

	

}
