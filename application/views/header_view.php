<!doctype html>
<html lang="es">
<head>
  
  <meta charset="UTF-8">
  <title>.:. SIBE .:.</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="author" href="http://tinyurl.com/qdg7ajv">
  <?php if (!empty($css_files)): ?>
    <?php foreach($css_files as $file): ?>
      <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
  <?php endif ?>
  <link rel="stylesheet" href="<?=base_url()?>jquery/themes/base/jquery.ui.all.css">
  <link rel="stylesheet" href="<?=base_url()?>css/bootstrap.min.css">
  <link rel="stylesheet" href="<?=base_url()?>css/estilos.css">

  <link rel="stylesheet" href="<?=base_url()?>css/chosen/chosen/chosen.css" />
  <link rel="stylesheet" href="<?=base_url()?>css/themes/flexigrid/css/flexigrid.css">

  <?php if (!empty($css_estilos)): ?>
    <?php foreach ($css_estilos as $css): ?>
      <link rel="stylesheet" href="<?=$css?>">
    <?php endforeach ?>
  <?php endif ?>
  <style>
    #form-reg label, #form-reg input { display:block; }
    #form-reg input.text { margin-bottom:12px; width:95%; padding: .4em; }
    #form-reg select { margin-bottom:12px; width:95%; padding: .4em; }
    #form-reg fieldset { padding:0; border:0; margin-top:25px; }
    #form-reg .ui-dialog .ui-state-error { padding: .3em; }
    #form-reg .validateTips { border: 1px solid transparent; padding: 0.3em; }
    /* Acordeon */
    .menu_body a{
      display:block;
      color:#006699;
      background-color:#fcfdfd;
      padding-left:10px;
      font-weight:bold;
      text-decoration:none;
    }
    .menu_body a:hover{
      color: #ffffff;
      background-color:#5c9ccc;
      text-decoration:none;
      }
    .menu_body hr{
      margin:5px;
      padding:0px;
      size="1";
      /*color="#4758a8";*/
      color="#5c9ccc";
      width="100%";
      align="center";
    }
  </style>

</head>
<body>
  <div id="main-container">
    <header><img src="<?=base_url()?>img/sibe_header.jpg" alt=""></header>
