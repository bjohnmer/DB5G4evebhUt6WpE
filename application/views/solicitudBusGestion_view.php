<?php
  $URL_PRIN = $this->urlprin->getUrl();
?>
    <div id="info-contenido">
        <!-- <div width="150" border="0" cellpadding="0" cellspacing="0" bgcolor="#4758a8"> -->
      <div id="datos" class="fila">
        <div id="menu" class="col">
          <?php $this->load->view("menu_view"); ?>
        </div>
        <div id="info" class="col">
          <?php if (!empty($titulo)): ?>
            <h1>
            <?=$titulo?>
            </h1>
          <?php endif ?>
          
          <div class="flexigrid crud-form" style="width: 100%;">  
            <div class="mDiv">
              <div class="ftitle">
                <div class="ftitle-left">
                  Gestión de Solicitud
                </div>      
                <div class="clear"></div>
              </div>
            </div>
            <div id="main-table-box">
              <?php if (!empty($solicitud)): ?>
                <form action="<?=$URL_PRIN?>solicitudBus/updGestionar" method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data" accept-charset="utf-8">
                  <div class="form-div">
                    <div class="form-field-box odd" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Solicitud N°:
                      </div>
                      <div>
                        <input type="text" name="nro_solicitud_t" id="nro_solicitud_a" value="<?=$solicitud[0]->nro_solicitud_t?>" disabled>
                      </div>
                    </div>
                    <div class="form-field-box even" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Emitida en Fecha:
                      </div>
                      <div>
                        <input type="text" name="fech_solicitud" id="fech_solicitud" value="<?=$solicitud[0]->fech_solicitud?>" disabled>
                      </div>
                    </div>
                    <div class="form-field-box odd" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Cédula :
                      </div>
                      <div>
                        <input type="text" name="ced_estudiante" id="ced_estudiante" value="<?=$estudiante[0]->ced_estudiante?>" disabled>
                      </div>
                    </div>
                    <div class="form-field-box even" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Apellidos :
                      </div>
                      <div>
                        <input type="text" name="apellidos" id="apellidos" value="<?=$estudiante[0]->apellidos?>" disabled>
                      </div>
                    </div>
                    <div class="form-field-box odd" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Nombres :
                      </div>
                      <div>
                        <input type="text" name="nombres" id="nombres" value="<?=$estudiante[0]->nombres?>" disabled>
                      </div>
                    </div>
                    <div class="form-field-box even" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Organización :
                      </div>
                      <div>
                        <input type="text" name="organizacion" id="organizacion" value="<?=$solicitud[0]->organizacion?>" disabled>
                      </div>
                    </div>
                    <div class="form-field-box odd" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Ruta :
                      </div>
                      <div>
                        <input type="text" name="ruta_destino" id="ruta_destino" value="<?=$solicitud[0]->ruta_destino?>" disabled>
                      </div>
                    </div>
                    <div class="form-field-box even" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Motivo :
                      </div>
                      <div>
                        <textarea name="motivo" id="motivo" cols="30" rows="10" disabled><?=$solicitud[0]->motivo?></textarea>
                      </div>
                    </div>
                    <div class="form-field-box odd" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Fecha de Salida :
                      </div>
                      <div>
                        <input type="text" name="fech_salida" id="fech_salida" value="<?=$solicitud[0]->fech_salida?>" disabled>
                      </div>
                    </div>
                    <div class="form-field-box even" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Hora de Salida :
                      </div>
                      <div>
                        <input type="text" name="hora_salida" id="hora_salida" value="<?=$solicitud[0]->hora_salida?>" disabled>
                      </div>
                    </div>
                    <div class="form-field-box odd" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Fecha de Regreso :
                      </div>
                      <div>
                        <input type="text" name="fech_regreso" id="fech_regreso" value="<?=$solicitud[0]->fech_regreso?>" disabled>
                      </div>
                    </div>
                  </div>
                  <div class="form-div">
                    <div class="form-field-box even" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Estado de la Solicitud :
                      </div>
                      <div>
                        <input type="radio" name="status" id="statusA" value="Aprobado"> Aprobado &nbsp;&nbsp;
                        <input type="radio" name="status" id="statusR" value="Rechazado"> Rechazado
                      </div>
                    </div>
                  </div>

                  <div id="camposA" class="form-div hidden">
                    <div class="form-field-box odd" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Bus :
                      </div>
                      <div>
                      <select name="id_bus" id="id_bus" placeholder="Seleccione un Bus" class="chzn-select <?php if (form_error('id_tipo')): ?>field_error<?php endif ?>" style="width:350px;" tabindex="2" >
                        <?php if (!empty($buses)): ?>
                          <?php foreach ($buses as $bus): ?>
                            <option value="<?=$bus->id?>"><?=$bus->placa?> <?=$bus->marca?> <?=$bus->modelo?> <?=$bus->puestos?> puestos </option> 
                          <?php endforeach ?>
                        <?php endif ?>
                      </select>
                    </div>
                    </div>
                    <div class="form-field-box even" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Chofer :
                      </div>
                      <div>
                        <div>
                          <select name="id_chofer" id="id_chofer" placeholder="Seleccione un Chofer" class="chzn-select <?php if (form_error('id_tipo')): ?>field_error<?php endif ?>" style="width:350px;" tabindex="2" >
                            <?php if (!empty($choferes)): ?>
                              <?php foreach ($choferes as $chofer): ?>
                                <option value="<?=$chofer->id?>"><?=$chofer->ced_chofer?> <?=$chofer->nombre?> <?=$chofer->apellido?></option> 
                              <?php endforeach ?>
                            <?php endif ?>
                          </select>
                        </div>
                      </div>
                    </div>
                  </div>
                  <?php if (validation_errors()): ?>
                    <div id="report-error" class="report-div error" style="display:block;">
                      <?=validation_errors()?>
                    </div>
                  <?php endif ?>
                  <div class="pDiv">
                    <div class="form-button-box">
                      <input type="submit" value="Guardar" class="btn btn-large">
                    </div>        
                    <div class="form-button-box">
                      <input type="button" value="Cancelar" onclick="window.location='<?=$URL_PRIN?>solicitudBus'" class="btn btn-large">
                    </div>
                          
                    <div class="clear"></div> 
                  </div>
                  <input type="hidden" name="id_bus_solicitud" value="<?=$solicitud[0]->id_bus_solicitud?>">
                </form>
              <?php else: ?>
                <h3>El registro no fué encontrado</h3>
              <?php endif ?>
            </div>
              
          </div>
        </div>
      </div>
    </div>
  