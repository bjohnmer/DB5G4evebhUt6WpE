<div id="info-contenido">
        <!-- <div width="150" border="0" cellpadding="0" cellspacing="0" bgcolor="#4758a8"> -->
      <div id="datos" class="fila">
        <div id="menu" class="col">
          <?php $this->load->view("menu_view"); ?>
        </div>
        <div align="center" id="info" class="col">
          <?php if (validation_errors()): ?>
            <div id="report-error" class="report-div error" style="display:block;">
              <?=validation_errors()?>
            </div>
          <?php endif ?>
          
          <?php if (!empty($mensaje['tipo'])): ?>
            <?php if ($mensaje['tipo'] == "success"): ?>
              <div id="report-success" class="report-div success" style="display:block;">
                <p>
                  <?=$mensaje['mensaje']?>
                </p>
              </div>
            <?php else: ?>
              <div id="report-error" class="report-div error" style="display:block;">
                <p>
                  <?=$mensaje['mensaje']?>
                </p>
              </div>
            <?php endif ?>
          <?php endif ?>
          <!-- inicio Form -->
          <form id="frmcenso" name="frmcenso" method="post" action= "<?=base_url()?>censo/insert">
            <h1>Censo</h1>
            <?php if (!empty($consultar)): ?>
              <a class="btn btn-primary" href="<?=base_url()?>estudiante">Atrás</a>
            <?php endif ?>
            <table width="1000" border="0" cellspacing="0" cellpadding="0">
              <tr>
                <td colspan="6">&nbsp;</td>
              </tr>
              <tr>
                <td colspan="6">
                  <div align="center" id="tabs"><!-- Inicio Tabs -->
                    <ul>
                      <li><a href="#tabs-1">Datos Personales</a></li>
                      <li><a href="#tabs-2">Datos de Dirección</a></li>
                      <li><a href="#tabs-3">Datos de Emergencia</a></li> 
                      <li><a href="#tabs-4">Datos Academicos</a></li>
                      <li><a href="#tabs-5">Datos SocioEconomicos</a></li>
                      <li><a href="#tabs-6">Datos PsicoSociales</a></li>
                    </ul>

<!-- Datos Personales -->
                  <div align="center" id="tabs-1">
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          Género: <br>
                          <select data-placeholder="Seleccione un género" name="genero" id="genero" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option <?php if ($estudiante[0]->genero == "Femenino") { echo "selected";} ?> value="Femenino">Femenino</option>
                            <option <?php if ($estudiante[0]->genero == "Masculino") { echo "selected";} ?> value="Masculino">Masculino</option>
                          </select>
                        </td>
                        <td>&nbsp;</td>
                        <td>
                          Edad: <br>
                          <input type="text" name="edad" id="edad" class="text ui-widget-content ui-corner-all" value = "<?=$estudiante[0]->edad?>">
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          Estado Civil: <br>
                          <select data-placeholder="Seleccione un estado civil" name="edocivil" id="edocivil" class="chzn-select">
                            <option></option>
                            <option value="Soltero" <?php if ($estudiante[0]->edocivil == "Soltero") { echo "selected";} ?>>Soltero</option>
                            <option value="Casado" <?php if ($estudiante[0]->edocivil == "Casado") { echo "selected";} ?>>Casado</option>
                            <option value="Concubinato" <?php if ($estudiante[0]->edocivil == "Concubinato") { echo "selected";} ?>>Concubinato</option>
                            <option value="Divorciado" <?php if ($estudiante[0]->edocivil == "Divorciado") { echo "selected";} ?>>Divorciado</option>
                            <option value="Viudo" <?php if ($estudiante[0]->edocivil == "Viudo") { echo "selected";} ?>>Viudo</option>
                          </select>
                        </td>
                        <td colspan="2">
                          Lugar de Nacimiento: <br>
                          <input name="lugnac" type="text" id="lugnac" size="81" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->lugnac?>">
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          Celular: <br>
                          <input name="celular" type="text" id="celular" size="81" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->celular?>">
                        </td>
                        <td colspan="2">
                          Correo Electrónico: <br>
                          <input name="email" type="text" id="email" size="81" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->email?>">
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          Twitter: <br>
                          <input name="twitter" type="text" id="twitter" size="50" placeholder="@esttudiante" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->twitter?>">
                        </td>
                        <td colspan="2">
                          Facebook: <br>
                          <input name="facebook" type="text" id="facebook" size="50" placeholder="www.facebook.com/estudiante" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->facebook?>">
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Estatura: <br>
                          <input type="text" name="estatura" id="estatura" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->estatura?>" placeholder="1.75">
                        </td>
                        <td>
                          Peso: <br>
                          <input type="text" name="peso" id="peso" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->peso?>" placeholder="70">
                        </td>
                        <td colspan="2">
                          Tallas: <br>
                          <input type="text" name="talla" id="talla" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->talla?>" placeholder="1.75">
                        </td>
                      </tr>
                      <tr>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                    </table>
                  </div>
                  
<!-- Datos de Dirección -->
                  <div align="center" id="tabs-2">
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="4">
                          <strong style="color:#006699">Direccion de Habitacion</strong>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Estado: <br>
                          <select data-placeholder="Estado" name="drhab_edo" id="drhab_edo" style="width:95%" class="">
                            <option></option>
                            <?php if (!empty($estados)): ?>
                              <?php foreach ($estados as $estado): ?>
                                <option value="<?=$estado->id_estado?>" <?php if ($estudiante[0]->drhab_edo == $estado->id_estado) { echo "selected";} ?>><?=$estado->nombre_estado?></option>
                              <?php endforeach ?>
                            <?php endif ?>
                          </select>
                        </td>
                        <td>
                          Municipio: <br>
                          <select data-placeholder="Municipio" name="drhab_mun" id="drhab_mun" style="width:94%" class="">
                            <option></option>
                            <?php if (!empty($municipios_hab)): ?>
                              <?php foreach ($municipios_hab as $municipio): ?>
                                <option value="<?=$municipio->id_municipio?>" <?php if ($estudiante[0]->drhab_mun == $municipio->id_municipio) { echo "selected";} ?>><?=$municipio->nombre_municipio?></option>
                             <?php endforeach ?>
                            <?php endif ?>
                          </select>
                        </td>
                        <td>
                          Parroquia: <br>
                          <select data-placeholder="Parroquia" name="drhab_parr" id="drhab_parr" style="width:95%" class="">
                            <option></option>
                            <?php if (!empty($parroquias_hab)): ?>
                              <?php foreach ($parroquias_hab as $parroquia): ?>
                                <option value="<?=$parroquia->id_parroquia?>" <?php if ($estudiante[0]->drhab_parr == $parroquia->id_parroquia) { echo "selected";} ?>><?=$parroquia->nombre_parroquia?></option>
                             <?php endforeach ?>
                            <?php endif ?>
                          </select>
                        </td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>
                          <select data-placeholder="Opción de dirección" name="drhab_tipo1" id="drhab_tipo1" class="chzn-select">
                            <option></option>
                            <option value="Urbanización" <?php if ($estudiante[0]->drhab_tipo1 == "Urbanización") { echo "selected";} ?>>Urbanización</option>
                            <option value="Conjunto Residencial" <?php if ($estudiante[0]->drhab_tipo1 == "Conjunto Residencial") { echo "selected";} ?>>Conjunto Residencial</option>
                            <option value="Barrio" <?php if ($estudiante[0]->drhab_tipo1 == "Barrio") { echo "selected";} ?>>Barrio</option>
                            <option value="Sector" <?php if ($estudiante[0]->drhab_tipo1 == "Sector") { echo "selected";} ?>>Sector</option>
                          </select>
                          <input type="text" name="drhab_tipo1_desc" id="drhab_tipo1_desc" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->drhab_tipo1_desc?>">
                        </td>
                        <td>
                          <select data-placeholder="Opción de dirección" name="drhab_tipo2" id="drhab_tipo2" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Avenida" <?php if ($estudiante[0]->drhab_tipo2 == "Avenida") { echo "selected";} ?>>Avenida</option>
                            <option value="Calle" <?php if ($estudiante[0]->drhab_tipo2 == "Calle") { echo "selected";} ?>>Calle</option>
                            <option value="Carrera" <?php if ($estudiante[0]->drhab_tipo2 == "Carrera") { echo "selected";} ?>>Carrera</option>
                            <option value="Transversal" <?php if ($estudiante[0]->drhab_tipo2 == "Transversal") { echo "selected";} ?>>Transversal</option>
                            <option value="Prolongación" <?php if ($estudiante[0]->drhab_tipo2 == "Prolongación") { echo "selected";} ?>>Prolongación</option>
                            <option value="Carretera" <?php if ($estudiante[0]->drhab_tipo2 == "Carretera") { echo "selected";} ?>>Carretera</option>
                            <option value="Callejon" <?php if ($estudiante[0]->drhab_tipo2 == "Callejon") { echo "selected";} ?>>Callejon</option>
                            <option value="Pasaje" <?php if ($estudiante[0]->drhab_tipo2 == "Pasaje") { echo "selected";} ?>>Pasaje</option>
                            <option value="Boulevar" <?php if ($estudiante[0]->drhab_tipo2 == "Boulevar") { echo "selected";} ?>>Boulevar</option>
                            <option value="Vereda" <?php if ($estudiante[0]->drhab_tipo2 == "Vereda") { echo "selected";} ?>>Vereda</option>
                            <option value="Escalera" <?php if ($estudiante[0]->drhab_tipo2 == "Escalera") { echo "selected";} ?>>Escalera</option>
                            <option value="Sendero" <?php if ($estudiante[0]->drhab_tipo2 == "Sendero") { echo "selected";} ?>>Sendero</option>
                            <option value="Troncal" <?php if ($estudiante[0]->drhab_tipo2 == "Troncal") { echo "selected";} ?>>Troncal</option>
                            <option value="Camino" <?php if ($estudiante[0]->drhab_tipo2 == "Camino") { echo "selected";} ?>>Camino</option>
                          </select>
                          <input type="text" name="drhab_tipo2_desc" id="drhab_tipo2_desc" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->drhab_tipo2_desc?>">
                        </td>
                        <td>
                          <select data-placeholder="Tipo de Habitación" name="drhab_tipo3" id="drhab_tipo3" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Edificio" <?php if ($estudiante[0]->drhab_tipo3 == "Edificio") { echo "selected";} ?>>Edificio</option>
                            <option value="Apartamento" <?php if ($estudiante[0]->drhab_tipo3 == "Apartamento") { echo "selected";} ?>>Apartamento</option>
                            <option value="Quinta" <?php if ($estudiante[0]->drhab_tipo3 == "Quinta") { echo "selected";} ?>>Quinta</option>
                            <option value="Casa" <?php if ($estudiante[0]->drhab_tipo3 == "Casa") { echo "selected";} ?>>Casa</option>
                            <option value="Rancho" <?php if ($estudiante[0]->drhab_tipo3 == "Rancho") { echo "selected";} ?>>Rancho</option>
                            <option value="Cento Comercial" <?php if ($estudiante[0]->drhab_tipo3 == "Cento Comercial") { echo "selected";} ?>>Cento Comercial</option>
                            <option value="Local Comercial" <?php if ($estudiante[0]->drhab_tipo3 == "Local Comercial") { echo "selected";} ?>>Local Comercial</option>
                            <option value="Oficina" <?php if ($estudiante[0]->drhab_tipo3 == "Oficina") { echo "selected";} ?>>Oficina</option>
                          </select>
                          <input type="text" name="drhab_tipo3_desc" id="drhab_tipo3_desc" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->drhab_tipo3_desc?>">
                        </td>
                        <td>&nbsp;</td>
                      </tr>
                      <tr>
                          <td>
                            Ciudad/Localidad: <br>
                            <input name="drhab_ciudad" type="text" id="drhab_ciudad" size="79" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->drhab_ciudad?>">
                          </td>
                          <td>
                            Telefono: <br>
                            <input type="text" name="telef_hab" id="telef_hab" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->telef_hab?>">
                          </td>
                          <td>
                          Condición de Habitación: <br>
                          <select data-placeholder="Condición de la Vivienda" name="cond_habitacion" id="cond_habitacion" style="width:95%" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Optimas condiciones, en condiciones de lujo" <?php if ($estudiante[0]->cond_habitacion == "Optimas condiciones, en condiciones de lujo") { echo "selected";} ?>>Optimas condiciones, en condiciones de lujo</option>
                            <option value="Optimas condiciones Sanitarias, en ambientes sin exceso de lujo" <?php if ($estudiante[0]->cond_habitacion == "Optimas condiciones Sanitarias, en ambientes sin exceso de lujo") { echo "selected";} ?>>Optimas condiciones Sanitarias, en ambientes sin exceso de lujo</option>
                            <option value="Buenas condiciones en ambiente reducidos" <?php if ($estudiante[0]->cond_habitacion == "Buenas condiciones en ambiente reducidos") { echo "selected";} ?>>Buenas condiciones en ambiente reducidos</option>
                            <option value="Ambientes y espacios insuficientes, deficiencia en el area sanitaria" <?php if ($estudiante[0]->cond_habitacion == "Ambientes y espacios insuficientes, deficiencia en el area sanitaria") { echo "selected";} ?>>Ambientes y espacios insuficientes, deficiencia en el area sanitaria</option>
                            <option value="Ambientes insuficientes y condiciones sanitarias marcadamente inadecuadas" <?php if ($estudiante[0]->cond_habitacion == "Ambientes insuficientes y condiciones sanitarias marcadamente inadecuadas") { echo "selected";} ?>>Ambientes insuficientes y condiciones sanitarias marcadamente inadecuadas</option>
                          </select></td>
                        </tr>
                      <tr>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="4"><strong style="color:#006699">Direccion de Residencia</strong></td>
                      </tr>
                      <tr>
                        <td>
                          Estado: <br>
                          <select data-placeholder="Estado" name="drres_edo" id="drres_edo" style="width:95%" class="">
                            <?php if (!empty($estados)): ?>
                              <?php foreach ($estados as $estado): ?>
                                <option value="<?=$estado->id_estado?>" <?php if ($estudiante[0]->drres_edo == $estado->id_estado) { echo "selected";} ?>><?=$estado->nombre_estado?></option>
                              <?php endforeach ?>
                            <?php endif ?>
                          </select>
                        </td>
                        <td>
                          Municipio: <br>
                          <select data-placeholder="Municipio" name="drres_mun" id="drres_mun" style="width:94%" class="">
                            <option></option>
                            <?php if (!empty($municipios_res)): ?>
                              <?php foreach ($municipios_res as $municipio): ?>
                                <option value="<?=$municipio->id_municipio?>" <?php if ($estudiante[0]->drres_mun == $municipio->id_municipio) { echo "selected";} ?>><?=$municipio->nombre_municipio?></option>
                              <?php endforeach ?>
                            <?php endif ?>
                          </select>
                        </td>
                        <td>
                          Parroquia: <br>
                          <select data-placeholder="Parroquia" name="drres_parr" id="drres_parr" style="width:95%" class="">
                            <option></option>
                            <?php if (!empty($parroquias_res)): ?>
                              <?php foreach ($parroquias_res as $parroquia): ?>
                                <option value="<?=$parroquia->id_parroquia?>" <?php if ($estudiante[0]->drres_parr == $parroquia->id_parroquia) { echo "selected";} ?>><?=$parroquia->nombre_parroquia?></option>
                              <?php endforeach ?>
                            <?php endif ?>
                          </select>
                        </td>
                        <td></td>
                      </tr>
                      <tr>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                      <tr>
                        <td>
                          <select data-placeholder="Opción de dirección" name="drres_tipo1" id="drres_tipo1" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Urbanización" <?php if ($estudiante[0]->drres_tipo1 == "Urbanización") { echo "selected";} ?>>Urbanización</option>
                            <option value="Conjunto Residencial" <?php if ($estudiante[0]->drres_tipo1 == "Conjunto Residencial") { echo "selected";} ?>>Conjunto Residencial</option>
                            <option value="Barrio" <?php if ($estudiante[0]->drres_tipo1 == "Barrio") { echo "selected";} ?>>Barrio</option>
                            <option value="Sector" <?php if ($estudiante[0]->drres_tipo1 == "Sector") { echo "selected";} ?>>Sector</option>
                          </select>
                          <input type="text" name="drres_tipo1_desc" id="drres_tipo1_desc" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->drres_tipo1_desc?>">
                        </td>
                        <td>
                          <select data-placeholder="Opción de dirección" name="drres_tipo2" id="drres_tipo2" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Avenida" <?php if ($estudiante[0]->drres_tipo2 == "Avenida") { echo "selected";} ?>>Avenida</option>
                            <option value="Calle" <?php if ($estudiante[0]->drres_tipo2 == "Calle") { echo "selected";} ?>>Calle</option>
                            <option value="Carrera" <?php if ($estudiante[0]->drres_tipo2 == "Carrera") { echo "selected";} ?>>Carrera</option>
                            <option value="Transversal" <?php if ($estudiante[0]->drres_tipo2 == "Transversal") { echo "selected";} ?>>Transversal</option>
                            <option value="Prolongación" <?php if ($estudiante[0]->drres_tipo2 == "Prolongación") { echo "selected";} ?>>Prolongación</option>
                            <option value="Carretera" <?php if ($estudiante[0]->drres_tipo2 == "Carretera") { echo "selected";} ?>>Carretera</option>
                            <option value="Callejon" <?php if ($estudiante[0]->drres_tipo2 == "Callejon") { echo "selected";} ?>>Callejon</option>
                            <option value="Pasaje" <?php if ($estudiante[0]->drres_tipo2 == "Pasaje") { echo "selected";} ?>>Pasaje</option>
                            <option value="Boulevar" <?php if ($estudiante[0]->drres_tipo2 == "Boulevar") { echo "selected";} ?>>Boulevar</option>
                            <option value="Vereda" <?php if ($estudiante[0]->drres_tipo2 == "Vereda") { echo "selected";} ?>>Vereda</option>
                            <option value="Escalera" <?php if ($estudiante[0]->drres_tipo2 == "Escalera") { echo "selected";} ?>>Escalera</option>
                            <option value="Sendero" <?php if ($estudiante[0]->drres_tipo2 == "Sendero") { echo "selected";} ?>>Sendero</option>
                            <option value="Troncal" <?php if ($estudiante[0]->drres_tipo2 == "Troncal") { echo "selected";} ?>>Troncal</option>
                            <option value="Camino" <?php if ($estudiante[0]->drres_tipo2 == "Camino") { echo "selected";} ?>>Camino</option>
                          </select>
                          <input type="text" name="drres_tipo2_desc" id="drres_tipo2_desc" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->drres_tipo2_desc?>">
                        </td>
                        <td>
                          <select data-placeholder="Tipo de Habitación" name="drres_tipo3" id="drres_tipo3" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Edificio" <?php if ($estudiante[0]->drres_tipo3 == "Edificio") { echo "selected";} ?>>Edificio</option>
                            <option value="Apartamento" <?php if ($estudiante[0]->drres_tipo3 == "Apartamento") { echo "selected";} ?>>Apartamento</option>
                            <option value="Quinta" <?php if ($estudiante[0]->drres_tipo3 == "Quinta") { echo "selected";} ?>>Quinta</option>
                            <option value="Casa" <?php if ($estudiante[0]->drres_tipo3 == "Casa") { echo "selected";} ?>>Casa</option>
                            <option value="Rancho" <?php if ($estudiante[0]->drres_tipo3 == "Rancho") { echo "selected";} ?>>Rancho</option>
                            <option value="Cento Comercial" <?php if ($estudiante[0]->drres_tipo3 == "Cento Comercial") { echo "selected";} ?>>Cento Comercial</option>
                            <option value="Local Comercial" <?php if ($estudiante[0]->drres_tipo3 == "Local Comercial") { echo "selected";} ?>>Local Comercial</option>
                            <option value="Oficina" <?php if ($estudiante[0]->drres_tipo3 == "Oficina") { echo "selected";} ?>>Oficina</option>
                          </select>
                          <input type="text" name="drres_tipo3_desc" id="drres_tipo3_desc" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->drres_tipo3_desc?>">
                        </td>
                        <td></td>
                      </tr>
                      <tr>
                        <td>
                          Ciudad/Localidad: <br>
                          <input name="drres_ciudad" type="text" id="drres_ciudad" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->drres_ciudad?>">
                        </td>
                        <td>
                          Teléfono: <br>
                          <input type="text" name="telef_res" id="telef_res" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->telef_res?>">
                        </td>
                        <td>
                        Condición de Residencia: <br />
                        <select data-placeholder="Condición de la Vivienda" name="cond_residencia" id="cond_residencia" style="width:95%" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Optimas condiciones, en condiciones de lujo" <?php if ($estudiante[0]->cond_residencia == "Optimas condiciones, en condiciones de lujo") { echo "selected";} ?>>Optimas condiciones, en condiciones de lujo</option>
                            <option value="Optimas condiciones Sanitarias, en ambientes sin exceso de lujo" <?php if ($estudiante[0]->cond_residencia == "Optimas condiciones Sanitarias, en ambientes sin exceso de lujo") { echo "selected";} ?>>Optimas condiciones Sanitarias, en ambientes sin exceso de lujo</option>
                            <option value="Buenas condiciones en ambiente reducidos" <?php if ($estudiante[0]->cond_residencia == "Buenas condiciones en ambiente reducidos") { echo "selected";} ?>>Buenas condiciones en ambiente reducidos</option>
                            <option value="Ambientes y espacios insuficientes, deficiencia en el area sanitaria" <?php if ($estudiante[0]->cond_residencia == "Ambientes y espacios insuficientes, deficiencia en el area sanitaria") { echo "selected";} ?>>Ambientes y espacios insuficientes, deficiencia en el area sanitaria</option>
                            <option value="Ambientes insuficientes y condiciones sanitarias marcadamente inadecuadas" <?php if ($estudiante[0]->cond_residencia == "Ambientes insuficientes y condiciones sanitarias marcadamente inadecuadas") { echo "selected";} ?>>Ambientes insuficientes y condiciones sanitarias marcadamente inadecuadas</option>
                        </select></td>
                      </tr>
                    </table>
                  </div>

<!-- Datos de Emergencia -->
                  <div align="center" id="tabs-3">
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td><b style="color:#006699">En Caso de Emergencia solicitar a:</b></td>
                      </tr>
                      <tr>
                        <td>
                          Nombre/Apellidos: <br>
                          <input name="emerg_nomape" type="text" id="emerg_nomape" size="50" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->emerg_nomape?>">
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Parentesco: <br>
                          <select data-placeholder="Seleccione un Parentesco" name="emerg_parent" id="emerg_parent"  class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Madre" <?php if ($estudiante[0]->emerg_parent == "Madre") { echo "selected";} ?>>Madre</option>
                            <option value="Padre" <?php if ($estudiante[0]->emerg_parent == "Padre") { echo "selected";} ?>>Padre</option>
                            <option value="Hermano(a)" <?php if ($estudiante[0]->emerg_parent == "Hermano(a)") { echo "selected";} ?>>Hermano(a)</option>
                            <option value="Abuelo(a)" <?php if ($estudiante[0]->emerg_parent == "Abuelo(a)") { echo "selected";} ?>>Abuelo(a)</option>
                            <option value="Tio(a)" <?php if ($estudiante[0]->emerg_parent == "Tio(a)") { echo "selected";} ?>>Tio(a)</option>
                            <option value="Conjugue" <?php if ($estudiante[0]->emerg_parent == "Conjugue") { echo "selected";} ?>>Conjugue</option>
                            <option value="Hijo(a)" <?php if ($estudiante[0]->emerg_parent == "Hijo(a)") { echo "selected";} ?>>Hijo(a)</option>
                            <option value="Sobrino(a)" <?php if ($estudiante[0]->emerg_parent == "Sobrino(a)") { echo "selected";} ?>>Sobrino(a)</option>
                            <option value="Primo" <?php if ($estudiante[0]->emerg_parent == "Primo") { echo "selected";} ?>>Primo</option>
                            <option value="Suegro(a)" <?php if ($estudiante[0]->emerg_parent == "Suegro(a)") { echo "selected";} ?>>Suegro(a)</option>
                            <option value="Cuñado(a)" <?php if ($estudiante[0]->emerg_parent == "Cuñado(a)") { echo "selected";} ?>>Cuñado(a)</option>
                            <option value="Yerno" <?php if ($estudiante[0]->emerg_parent == "Yerno") { echo "selected";} ?>>Yerno</option>
                            <option value="Nuera" <?php if ($estudiante[0]->emerg_parent == "Nuera") { echo "selected";} ?>>Nuera</option>
                            <option value="Nieto(a)" <?php if ($estudiante[0]->emerg_parent == "Nieto(a)") { echo "selected";} ?>>Nieto(a)</option>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Telefono: <br>
                          <input type="text" name="emerg_telef" id="emerg_telef" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->emerg_telef?>">
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Otro Telefono: <br>
                          <input type="text" name="emerg_otelef" id="emerg_otelef" class="text ui-widget-content ui-corner-all" value="<?=$estudiante[0]->emerg_otelef?>">
                        </td>
                      </tr>
                    </table>
                  </div>

<!-- Datos Academicos -->
                  <div align="center" id="tabs-4">
                    <table  border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td>
                          Carrera: <br>
                          <select data-placeholder="Seleccione una Carrera" name="id_carrera" id="id_carrera" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <?php if (!empty($carreras)): ?>
                              <?php foreach ($carreras as $carrera): ?>
                                <option value="<?=$carrera->id?>" <?php if ($carrera->id == $estudiante[0]->id_carrera) { echo "selected";} ?>><?=$carrera->descrip_carrera?></option>
                              <?php endforeach ?>
                            <?php endif ?>  
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Semestre: <br>
                          <select data-placeholder="Seleccione un Semestre" name="semestre" id="semestre" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="1" <?php if ($academico[0]->semestre == "1") { echo "selected";} ?>>1</option>
                            <option value="2" <?php if ($academico[0]->semestre == "2") { echo "selected";} ?>>2</option>
                            <option value="3" <?php if ($academico[0]->semestre == "3") { echo "selected";} ?>>3</option>
                            <option value="4" <?php if ($academico[0]->semestre == "4") { echo "selected";} ?>>4</option>
                            <option value="5" <?php if ($academico[0]->semestre == "5") { echo "selected";} ?>>5</option>
                            <option value="6" <?php if ($academico[0]->semestre == "6") { echo "selected";} ?>>6</option>
                            <option value="7" <?php if ($academico[0]->semestre == "7") { echo "selected";} ?>>7</option>
                            <option value="8" <?php if ($academico[0]->semestre == "8") { echo "selected";} ?>>8</option>
                            <option value="9" <?php if ($academico[0]->semestre == "9") { echo "selected";} ?>>9</option>
                            <option value="10" <?php if ($academico[0]->semestre == "10") { echo "selected";} ?>>10</option>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Fecha de Ingreso: <br>
                          <input type="text" name="fech_ingreso" id="fech_ingreso" class="text ui-widget-content ui-corner-all" value="<?php if (!empty($academico)) { echo $academico[0]->fech_ingreso; }?>" readonly/>
                        </td>
                      </tr>
                      <tr>
                        <td>
                          Turno: <br>
                          <select data-placeholder="Seleccione un Turno" name="turno" id="turno" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Matutino" <?php if ($academico[0]->turno == "Matutino") { echo "selected";} ?>>Matutino</option>
                            <option value="Vespertino" <?php if ($academico[0]->turno == "Vespertino") { echo "selected";} ?>>Vespertino</option>
                            <option value="Nocturno" <?php if ($academico[0]->turno == "Nocturno") { echo "selected";} ?>>Nocturno</option>
                          </select>
                        </td>
                      </tr>
                    </table>
                  </div>
                  
<!-- Datos SocioEconomicos -->
                  <div align="center" id="tabs-5">
                    <table  border="0" cellspacing="0" cellpadding="0" width="100%">
                      <tr valign="top">
                        <td colspan="4">Personas que componen tu Grupo Familiar 
                          <?php if(!empty($familia)) { $miembros=array(); foreach ($familia as $familiar) { $miembros[] = $familiar->parentesco;  }}?>
                        </td>
                      </tr>
                      <tr valign="top">
                        <td colspan="4">
                        <select data-placeholder="Seleccione las Personas" name="grupof[]" id="grupof" style="width:100%" class="chzn-select" multiple="multiple">
                          <option></option>
                          <option value="Madre" <?php if (!empty($familia)) {if (in_array("Madre",$miembros)) { echo "selected";}} ?>>Madre</option>
                          <option value="Padre" <?php if (!empty($familia)) {if (in_array("Padre", $miembros)) { echo "selected";}} ?>>Padre</option>
                          <option value="Hermano(a)" <?php if (!empty($familia)) {if(in_array("Hermano(a)", $miembros)) { echo "selected";}} ?>>Hermano(a)</option>
                          <option value="Abuelo(a)" <?php if (!empty($familia)) {if(in_array("Abuelo(a)", $miembros)) { echo "selected";}} ?>>Abuelo(a)</option>
                          <option value="Tio(a)" <?php if (!empty($familia)) {if(in_array("Tio(a)", $miembros)) { echo "selected";}} ?>>Tio(a)</option>
                          <option value="Conjugue" <?php if (!empty($familia)) {if(in_array("Conjugue", $miembros)) { echo "selected";}} ?>>Conjugue</option>
                          <option value="Hijo(a)" <?php if (!empty($familia)) {if(in_array("Hijo(a)", $miembros)) { echo "selected";}} ?>>Hijo(a)</option>
                          <option value="Sobrino(a)" <?php if (!empty($familia)) {if(in_array("Sobrino(a)", $miembros)) { echo "selected";}} ?>>Sobrino(a)</option>
                          <option value="Primo" <?php if (!empty($familia)) {if(in_array("Primo", $miembros)) { echo "selected";}} ?>>Primo</option>
                          <option value="Suegro(a)" <?php if (!empty($familia)) {if(in_array("Suegro(a)", $miembros)) { echo "selected";}} ?>>Suegro(a)</option>
                          <option value="Cuñado(a)" <?php if (!empty($familia)) {if(in_array("Cuñado(a)", $miembros)) { echo "selected";}} ?>>Cuñado(a)</option>
                          <option value="Yerno" <?php if (!empty($familia)) {if(in_array("Yerno", $miembros)) { echo "selected";}} ?>>Yerno</option>
                          <option value="Nuera" <?php if (!empty($familia)) {if(in_array("Nuera", $miembros)) { echo "selected";}} ?>>Nuera</option>
                          <option value="Nieto(a)" <?php if (!empty($familia)) {if(in_array("Nieto(a)", $miembros)) { echo "selected";}} ?>>Nieto(a)</option>
                        </select>
                      </td>
                      </tr>
                      <tr valign="top">
                        <td colspan="4">&nbsp;</td>
                      </tr>
                      <tr valign="top">
                        <td colspan="4"><b style="color:#006699"> ¿Depende Economicamente de Alguien?</b></td>
                      </tr>
                      <tr valign="top">
                        <td colspan="4">Indique si hay una persona que pague por tus gastos:
                          <select data-placeholder="Seleccione Si o No" name="depende" id="depende" style="max-width:50px" class="">
                            <option></option>
                            <option value="No" <?php if (empty($depende_familiar)) { echo "SELECTED"; } ?> >No</option>
                            <option value="Si" <?php if (!empty($depende_familiar)) { echo "SELECTED"; } ?> >Si</option>
                          </select>
                        </td>
                      </tr>

                      <tr valign="top" class="dep">
                        <td>
                          Cedula:&nbsp; <br>
                          <input name="ced_familia" type="text" class="text ui-widget-content ui-corner-all" id="ced_familia" value="<?php if (!empty($depende_familiar)) { echo $depende_familiar[0]->ced_familia;}?>"/>
                        </td>
                        <td>
                          Nombres:&nbsp; <br>
                          <input name="nom_familia" type="text" id="nom_familia" size="50" class="text ui-widget-content ui-corner-all" value="<?php if (!empty($depende_familiar)) { echo $depende_familiar[0]->nom_familia;}?>"/>
                        </td>
                        <td colspan="2">
                          Apellidos:&nbsp; <br>
                          <input name="ape_familia" type="text" id="ape_familia" size="50" class="text ui-widget-content ui-corner-all" value="<?php if (!empty($depende_familiar)) { echo $depende_familiar[0]->ape_familia;}?>"/>
                        </td>
                      </tr>
                      <tr valign="top" class="dep">
                        <td colspan="4"></td>
                      </tr>
                      <tr valign="top" class="dep">
                        <td>
                          Parentesco: <br>
                          <select data-placeholder="Seleccione un Parentesco" name="parentesco" id="parentesco" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Madre" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Madre") { echo "selected";}} ?>>Madre</option>
                            <option value="Padre" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Padre") { echo "selected";}} ?>>Padre</option>
                            <option value="Hermano(a)" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Hermano(a)") { echo "selected";}} ?>>Hermano(a)</option>
                            <option value="Abuelo(a)" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Abuelo(a)") { echo "selected";}} ?>>Abuelo(a)</option>
                            <option value="Tio(a)" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Tio(a)") { echo "selected";}} ?>>Tio(a)</option>
                            <option value="Conjugue" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Conjugue") { echo "selected";}} ?>>Conjugue</option>
                            <option value="Hijo(a)" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Hijo(a)") { echo "selected";}} ?>>Hijo(a)</option>
                            <option value="Sobrino(a)" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Sobrino(a)") { echo "selected";}} ?>>Sobrino(a)</option>
                            <option value="Primo" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Primo") { echo "selected";}} ?>>Primo</option>
                            <option value="Suegro(a)" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Suegro(a)") { echo "selected";}} ?>>Suegro(a)</option>
                            <option value="Cuñado(a)" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Cuñado(a)") { echo "selected";}} ?>>Cuñado(a)</option>
                            <option value="Yerno" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Yerno") { echo "selected";}} ?>>Yerno</option>
                            <option value="Nuera" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Nuera") { echo "selected";}} ?>>Nuera</option>
                            <option value="Nieto(a)" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->parentesco == "Nieto(a)") { echo "selected";}} ?>>Nieto(a)</option>
                            </select>
                        </td>
                        <td>
                          Fecha de Nacimiento: <br>
                          <input type="text" name="fech_nac_fam" id="fech_nac_fam" class="text ui-widget-content ui-corner-all" readonly value="<?php if (!empty($depende_familiar)) { echo $depende_familiar[0]->fech_nac_fam;}?>"/>
                        </td>
                        <td colspan="2">Edad: <br />
                        <input name="edad_familia" type="text" class="text ui-widget-content ui-corner-all" id="edad_familia" value="<?php if (!empty($depende_familiar)) { echo $depende_familiar[0]->edad_familia;}?>"/></td>
                      </tr>
                      <tr valign="top" class="dep">
                        <td>Estado Civil: <br />
                          <select data-placeholder="Seleccione un Estado Civil" name="edocivil_fam" id="edocivil_fam" class="chzn-select">
                            <option></option>
                            <option value="Soltero" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->edocivil_fam == "Soltero") { echo "selected";}} ?>>Soltero</option>
                            <option value="Casado" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->edocivil_fam == "Casado") { echo "selected";}} ?>>Casado</option>
                            <option value="Concubinato" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->edocivil_fam == "Concubinato") { echo "selected";}} ?>>Concubinato</option>
                            <option value="Divorciado" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->edocivil_fam == "Divorciado") { echo "selected";}} ?>>Divorciado</option>
                            <option value="Viudo" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->edocivil_fam == "Viudo") { echo "selected";}} ?>>Viudo</option>
                        </select></td>
                        <td>Ocupación:<br />
                        <input name="ocupacion" type="text" id="ocupacion" class="text ui-widget-content ui-corner-all" value="<?php if (!empty($depende_familiar)) { echo $depende_familiar[0]->ocupacion;}?>"/></td>
                        <td>Grado de Instrucción: <br />
                          <select data-placeholder="Seleccione un Grado de Instrucción" name="g_instruccion" id="g_instruccion" style="width:70%" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Primaria Incompleta" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->g_instruccion == "Primaria Incompleta") { echo "selected";}} ?>>Primaria Incompleta</option>
                            <option value="Primaria" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->g_instruccion == "Primaria") { echo "selected";}} ?>>Primaria</option>
                            <option value="Basica" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->g_instruccion == "Basica") { echo "selected";}} ?>>Basica</option>
                            <option value="Bachiller" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->g_instruccion == "Bachiller") { echo "selected";}} ?>>Bachiller</option>
                            <option value="Tecnico Superior" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->g_instruccion == "Tecnico Superior") { echo "selected";}} ?>>Tecnico Superior</option>
                            <option value="Profesional Universitario" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->g_instruccion == "Profesional Universitario") { echo "selected";}} ?>>Profesional Universitario</option>
                            <option value="Postgrado" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->g_instruccion == "Postgrado") { echo "selected";}} ?>>Postgrado</option>
                        </select></td>
                      </tr>
                      <tr valign="top" class="dep">
                        <td colspan="2">
                          Nombre y Lugar de Trabajo: <br>
                          <textarea name="lg_trabajo" id="lg_trabajo" cols="30" style="width:85%" ><?php if (!empty($depende_familiar)) { echo $depende_familiar[0]->lg_trabajo;}?></textarea>
                        </td>
                        <td>Monto de Ingreso: <br />
                          <select data-placeholder="Seleccione un Monto de Ingreso" name="ing_mensual" id="ing_mensual" style="width:70%" class="chzn-select" value="<?=$familia[0]->ing_mensual?>">
                            <option></option>
                            <option value="Menos de Bsf. 500,00" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->ing_mensual == "Menos de Bsf. 500,00") { echo "selected";}} ?>>Menos de Bsf. 500,00</option>
                            <option value="Entre Bsf. 500,00 y Bsf. 1000,00" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->ing_mensual == "Entre Bsf. 500,00 y Bsf. 1000,00") { echo "selected";}} ?>>Entre Bsf. 500,00 y Bsf. 1000,00</option>
                            <option value="Entre Bsf. 1001,00 y 1500,00" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->ing_mensual == "Entre Bsf. 1001,00 y 1500,00") { echo "selected";}} ?>>Entre Bsf. 1001,00 y 1500,00</option>
                            <option value="Mas de 1500,00" <?php if (!empty($depende_familiar)) { if ($depende_familiar[0]->ing_mensual == "Mas de 1500,00") { echo "selected";}} ?>>Mas de 1500,00</option>
                        </select></td>
                      </tr>
                    </table>
                  </div>
                 
<!-- Datos PsicoSociales -->
                  <div align="center" id="tabs-6">
                    <table border="0" cellspacing="0" cellpadding="0">
                      <tr>
                        <td colspan="4">&nbsp;</td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          En relacion a la carrera que cursas ¿Como te sientes? <br>
                          <select data-placeholder="Seleccione una opción" name="siente_carrera" id="siente_carrera" style="max-width:150px" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Muy Insatisfecho" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->siente_carrera == "Muy Insatisfecho") { echo "selected";}} ?>>Muy Insatisfecho</option>
                            <option value="Insatisfecho" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->siente_carrera == "Insatisfecho") { echo "selected";}} ?>>Insatisfecho</option>
                            <option value="Indiferente" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->siente_carrera == "Indiferente") { echo "selected";}} ?>>Indiferente</option>
                            <option value="Satisfecho" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->siente_carrera == "Satisfecho") { echo "selected";}} ?>>Satisfecho</option>
                            <option value="Muy Satisfecho" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->siente_carrera == "Muy Satisfecho") { echo "selected";}} ?>>Muy Satisfecho</option>
                          </select>
                        </td>
                        <td colspan="2">
                          Grado de satisfacción respecto a la Coord. de Desarrollo Estudiantil <br>
                          <select data-placeholder="Seleccione una opción" name="agrado_coord" id="agrado_coord" style="max-width:150px" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Muy Insatisfecho" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->agrado_coord == "Muy Insatisfecho") { echo "selected";}} ?>>Muy Insatisfecho</option>
                            <option value="Insatisfecho" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->agrado_coord == "Insatisfecho") { echo "selected";}} ?>>Insatisfecho</option>
                            <option value="Indiferente" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->agrado_coord == "Indiferente") { echo "selected";}} ?>>Indiferente</option>
                            <option value="Satisfecho" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->agrado_coord == "Satisfecho") { echo "selected";}} ?>>Satisfecho</option>
                            <option value="Muy Satisfecho" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->agrado_coord == "Muy Satisfecho") { echo "selected";}} ?>>Muy Satisfecho</option>                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          ¿Te sientes Apoyado por tu Familia? <br>
                          <select data-placeholder="Seleccione una opción" name="apoyo_fam" id="apoyo_fam" style="max-width:150px" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Nunca" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->apoyo_fam == "Nunca") { echo "selected";}} ?>>Nunca</option>
                            <option value="Casi Nunca" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->apoyo_fam == "Casi Nunca") { echo "selected";}} ?>>Casi Nunca</option>
                            <option value="Normalmente" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->apoyo_fam == "Normalmente") { echo "selected";}} ?>>Normalmente</option>
                            <option value="Casi Siempre" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->apoyo_fam == "Casi Siempre") { echo "selected";}} ?>>Casi Siempre</option>
                            <option value="Siempre" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->apoyo_fam == "Siempre") { echo "selected";}} ?>>Siempre</option> 
                          </select>
                        </td>
                        <td colspan="2">
                          ¿Que horario te gustaria o convendría para estudiar? <br>
                          <select data-placeholder="Seleccione un horario" name="gusta_horrario" id="gusta_horrario" style="max-width:150px" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Matutino" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->gusta_horrario == "Matutino") { echo "selected";}} ?>>Matutino</option>
                            <option value="Vespertino" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->gusta_horrario == "Vespertino") { echo "selected";}} ?>>Vespertino</option>
                            <option value="Nocturno" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->gusta_horrario == "Nocturno") { echo "selected";}} ?>>Nocturno</option>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          ¿Cuantos hijos Tienes? <br>
                          <select data-placeholder="Seleccione una Cantidad" name="ctd_hijos" class="chzn-select text ui-widget-content ui-corner-all" id="ctd_hijos" style="max-width:150px">
                            <option></option>
                            <option value="Ninguno" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->ctd_hijos == "Ninguno") { echo "selected";}} ?>>Ninguno</option>
                            <option value="1" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->ctd_hijos == "1") { echo "selected";}} ?>>1</option>
                            <option value="2" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->ctd_hijos == "2") { echo "selected";}} ?>>2</option>
                            <option value="3" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->ctd_hijos == "3") { echo "selected";}} ?>>3</option>
                            <option value="Mas de 3" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->ctd_hijos == "Mas de 3") { echo "selected";}} ?>>Mas de 3</option>
                          </select>
                        </td>
                        <td colspan="2">
                          ¿Te gustaria una guarderia en la Institucion? <br>
                          <select data-placeholder="Seleccione una opción" name="guarderia" id="guarderia" style="max-width:150px" class="chzn-select text ui-widget-content ui-corner-all">
                            <option></option>
                            <option value="Si" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->guarderia == "Si") { echo "selected";}} ?>>Si</option>
                            <option value="No" <?php if (!empty($psicosocial)) { if ($psicosocial[0]->guarderia == "No") { echo "selected";}} ?>>No</option>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          Actividades Extracurriculares Comunitarias<br>
                          <?php if (!empty($destrezas_a)) { $act=array(); foreach ($destrezas_a as $des) { $act[] = $des->descrip_oficio;  }}?>
                          <select data-placeholder="Seleccione una actividad" name="id_oficio[]" id="id_oficio" style="width:350px" class="chzn-select" multiple>
                            <option></option>
                            <?php if (!empty($actividades)): ?>
                              <?php foreach ($actividades as $actividad): ?>
                                <option value="<?=$actividad->id?>" <?php if (!empty($destrezas_a)) { if (in_array($actividad->descrip_oficio, $act)) { echo "selected";}} ?>><?=$actividad->descrip_oficio?></option>
                              <?php endforeach ?>
                            <?php endif ?>  
                          
                          </select>
                        </td>
                        <td colspan="2">
                          ¿Tienes habilidades propias en alguna de estas areas? <br>
                          <?php if (!empty($destrezas_h)) { $hab=array(); foreach ($destrezas_h as $habli) { $hab[] = $habli->descrip_oficio;  }}?>
                          <select data-placeholder="Seleccione uns habilidad" name="id_habilidad[]" id="id_habilidad" style="width:350px" class="chzn-select" multiple>
                            <option></option>
                            <?php if (!empty($habilidades)): ?>
                              <?php foreach ($habilidades as $habilidad): ?>
                                <option value="<?=$habilidad->id?>" <?php if (!empty($destrezas_h)) { if (in_array($habilidad->descrip_oficio, $hab)) { echo "selected";}} ?>><?=$habilidad->descrip_oficio?></option>
                              <?php endforeach ?>
                            <?php endif ?>
                          
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="4">
                          Nombre del Concejo Comunal al que perteneces: <br>
                          <input name="nom_cc" type="text" id="nom_cc" style="width:98%;" class="text ui-widget-content ui-corner-all" value="<?php if (!empty($psicosocial)) { echo $psicosocial[0]->nom_cc;}?>"/>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="4">
                          Dirección del Concejo Comunal al que perteneces: <br>
                          <textarea name="dir_cc" id="dir_cc" style="width:98%;"><?php if (!empty($psicosocial)) { echo $psicosocial[0]->dir_cc;} ?></textarea>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          Enfermedades que has padecido: <br>
                          <?php if (!empty($pdcdo)) { $sal=array(); foreach ($pdcdo as $salu) { $sal[] = $salu->descrip_patologia;  }}?>
                          <select data-placeholder="Seleccione una enfermedad" name="id_patologia_padecido[]" id="id_patologia_padecido" style="width:350px" class="chzn-select" multiple>
                            <option></option>
                            <?php if (!empty($padecido)): ?>
                              <?php foreach ($padecido as $enfermedad): ?>
                                <option value="<?=$enfermedad->id?>" <?php if (!empty($pdcdo)) { if (in_array($enfermedad->descrip_patologia, $sal)) { echo "selected";}} ?>><?=$enfermedad->descrip_patologia?></option>
                              <?php endforeach ?>
                            <?php endif ?>
                          </select>
                        </td>
                        <td colspan="2">
                          Enfermedades que padeces: <br>
                          <?php if (!empty($pdc)) { $sal2=array(); foreach ($pdc as $salu) { $sal2[] = $salu->descrip_patologia;  }}?>
                          <select data-placeholder="Seleccione una enfermedad" name="id_patologia_padeces[]" id="id_patologia_padeces" style="width:350px" class="chzn-select" multiple>
                            <option></option>
                            <?php if (!empty($padeces)): ?>
                              <?php foreach ($padeces as $enfermedad): ?>
                                <option value="<?=$enfermedad->id?>" <?php if (!empty($pdc)) { if (in_array($enfermedad->descrip_patologia, $sal2)) { echo "selected";}} ?>><?=$enfermedad->descrip_patologia?></option>
                              <?php endforeach ?>
                            <?php endif ?>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="4">
                          Vacunas: <br>
                          <?php if (!empty($vac)) { $sal3=array(); foreach ($vac as $salu) { $sal3[] = $salu->descrip_patologia;  }}?>
                          <select data-placeholder="Seleccione las Vacunas" name="id_patologia_vacunas[]" id="id_patologia_vacunas" style="width:100%" class="chzn-select" multiple>
                            <option></option>
                            <?php if (!empty($vacunas)): ?>
                              <?php foreach ($vacunas as $vacuna): ?>
                                <option value="<?=$vacuna->id?>" <?php if (!empty($vac)) { if (in_array($vacuna->descrip_patologia, $sal3)) { echo "selected";}} ?>><?=$vacuna->descrip_patologia?></option>
                              <?php endforeach ?>
                            <?php endif ?>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="4">
                          Discapacidades:
                        </td>
                      </tr>
                      <tr>
                        <td colspan="2">
                          Tipo: <br>
                          <select name="tipo_discapacidad" id="tipo_discapacidad" class="form-control">
                                <?php 
                                  $dis = $this->db
                                                      ->where('id_discapacidad',$estudiante[0]->id_discapacidad)
                                                      ->get('discapacidad')
                                                      ->row(0);
                                  $discapacidades = $this->db
                                                                ->where('tipo_discapacidad',$dis->tipo_discapacidad)
                                                                ->get('discapacidad')
                                                                ->result();
                                ?>
                            <option value="Ninguna" <?=empty($dis) ? 'selected="selected"' : ''?>>Ninguna</option>
                            <option value="Discapacidades Sensoriales y De La Comunicación" <?=(!empty($dis) && $dis->tipo_discapacidad == "Discapacidades Sensoriales y De La Comunicación") ? 'selected="selected"' : ''?>>Discapacidades Sensoriales y De La Comunicación</option>
                            <option value="Discapacidades Motrices" <?=(!empty($dis) && $dis->tipo_discapacidad == "Discapacidades Motrices") ? 'selected="selected"' : ''?>>Discapacidades Motrices</option>
                            <option value="Discapacidades Mentales" <?=(!empty($dis) && $dis->tipo_discapacidad == "Discapacidades Mentales") ? 'selected="selected"' : ''?>>Discapacidades Mentales</option>
                            <option value="Discapacidades Múltiples y Otras" <?=(!empty($dis) && $dis->tipo_discapacidad == "Discapacidades Múltiples y Otras") ? 'selected="selected"' : ''?>>Discapacidades Múltiples y Otras</option>
                          </select>
                        </td>
                        <td colspan="2">
                          Sub-tipo: <br>
                          <select name="subtipo_discapacidad" id="subtipo_discapacidad" class="form-control">
                              <pre>
                                <?php 
                                  print_r($discapacidades)
                                 ?>
                              </pre>
                              <?php if (!empty($estudiante[0]->id_discapacidad)): ?>
                                <?php foreach ($discapacidades as $d): ?>
                              <option value="<?=$d->id_discapacidad?>" <?=$d->id_discapacidad == $estudiante[0]->id_discapacidad ? 'selected="selected"' : '' ?>><?=$d->subtipo_discapacidad?></option>
                                <?php endforeach ?>
                              <?php else: ?>
                                <option value=""></option>
                              <?php endif ?>
                          </select>
                        </td>
                      </tr>
                      <tr>
                        <td colspan="4">
                          Detalle de discapacidad: <br>
                          <input type="text" style="width:100%" name="detalle_discapacidad" id="detalle_discapacidad" value="<?=empty($estudiante[0]->detalle_discapacidad) ? '' : $estudiante[0]->detalle_discapacidad ?>">
                        </td>
                      </tr>
                          
                    </table>
                  </div>

                </div><!-- Fin Tabs -->

              </td>
              </tr>
              <tr>
                <td colspan="6">
                  <hr size="1" color="#4297d7" width="97%" align="center" noshade="noshade" />
                </td>
              </tr>
              <tr>
                <td colspan="5" width="60%">&nbsp;</td>
                  
                <?php if (empty($consultar)): ?>
                  <td><input type="submit" name="opcion" id="button" value="Guardar" style="background-color:#5c9ccc; color:#FFF; width:110px; height:30px" /></td>
                <?php endif ?>
              </tr>
            </table>
          </form>
          <!-- Fin Form -->
        </div>
      </div>
</div>