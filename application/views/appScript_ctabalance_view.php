<?php 
    $URL_PRIN = $this->urlprin->getUrl();
?>
<?php 
if (!empty($URL_PRIN)) {
?>
<script src="<?=base_url()?>jquery/jquery-1.8.2.js"></script>
<script type="text/javascript">
    $('#save-and-go-back-button, .btn[value=Guardar]').click(function(){
      codigo  = $("#field-cod_cuenta").val();
      $.post("<?=$URL_PRIN?>ctabalance/checkIsUnique", 
        { cod_cuenta: codigo } , 
        function(data) {
          if (data.mensaje!="ok") 
          {
            alert(data.mensaje);
            return false;
          };
        },'json');
      });
</script>
<?php 
    }
?>
