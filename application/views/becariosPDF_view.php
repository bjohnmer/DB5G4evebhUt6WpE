<?php
  $URL_PRIN = $this->urlprin->getUrl();
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
  <div id="main-container">
    <table border="0" align="center" width="90%" cellpadding="5" valign="top">
    <tr>
      <td colspan="2" align="center">
        <img src="<?=base_url()?>img/sibe_header.jpg" alt="" width="700px">
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <h1>
          <?php if (!empty($titulo)): ?>
            <?=$titulo?>
          <?php endif ?>
          <br>
          <!-- <input class="atras" type="image" src="<?=base_url()?>css/themes/flexigrid/css/images/prev.gif" alt="Devolverse" title="Devolverse">
          <input class="imprimir" type="image" src="<?=base_url()?>css/themes/flexigrid/css/images/print.png" alt="Imprimir" title="Imprimir"> -->
        </h1>
      </td>
    </tr>   
    
<tr>
  <td colspan="2">
    
    <table border="1" cellspacing="0" cellpadding="0" id="flex1" width="100%">
      <thead>
        <tr class="hDiv">
          <th width="5%">
            <div rel="nro_solicitud_a" class="text-left field-sorting ASC">
              Solicitud
            </div>
          </th>
          <th width="10%">
            <div rel="cedula" class="text-left field-sorting ASC">
              Cédula
            </div>
          </th>

          <th width="25%">
            <div rel="nro_solicitud_a" class="text-left field-sorting ASC">
              Solicitante
            </div>
          </th>
          <th width="5%">
            <div rel="fech_solicitud" class="text-left field-sorting ASC">
              Fecha
            </div>
          </th>
          <th width="20%">
            <div rel="descrip_ayuda" class="text-left field-sorting ASC">
              Tipo de Beca          
            </div>
          </th>
          <th width="20%">
            <div rel="status" class="text-left field-sorting ASC">
              Estatus          
            </div>
          </th>
        </tr>
      </thead>    
      <tbody style="font-size: .8em">
        <?php if (!empty($solicitudes)):  $band = false; ?>
          <?php foreach ($solicitudes as $solicitud):?>
            <tr <?php if ($band): ?>class="erow"<?php $band = false; else: $band = true;?><?php endif ?> >
              <td width="20%" class="sorted" style="text-align:center;">
                <div class="text-left"><?=$solicitud->nro_solicitud_b?></div>
              </td>
                           
              <td width="20%" class="sorted" style="text-align:center;">
                <?php 
                  $estud = $this->estudiante->getBy("id",$solicitud->id_estudiante);
                ?>
                <div class="text-left"><?=$estud[0]->ced_estudiante?></div>
              </td>
              
              <td width="20%" class="sorted" style="text-align:center;">
                <div class="text-left"><?=$estud[0]->nombres." ".$estud[0]->apellidos?></div>
              </td>

              <td width="20%" class="" style="text-align:center;">
                <div class="text-left"><?=$this->datemanager->date2normal($solicitud->fech_solicitud)?></div>
              </td>
              <td width="20%" class="" style="text-align:center;">
                <div class="text-left"><?=$solicitud->descrip_beca?></div>
              </td>
              <td width="20%" class="" style="text-align:center;">
                <div class="text-left"><?=$solicitud->status?></div>
              </td>
              
            </tr>
          <?php endforeach ?>
        <?php else: ?>
          <tr>
            <td colspan="6">
              <br>No Hay Solicitudes de Ayuda registradas<br>
            </td>
          </tr>
        <?php endif ?>
      </tbody>
    </table>

  </td>
</tr>
    <tr>
      <td colspan="2">
        <img src="<?=base_url()?>img/footer_sibe.jpg" alt="" width="700px">
      </td>
    </tr>
  </table>
    <footer>
      <?php if (!$this->session->userdata("logged_in")){ ?>
        <p><b> Equipo de Desarrollo: </b>
        <a href="#"> Gonzalo Calderas |</a>
        <a href="#"> Nelson Sánchez |</a>
        <a href="#"> Gustavo Núñez |</a>
        <a href="#"> Belfer Rivero </a></p> 
      <?php } else { ?>
        <p><b> Usuario: </b>
        <?=$this->session->userdata("tp_usuario")."/".$this->session->userdata("nombre")?>
        </p> 
      <?php } ?>
    </footer>
  </div>
</body>

</html>
