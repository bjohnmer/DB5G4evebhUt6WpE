<?php 
    $URL_PRIN = $this->urlprin->getUrl();
?>
<?php 
if (!empty($URL_PRIN)) {
?>
<script src="<?=base_url()?>jquery/jquery-1.8.2.js"></script>
<script type="text/javascript">
  $('#save-and-go-back-button, .btn[value=Guardar]').click(function(){
    codigo  = $("#field-cod_ayuda").val();
    $.post("<?=$URL_PRIN?>tpayudas/checkIsUnique", 
    { cod_ayuda: codigo } , 
    function(data) {
      if (data.mensaje!="ok") 
      {
        alert(data.mensaje);
        return false;
      };
    },'json');
  });
  
  $('.delete-row').click(function(){
    var delete_url = $(this).attr('href');
    // var id         = $(this).attr('alt');
    if (!confirm("¿Está seguro de Eliminar el registro?"))
    {
      return false;
    }    
  });

</script>
<?php 
    }
?>
