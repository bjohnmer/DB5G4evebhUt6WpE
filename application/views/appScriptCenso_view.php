<?php 
    $URL_PRIN = $this->urlprin->getUrl();
?>
<?php 
  if (!empty($URL_PRIN)) {
?>
<script type="text/javascript">


  $(document).ready(function(){

    <?php if (empty($depende_familiar)) { ?>
      $(".dep").hide();
    <?php } ?>
    $("#depende").change(function(){
        $("#depende option:selected").each(function () {
          depende = $(this).val();
          if (depende == "Si") {
            $(".dep").show();
          }
          else
          {
            $(".dep").hide();
          };
        });
    });

     $("#drres_edo").change(function () {
        $("#drres_edo option:selected").each(function () {
           estado=$(this).val();
           $.post("<?=$URL_PRIN?>censo/getMunicipios", { estado: estado }, function(data){
              $("#drres_mun").html(data);
              $("#drres_parr").html("");
           },"json");
        });
     });

     $("#drres_mun").change(function () {
        $("#drres_mun option:selected").each(function () {
           municipio=$(this).val();
           $.post("<?=$URL_PRIN?>censo/getParroquias", { municipio: municipio }, function(data){
              $("#drres_parr").html(data);
           },"json");
        });
     });


     $("#drhab_edo").change(function () {
        $("#drhab_edo option:selected").each(function () {
           estado=$(this).val();
           $.post("<?=$URL_PRIN?>censo/getMunicipios", { estado: estado }, function(data){
              $("#drhab_mun").html(data);
              $("#drhab_parr").html("");
           },"json");
        });
     });

     $("#drhab_mun").change(function () {
        $("#drhab_mun option:selected").each(function () {
           municipio=$(this).val();
           $.post("<?=$URL_PRIN?>censo/getParroquias", { municipio: municipio }, function(data){
              $("#drhab_parr").html(data);
           },"json");
        });
     });
    
    $("#tipo_discapacidad").change(function () {
       $("#tipo_discapacidad option:selected").each(function () {
          tipo_discapacidad=$(this).val();
          $.post("<?=$URL_PRIN?>censo/getDiscap", { tipo_discapacidad: tipo_discapacidad }, function(data){
             $("#subtipo_discapacidad").html(data);
          },"json");
       });
    });

  });
</script>
<?php 
    }
?>
