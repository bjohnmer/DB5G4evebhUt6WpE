<div id="info-contenido">
      <div id="datos" class="fila">
        <div id="menu" class="col">
          <?php $this->load->view("menu_view"); ?>
        </div>
        <div id="info" class="col">
          <?php if (!empty($advertencia)) { ?>
            <div class="report-div error" id="report-error" style="display: block;">
              <p>
                Tenga en cuenta que el estudiante tiene Beca Asignada. La modificación del Status y/o El Índice Académico pudiera suspender la Beca
              </p>
            </div>
          <?php } ?>
          
          <?php if (!empty($reportes)): ?>
            <h1><?=$titulo?></h1>
            <div style="width: 100%;" class="flexigrid crud-form">  
              <div class="mDiv">
                <div class="ftitle">
                  <div class="ftitle-left">
                    Seleccione las opciones para descargar el reporte
                  </div>      
                  <div class="clear"></div>
                </div>
              </div>
              <div id="main-table-box">
                <?php 
                  $go = "verReporteSB";
                ?>

                <form accept-charset="utf-8" id="reportesForm" method="post" action="<?=base_url()?>reportes/<?=$go?>">

                  <div class="form-div">
                    <div id="general_field_box" class="form-field-box odd">
                      <div id="general_display_as_box" class="form-display-as-box">
                          General :
                      </div>
                      <div>
                        <input type="radio" value="general" id="general" name="seleccion" checked="checked">
                      </div>
                    </div>
                    

                    <div id="placa_field_box" class="form-field-box odd">
                      <div id="placa_display_as_box" class="form-display-as-box">
                          Placa :
                      </div>
                      <div>
                        <input type="radio" value="id_bus" id="id_bus" name="seleccion">
                        <input type="text" name="placa" id="placa">
                      </div>
                    </div>

                    <div id="por_fecha" class="form-field-box odd">
                      <div id="por_fecha" class="form-display-as-box">
                          Por Fecha :
                      </div>
                      <div>
                        <input type="radio" value="fecha_status" id="fecha_status" name="seleccion">
                      </div>
                    </div>
                    <div id="por_fecha" class="form-field-box odd">
                      <div id="por_fecha_desde" class="form-display-as-box">
                          Desde :
                      </div>
                      <div>
                        <div>
                          <input type="text" id="desde" name="desde" class="span3" readonly style=" width: 100px;">
                        </div>
                      </div>
                    </div>
                    <div id="por_fecha" class="form-field-box odd">
                      <div id="por_fecha_hasta" class="form-display-as-box">
                          Hasta :
                      </div>
                      <div>
                        <div>
                          <input type="text" id="hasta" name="hasta" class="span3" readonly style=" width: 100px;">
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  <div class="pDiv">
                    <div class="form-button-box">
                      <input type="submit" class="btn btn-large" value="Guardar">
                    </div>
                    <div class="clear"></div> 
                  </div>


                </form>

            </div>
          </div>


          <?php endif ?>
        </div>
      </div>
</div>