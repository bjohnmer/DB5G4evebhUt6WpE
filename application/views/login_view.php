<?php 
    $URL_PRIN = $this->urlprin->getUrl();
?>
<div id="contenido">
  <?php
    if (!empty($mensaje['tipo']) && $mensaje['tipo']=="success") 
    {
      echo "<div class='alert alert-success'>";
        echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
        echo "<h4>¡Muy bien!</h4>";
        echo "<p>".$mensaje['mensaje']."</p>";
      echo "</div>";
    }
    if (empty($mensaje['from'])) 
    {
      if (validation_errors() || !empty($logged_in_fail))
      {
        echo "<div class='alert alert-error'>";
        echo '<button type="button" class="close" data-dismiss="alert">&times;</button>';
        echo "<h4>Se han encontrado errores</h4>";
        if (validation_errors()) echo validation_errors();
        if (!empty($logged_in_fail)) echo "<p>ERROR:<br>Usted a proporcionado datos inválidos<br>Intente nuevamente</p>";
        echo "</div>";
      }
    }
  ?>

  <div id="form-login">
    <form action="<?=$URL_PRIN?>login/entrar" method="post">
      <label for="usuario">Usuario</label>
      <input type="text" name="usuario" id="usuario">
      <label for="clave">Clave</label>
      <input type="password" name="clave" id="clave">
      <button>Ingresar</button>
    </form>
    <p>
      <a href="#" id="registro">Pulse aquí para registro</a>
    </p>
  </div>


  <div id="form_registro" title="Registro de Usuarios">
    <p class="validateTips">
      <?php if (!empty($mensaje['from'])): ?>
        <?php if ($mensaje['from']=="registro"): ?>
          <?php //if (!empty($mensaje['tipo'])): ?>
            <?php //if ($mensaje['tipo'] == 'error'): ?>
              <div id="report-error" class="alert alert-error" style="display:block;">
                <h5>Se han encontrado errores</h5>
                <?php if (!empty($mensaje['mensaje'])): ?>
                  <?php echo $mensaje['mensaje']; ?>
                <?php endif ?>
                <?php if (validation_errors()): ?>
                    <?=validation_errors()?>
                <?php endif ?>
              </div>
            <?php //endif ?>
          <?php //endif ?>
        
        <?php endif ?>
      <?php endif ?>
    </p>
    <!--Formulario de Registro -->
      <?php echo '
        <form action="'.$URL_PRIN.'login/registro" name="form_registro1" method="post" id="form-reg">
          <label for="cedula">Cedula</label>
          <input type="text" name="cedula" id="cedula" class="text ui-widget-content ui-corner-all" />
          <label for="fechanac">Fecha de Nacimiento</label>
          <input type="text" name="fechanac" id="datepicker" value="" class="text ui-widget-content ui-corner-all" readonly/>
          <hr size="1" color="#79b7e7" align="center" noshade>
          <label for="usuario">Usuario</label>
          <input type="text" name="usuario" id="usuario" class="text ui-widget-content ui-corner-all" />
          <label for="clave">Clave</label>
          <input type="password" name="clave" id="clave" class="text ui-widget-content ui-corner-all" />
          <label for="confir">Confirme</label>
          <input type="password" name="confir" id="confir" class="text ui-widget-content ui-corner-all" />
        </form>';
       ?> 
  </div>

  
</div>