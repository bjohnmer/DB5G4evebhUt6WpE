<?php
  $URL_PRIN = $this->urlprin->getUrl();
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
  <div id="main-container">
<?php if (!empty($solicitud)): ?>
  <table border="0" align="center" width="90%" cellpadding="5" valign="top">
    <tr>
      <td colspan="2" align="center">
        <img src="<?=base_url()?>img/sibe_header.jpg" alt="" width="700px">
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <h1>
          <?php if (!empty($titulo)): ?>
            <?=$titulo?>
          <?php endif ?><br>
        </h1>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <h2>Comprobante</h2>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Solicitud N°:</strong></td>
      <td><?=$solicitud[0]->nro_solicitud_t?></td>
    </tr>
    <tr>
      <td align="right"><strong>Emitida en Fecha:</strong></td>
      <td><?=$solicitud[0]->fech_solicitud?></td>
    </tr>
    <tr>
      <td align="right"><strong>Cédula:</strong></td>
      <td><?=$estudiante[0]->ced_estudiante?></td>
    </tr>
    <tr>
      <td align="right"><strong>Apellidos:</strong></td>
      <td><?=$estudiante[0]->apellidos?></td>
    </tr>
    <tr>
      <td align="right"><strong>Nombres:</strong></td>
      <td><?=$estudiante[0]->nombres?></td>
    </tr>
    <tr>
      <td align="right"><strong>Organización:</strong></td>
      <td><?=$solicitud[0]->organizacion;?></td>
    </tr>
    <tr>
      <td align="right"><strong>Ruta:</strong></td>
      <td>
        <p>
          <?=$solicitud[0]->ruta_destino;?>
        </p>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Motivo:</strong></td>
      <td>
        <p>
          <?=$solicitud[0]->motivo; ?>
        </p>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Fecha de Salida:</strong></td>
      <td>
        <p>
          <?=$solicitud[0]->fech_salida; ?>
        </p>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Hora de Salida:</strong></td>
      <td>
        <p>
          <?=$solicitud[0]->hora_salida; ?>
        </p>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Fecha de Regreso:</strong></td>
      <td>
        <p>
          <?=$solicitud[0]->fech_regreso; ?>
        </p>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <img src="<?=base_url()?>img/footer_sibe.jpg" alt=""  width="700px">
      </td>
    </tr>
  </table>
<?php else: ?>
  <h3>El registro no fué encontrado</h3>
<?php endif ?>

    <footer>
      <?php if (!$this->session->userdata("logged_in")){ ?>
        <p><b> Equipo de Desarrollo: </b>
        <a href="#"> Gonzalo Calderas |</a>
        <a href="#"> Nelson Sánchez |</a>
        <a href="#"> Gustavo Núñez |</a>
        <a href="#"> Belfer Rivero </a></p> 
      <?php } else { ?>
        <p><b> Usuario: </b>
        <?=$this->session->userdata("tp_usuario")."/".$this->session->userdata("nombre")?>
        </p> 
      <?php } ?>
    </footer>
  </div>
</body>

</html>
