<?php
  $URL_PRIN = $this->urlprin->getUrl();
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
  <div id="main-container">
    <table border="0" align="center" width="90%" cellpadding="5" valign="top">
    <tr>
      <td colspan="2" align="center">
        <img src="<?=base_url()?>img/sibe_header.jpg" alt="" width="700px">
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <h1>
          <?php if (!empty($titulo)): ?>
            <?=$titulo?>
          <?php endif ?>
          <br>
          <!-- <input class="atras" type="image" src="<?=base_url()?>css/themes/flexigrid/css/images/prev.gif" alt="Devolverse" title="Devolverse">
          <input class="imprimir" type="image" src="<?=base_url()?>css/themes/flexigrid/css/images/print.png" alt="Imprimir" title="Imprimir"> -->
        </h1>
      </td>
    </tr>   
    
<tr>
  <td colspan="2">
    
    <table border="1" cellspacing="0" cellpadding="0" id="flex1" width="100%">
      <thead>
        <tr class="hDiv">
          <th width="5%">
                <div rel="nro_solicitud_t" class="text-left field-sorting ASC">
                  Nro
                </div>
              </th>
              <th width="20%">
                <div rel="nro_solicitud_t" class="text-left field-sorting ASC">
                  Bus Asignado
                </div>
              </th>
              <th width="10%">
                <div rel="fech_solicitud" class="text-left field-sorting ASC">
                  Fecha de Solicitud
                </div>
              </th>
              <th width="25%">
                <div rel="ruta_destino" class="text-left field-sorting ASC">
                  Ruta
                </div>
              </th>
              <th width="10%">
                <div rel="fech_salida" class="text-left field-sorting ASC">
                  Salida
                </div>
              </th>
              <th width="10%">
                <div rel="fech_regreso" class="text-left field-sorting ASC">
                  Regreso
                </div>
              </th>
              <th width="20%">
                <div rel="status" class="text-left field-sorting ASC">
                  Estatus
                </div>
              </th>
        </tr>
      </thead>    
      <tbody style="font-size: .8em">
        <?php if (!empty($solicitudes)):  $band = false; ?>
          <?php foreach ($solicitudes as $solicitud):?>
            <tr <?php if ($band): ?>class="erow"<?php $band = false; else: $band = true;?><?php endif ?> >
              <td class="sorted" style="text-align:center;">
                <div class="text-left"><?=$solicitud->nro_solicitud_t?></div>
              </td>
              <td class="sorted" style="text-align:center;">
                <div class="text-left"><?=$solicitud->id_bus == 0 ? "" : $solicitud->placa ." - ".$solicitud->marca ." ".$solicitud->modelo ." <br> ". $solicitud->ced_chofer ." ". $solicitud->nombre . " " . $solicitud->apellido?></div>
              </td>
              <td class="" style="text-align:center;">
                <div class="text-left"><?=$solicitud->fech_solicitud?></div>
              </td>
              <td class="" style="text-align:center;">
                <div class="text-left"><?=$solicitud->ruta_destino?></div>
              </td>
              <td class="" style="text-align:center;">
                <div class="text-left"><?=$solicitud->fech_salida?></div>
              </td>
              <td class="" style="text-align:center;">
                <div class="text-left"><?=$solicitud->fech_regreso?></div>
              </td>
              <td class="" style="text-align:center;">
                <div class="text-left"><?=$solicitud->status?></div>
              </td>
             
            </tr>
          <?php endforeach ?>
        <?php else: ?>
          <tr>
            <td colspan="6">
              <br>No Hay Solicitudes de Ayuda registradas<br>
            </td>
          </tr>
        <?php endif ?>
      </tbody>
    </table>

  </td>
</tr>
    <tr>
      <td colspan="2">
        <img src="<?=base_url()?>img/footer_sibe.jpg" alt="" width="700px">
      </td>
    </tr>
  </table>
    <footer>
      <?php if (!$this->session->userdata("logged_in")){ ?>
        <p><b> Equipo de Desarrollo: </b>
        <a href="#"> Gonzalo Calderas |</a>
        <a href="#"> Nelson Sánchez |</a>
        <a href="#"> Gustavo Núñez |</a>
        <a href="#"> Belfer Rivero </a></p> 
      <?php } else { ?>
        <p><b> Usuario: </b>
        <?=$this->session->userdata("tp_usuario")."/".$this->session->userdata("nombre")?>
        </p> 
      <?php } ?>
    </footer>
  </div>
</body>

</html>
