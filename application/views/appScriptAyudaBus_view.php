<script type="text/javascript">
  $(document).ready(function(){
    
    $("#id_bus, #id_chofer").prop('disabled', true);
    $("input[name='status']").click(function(){
      if ($(this).val()=="Aprobado") {
        $("#id_bus, #id_chofer").prop('disabled', false);
        $("#camposA").removeClass('hidden');
      }
      else
      {
        $("#id_bus, #id_chofer").prop('disabled', true);
        $("#camposA").addClass('hidden');
      };
    });
  });
</script>