<?php
  $URL_PRIN = $this->urlprin->getUrl();
?>
<?php if (!empty($solicitud)): ?>
  <table border="0" align="center" width="90%" cellpadding="5" valign="top">
    <tr>
      <td colspan="2" align="center">
        <img src="<?=base_url()?>img/sibe_header.jpg" alt="">
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <h1>
          <?php if (!empty($titulo)): ?>
            <?=$titulo?>
          <?php endif ?><br>
          <input class="atras" type="image" src="<?=base_url()?>css/themes/flexigrid/css/images/prev.gif" alt="Devolverse" title="Devolverse">
          <input class="imprimir" type="image" src="<?=base_url()?>css/themes/flexigrid/css/images/print.png" alt="Imprimir" title="Imprimir">
        </h1>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <h2>Comprobante</h2>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Solicitud N°:</strong></td>
      <td><?=$solicitud[0]->nro_solicitud_t?></td>
    </tr>
    <tr>
      <td align="right"><strong>Emitida en Fecha:</strong></td>
      <td><?=$solicitud[0]->fech_solicitud?></td>
    </tr>
    <tr>
      <td align="right"><strong>Cédula:</strong></td>
      <td><?=$estudiante[0]->ced_estudiante?></td>
    </tr>
    <tr>
      <td align="right"><strong>Apellidos:</strong></td>
      <td><?=$estudiante[0]->apellidos?></td>
    </tr>
    <tr>
      <td align="right"><strong>Nombres:</strong></td>
      <td><?=$estudiante[0]->nombres?></td>
    </tr>
    <tr>
      <td align="right"><strong>Organización:</strong></td>
      <td><?=$solicitud[0]->organizacion;?></td>
    </tr>
    <tr>
      <td align="right"><strong>Ruta:</strong></td>
      <td>
        <p>
          <?=$solicitud[0]->ruta_destino;?>
        </p>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Motivo:</strong></td>
      <td>
        <p>
          <?=$solicitud[0]->motivo; ?>
        </p>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Fecha de Salida:</strong></td>
      <td>
        <p>
          <?=$solicitud[0]->fech_salida; ?>
        </p>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Hora de Salida:</strong></td>
      <td>
        <p>
          <?=$solicitud[0]->hora_salida; ?>
        </p>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Fecha de Regreso:</strong></td>
      <td>
        <p>
          <?=$solicitud[0]->fech_regreso; ?>
        </p>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <img src="<?=base_url()?>img/footer_sibe.jpg" alt="">
      </td>
    </tr>
  </table>
<?php else: ?>
  <h3>El registro no fué encontrado</h3>
<?php endif ?>
<script>
  $(document).ready(function(){
    $(".atras").click(function(){
      window.location = "<?=$URL_PRIN?>/solicitudBus"
    });
  });
</script>