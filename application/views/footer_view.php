    <footer>
      <?php if (!$this->session->userdata("logged_in")){ ?>
        <p><b> Equipo de Desarrollo: </b>
        <a href="#"> Gonzalo Calderas |</a>
        <a href="#"> Nelson Sánchez |</a>
        <a href="#"> Gustavo Núñez |</a>
        <a href="#"> Belfer Rivero |</a>
        <a href="#"> Vanessa Pirela </a>
      </p> 
      <?php } else { ?>
        <p><b> Usuario: </b>
        <?=$this->session->userdata("tp_usuario")."/".$this->session->userdata("nombre")?>
        </p> 
      <?php } ?>
    </footer>
  </div>
</body>
  


  <script src="<?=base_url()?>jquery/jqueryA.min.js" type="text/javascript"></script>

  <script src="<?=base_url()?>css/chosen/chosen/chosen.jquery.js" type="text/javascript"></script>
  
  <script type="text/javascript">
    $(".chzn-select").chosen(); 
    $(".chzn-select-deselect").chosen({allow_single_deselect:true}); 
  </script>


  <?php if (!empty($js_files)): ?>
    <?php foreach($js_files as $file): ?>
      <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
  <?php endif ?>
  <?php if (empty($js_files)): ?>
    <script src="<?=base_url()?>jquery/jquery-1.8.2.js"></script>
  <?php endif ?>

  <script src="<?=base_url()?>jquery/app.js"></script>


  <script src="<?=base_url()?>jquery/ui/jquery.ui.core.js"></script>
  <script src="<?=base_url()?>jquery/ui/jquery.ui.widget.js"></script>
  <script src="<?=base_url()?>jquery/ui/jquery.ui.button.js"></script>
  <script src="<?=base_url()?>jquery/ui/jquery.ui.position.js"></script>
  <script src="<?=base_url()?>jquery/ui/jquery.ui.datepicker.js"></script>
  <script src="<?=base_url()?>jquery/ui/i18n/jquery.ui.datepicker-es.js"></script>
  <script src="<?=base_url()?>jquery/ui/jquery.ui.accordion.js"></script>
  <script src="<?=base_url()?>jquery/ui/jquery.ui.tabs.js"></script>
  <script src="<?=base_url()?>js/bootstrap.min.js"></script>
  
<script type="text/javascript">
    $(function() {
      $( "#menus" ).accordion();
    });
    $(function() {
      $( "#tabs" ).tabs();
    });
    $(function() {
      $( "#Feching" ).datepicker();
      $( "#fechnacpaga" ).datepicker();
      $( "#desde, #hasta" ).datepicker({
        changeYear: true,
        changeMonth: true,
        yearRange: "-50:"
      });
    });
    
    // function activa()
    // {
    //   if(document.frmcenso.cmb_depende.value=="si")
    //   {
    //     document.frmcenso.cedfampaga.disabled=false;
    //     document.frmcenso.nombpaga.disabled=false;
    //     document.frmcenso.apellpaga.disabled=false;
    //     document.frmcenso.cmb_parentesco.disabled=false;
    //     document.frmcenso.fechnacpaga.disabled=false;
    //     document.frmcenso.cmb_edocivpaga.disabled=false;
    //     document.frmcenso.oficiopaga.disabled=false;
    //     document.frmcenso.cmb_estudios.disabled=false;
    //     document.frmcenso.dirtrabpaga.disabled=false;
    //     document.frmcenso.cmb_ingresopaga.disabled=false;
    //     document.frmcenso.cedfampaga.focus();
    //   }else{
    //     document.frmcenso.cedfampaga.value="";
    //     document.frmcenso.nombpaga.value="";
    //     document.frmcenso.apellpaga.value="";
    //     document.frmcenso.cmb_parentesco.value="se";
    //     document.frmcenso.fechnacpaga.value="";
    //     document.frmcenso.cmb_edocivpaga.value="se";
    //     document.frmcenso.oficiopaga.value="";
    //     document.frmcenso.cmb_estudios.value="se";
    //     document.frmcenso.dirtrabpaga.value="";
    //     document.frmcenso.cmb_ingresopaga.value="0";
        
    //     document.frmcenso.cedfampaga.disabled=true;
    //     document.frmcenso.nombpaga.disabled=true;
    //     document.frmcenso.apellpaga.disabled=true;
    //     document.frmcenso.cmb_parentesco.disabled=true;
    //     document.frmcenso.fechnacpaga.disabled=true;
    //     document.frmcenso.cmb_edocivpaga.disabled=true;
    //     document.frmcenso.oficiopaga.disabled=true;
    //     document.frmcenso.cmb_estudios.disabled=true;
    //     document.frmcenso.dirtrabpaga.disabled=true;
    //     document.frmcenso.cmb_ingresopaga.disabled=true;
    //   }
    // }

    // function validar(){
    //     alert("Datos Incompletos");
    //     window.location.href="#tabs-1";
    //     return false;
    // }


  </script>

</html>