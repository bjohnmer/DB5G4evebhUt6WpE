<?php
  $URL_PRIN = $this->urlprin->getUrl();
?>
<!doctype html>
<html lang="es">
<head>
  <meta charset="UTF-8">
  <title>.:. SIBE .:.</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="author" href="http://tinyurl.com/qdg7ajv">
  <style>
    img
    {
      width:100%;
      height: auto;
    }
    table
    {
      border: thin solid #999;
      margin: 3em auto;
    }
    td 
    {
      border-bottom: thin solid #bbb;
    }
  </style>
  <script src="<?=base_url()?>jquery/jquery-1.8.2.js"></script>
  <script>
    $(document).ready(function(){
      $(".imprimir").click(function(){
        $(this).hide();
        $(".atras").hide();
        window.print();
        $(this).show();
        $(".atras").show();
      });
    });
  </script>

</head>
<body>
  <div id="main-container">
