<?php 
    $URL_PRIN = $this->urlprin->getUrl();
?>
<div id="menus">
  <h3>Registro</h3>
  <div class="menu_body">
    <a href="<?=$URL_PRIN?>admin">Inicio</a>
    <hr>
    <?php if ($this->session->userdata("tp_usuario")=="Administrador") { ?>
      <a href="<?=$URL_PRIN?>usuarios">Usuarios</a>
      <hr>
      <a href="<?=$URL_PRIN?>choferes">Choferes</a>
      <a href="<?=$URL_PRIN?>autobus">Transportes</a>
      <hr>
      <a href="<?=$URL_PRIN?>carreras">Carreras</a>
      <a href="<?=$URL_PRIN?>tpbecas">Tipos de Becas</a>
      <a href="<?=$URL_PRIN?>tpayudas">Tipos de Ayudas</a>
      <hr>
      <a href="<?=$URL_PRIN?>patologicos">Patologias</a>
      <!-- <a href="<?=$URL_PRIN?>ctabalance">Cuentas de Balance</a> -->
      <a href="<?=$URL_PRIN?>oficios">Oficios</a>
      <!-- <hr> -->
      <!-- <a href="#" onClick="alert('Modulo en Construcción')">Regiones</a> -->
      <hr>
      <a href="<?=$URL_PRIN?>noticias" >Noticias</a>

    <?php } ?>
    <?php if ($this->session->userdata("tp_usuario")=="Estudiante") { ?>
      <a href="<?=$URL_PRIN?>censo">Censo</a>
    <?php } ?>
    <?php if ($this->session->userdata("tp_usuario")=="Coordinador") { ?>
      <a href="<?=$URL_PRIN?>estudiante">Estudiantes</a>
    <?php } ?>
  </div>

  <h3>Gestion</h3>
  <div class="menu_body">
    <?php if ($this->session->userdata("tp_usuario")=="Administrador") { ?>
      <a href="#" onClick="alert('Modulo en Construcción')">Solicitudes</a>
    <?php } ?>
    <?php if ($this->session->userdata("tp_usuario")=="Estudiante") { ?>
      <a href="<?=$URL_PRIN?>solicitudAyuda">Solicitud Ayuda Ecónomica</a>
      <a href="<?=$URL_PRIN?>solicitudBeca">Solicitud Beca</a>
      <a href="<?=$URL_PRIN?>solicitudBus">Solicitud Transporte</a>
    <?php } ?>
    <?php if ($this->session->userdata("tp_usuario")=="Coordinador") { ?>
      <a href="<?=$URL_PRIN?>solicitudAyuda">Solicitud Ayuda Ecónomica</a>
      <a href="<?=$URL_PRIN?>solicitudBeca">Solicitud Beca</a>
      <a href="<?=$URL_PRIN?>solicitudBus">Solicitud de Transporte</a>
      <hr>
      <a href="#">Mantenimiento de Transporte</a>

    <?php } ?>
  </div>

  <h3>Reportes y Consultas</h3>
  <div class="menu_body">
    <?php if ($this->session->userdata("tp_usuario")=="Administrador") { ?>
      <!-- <a href="#" onClick="alert('Modulo en Construcción')">Bitacora de usuarios</a> -->
    <?php } ?>
    <?php if ($this->session->userdata("tp_usuario")=="Estudiante") { ?>
      <!-- <a href="#" onClick="alert('Modulo en Construcción')">Solicitud Ayuda Ecónomica</a>
      <a href="#" onClick="alert('Modulo en Construcción')">Solicitud Beca</a>
      <a href="#" onClick="alert('Modulo en Construcción')">Solicitud Transporte</a> -->
    <?php } ?>
    <?php if ($this->session->userdata("tp_usuario")=="Coordinador") { ?>
      <a href="<?=$URL_PRIN?>reportes/becarios" >Becarios</a>
      <a href="<?=$URL_PRIN?>reportes/censados" >Censados</a>
      <a href="<?=$URL_PRIN?>reportes/ayudasEconomicas" >Ayudas Ecónomicas</a>
      <a href="<?=$URL_PRIN?>reportes/discapacidad" >Con Discapacidad</a>
      <a href="<?=$URL_PRIN?>reportes/solicitudBus" >Uso de Bus</a>
    <?php } ?>
  </div>

  <h3>Sistema</h3>
  <div class="menu_body">
      <a href="#" onClick="alert('Modulo en Construcción')">Documentación de Ayuda</a>
      <?php if ($this->session->userdata("tp_usuario")=="Administrador") { ?>
        <!-- <a href="#" onClick="alert('Modulo en Construcción')">Configuración</a> -->
      <?php } ?>
      <a href="#" onClick="alert('Pendiente')">Acerca de Sibe</a>
      <hr>
      <!-- <a href="#" onClick="alert('Modulo en Construcción')">Cambio de Clave</a> -->
      <a href="<?=$URL_PRIN?>login/logout">Salir</a>
  </div>
</div>