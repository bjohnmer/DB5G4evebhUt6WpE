<?php
  $URL_PRIN = $this->urlprin->getUrl();
?>
<html>
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
</head>
<body>
  <div id="main-container">
<?php if (!empty($solicitud)): ?>
  <table border="0" align="center" width="90%" cellpadding="5" valign="top">
    <tr>
      <td colspan="2" align="center">
        <img src="<?=base_url()?>img/sibe_header.jpg" alt="" width="700px">
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <h1>
          <?php if (!empty($titulo)): ?>
            <?=$titulo?>
          <?php endif ?><br>
          <input class="atras" type="image" src="<?=base_url()?>css/themes/flexigrid/css/images/prev.gif" alt="Devolverse" title="Devolverse">
          <input class="imprimir" type="image" src="<?=base_url()?>css/themes/flexigrid/css/images/print.png" alt="Imprimir" title="Imprimir">
        </h1>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <h2>Comprobante</h2>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Solicitud N°:</strong></td>
      <td><?=$solicitud[0]->nro_solicitud_b?></td>
    </tr>
    <tr>
      <td align="right"><strong>Emitida en Fecha:</strong></td>
      <td><?=$solicitud[0]->fech_solicitud?></td>
    </tr>
    <tr>
      <td align="right"><strong>Cédula:</strong></td>
      <td><?=$estudiante[0]->ced_estudiante?></td>
    </tr>
    <tr>
      <td align="right"><strong>Apellidos:</strong></td>
      <td><?=$estudiante[0]->apellidos?></td>
    </tr>
    <tr>
      <td align="right"><strong>Nombres:</strong></td>
      <td><?=$estudiante[0]->nombres?></td>
    </tr>
    <tr>
      <td align="right"><strong>Tipo:</strong></td>
      <td><?php if (!empty($tipos)): ?>
            <?php foreach ($tipos as $tipo): ?>
              <?php if ($solicitud[0]->id_tipo == $tipo->id): ?> <?=$tipo->descrip_beca?> <?php endif ?> 
            <?php endforeach ?>
          <?php endif ?></td>
    </tr>
    

    <tr>
      <td align="right" valign="top"><strong>Recaudos:</strong></td>
      <td>
        <label class="checkbox">
          ( ) <input type="checkbox" name="comprobante" id="comprobante" value="1"> Comprobante de Solicitud<br>
        </label>
        <label class="checkbox">
          ( ) <input type="checkbox" name="solicitud" id="solicitud" value="1"> Formato de Solicitud de Beca<br>
        </label>
        <label class="checkbox">
          ( ) <input type="checkbox" name="partida" id="partida" value="1"> 2 Copias de la Partida de Nacimiento<br>
        </label>
        <label class="checkbox">
          ( ) <input type="checkbox" name="cedulas" id="cedulas" value="1"> 2 Copias de Cédula de Identidad o Pasaporte<br>
        </label>
        <label class="checkbox">
          ( ) <input type="checkbox" name="cartares" id="cartares" value="1"> Original y Copia de Carta de Residencia (Consejo Comunal o Junta Parroquial)<br>
        </label>
        <label class="checkbox">
          ( ) <input type="checkbox" name="fotos" id="fotos" value="1"> 2 Fotos recientes<br>
        </label>
        <label class="checkbox">
          ( ) <input type="checkbox" name="rusnies" id="rusnies" value="1"> 2 Copias RUSNIES<br>
        </label>
        <label class="checkbox">
          ( ) <input type="checkbox" name="notas" id="notas" value="1"> 2 Copias de las Notas<br>
        </label>
        <label class="checkbox">
          ( ) <input type="checkbox" name="buenacond" id="buenacond" value="1"> 2 Cartas de Buena Conducta: a) Prefectura y b) Universidad<br>
        </label>

      </td>
    </tr>
    <tr>
      <td colspan="2">
        <img src="<?=base_url()?>img/footer_sibe.jpg" alt="" width="700px">
      </td>
    </tr>
  </table>
<?php else: ?>
  <h3>El registro no fué encontrado</h3>
<?php endif ?>

    <footer>
      <?php if (!$this->session->userdata("logged_in")){ ?>
        <p><b> Equipo de Desarrollo: </b>
        <a href="#"> Gonzalo Calderas |</a>
        <a href="#"> Nelson Sánchez |</a>
        <a href="#"> Gustavo Núñez |</a>
        <a href="#"> Belfer Rivero </a></p> 
      <?php } else { ?>
        <p><b> Usuario: </b>
        <?=$this->session->userdata("tp_usuario")."/".$this->session->userdata("nombre")?>
        </p> 
      <?php } ?>
    </footer>
  </div>
</body>

</html>
