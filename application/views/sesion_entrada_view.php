<div id="info-contenido">
        <!-- <div width="150" border="0" cellpadding="0" cellspacing="0" bgcolor="#4758a8"> -->
      <div id="datos" class="fila">
        <div id="menu" class="col">
          <?php $this->load->view("menu_view"); ?>
        </div>
        <div id="info" class="col">
          <?php if (!empty($advertencia)) { ?>
            <div class="report-div error" id="report-error" style="display: block;">
              <p>
                Tenga en cuenta que el estudiante tiene Beca Asignada. La modificación del Status y/o El Índice Académico pudiera suspender la Beca
              </p>
            </div>
          <?php } ?>
          <?php if (!empty($output)): ?>
            <?php if (!empty($titulo)): ?>
              <h1>
              <?=$titulo?>
              </h1>
            <?php endif ?>
            <?=$output?>
          <?php elseif(!empty($noticias)): ?>
            <div class="noticias">
              <h1>
                Notificaciones
              </h1>
            <?php foreach ($noticias as $valor) { ?>
              <div class="noticia">
                <div class="titulo">
                  <h2>
                    <a href="<?=base_url()?>noticias/ver/<?=$valor->id_noticia?>"><?=$valor->titulo_noticia?></a>
                  </h2>
                </div>
                
                <img src="<?=base_url()?>assets/uploads/files/<?=$valor->img_noticia?>" alt="<?=$valor->titulo_noticia?>">
                <p class="resumen">
                  <?php if (!empty($completo)): ?>
                    <?=html_entity_decode($valor->descripcion_noticia)?>
                  <?php else: ?>
                    <?=substr(html_entity_decode($valor->descripcion_noticia), 0, 500);?>
                  ...&nbsp;&nbsp;&nbsp;<a href="<?=base_url()?>noticias/ver/<?=$valor->id_noticia?>">Leer más</a>
                  <?php endif ?>
                </p>

              </div>
            <?php } ?>
              <p>
                <?php if (!empty($completo)): ?>
                  <a href="<?=base_url()?>admin">Atrás</a>
                <?php else: ?>
                  <a href="<?=base_url()?>noticias/anteriores">Ver todas las noticias</a>
                <?php endif ?>
              </p>
            </div>

          <?php endif ?>
          <?php if (!empty($reportes)): ?>
            
            <h1><?=$titulo?></h1>
            <div style="width: 100%;" class="flexigrid crud-form">  
              <div class="mDiv">
                <div class="ftitle">
                  <div class="ftitle-left">
                    Seleccione las opciones para descargar el reporte
                  </div>      
                  <div class="clear"></div>
                </div>
              </div>
              <div id="main-table-box">
                <?php 
                  switch ($titulo) {
                    case 'REPORTE DE BECARIOS':
                      $go = "verReporteBecarios";
                      break;
                    case 'REPORTE DE AYUDAS ECONÓMICAS':
                      $go = "verReporteAE";
                      break;

                    default:
                      $go = "";
                      break;
                  }
                ?>

                <form accept-charset="utf-8" id="reportesForm" method="post" action="<?=base_url()?>reportes/<?=$go?>">

                  <div class="form-div">
                    <div id="general_field_box" class="form-field-box odd">
                      <div id="general_display_as_box" class="form-display-as-box">
                          General :
                      </div>
                      <div>
                        <input type="radio" value="general" id="general" name="seleccion" checked="checked">
                      </div>
                    </div>
                    <div id="general_field_box" class="form-field-box odd">
                      <div id="general_display_as_box" class="form-display-as-box">
                          Detallado :
                      </div>
                      <div>
                        <input type="radio" value="detallado" id="detallado" name="seleccion">
                      </div>
                    </div>

                    <?php if ($go == "verReporteBecarios"): ?>

                    <div id="tipo_beca_field_box" class="form-field-box odd">
                      <div id="tipo_beca_display_as_box" class="form-display-as-box">
                          Tipo de Beca :
                      </div>
                      <div>
                        <input type="checkbox" value="id_tipo" id="tipo" name="tipo">
                        <select name="id_tipo" placeholder="Seleccione un Tipo de Beca" class="<?php if (form_error('id_tipo')): ?>field_error<?php endif ?>" style="width:350px;" tabindex="2">
                          <?php if (!empty($tipos)): ?>
                            <?php foreach ($tipos as $tipo): ?>
                              <option value="<?=$tipo->id?>"><?=$tipo->descrip_beca?></option> 
                            <?php endforeach ?>
                          <?php endif ?>
                        </select>
                      </div>
                    </div>

                    <?php endif ?>

                    <?php if ($go == "verReporteAE"): ?>

                    <div id="tipo_beca_field_box" class="form-field-box odd">
                      <div id="tipo_beca_display_as_box" class="form-display-as-box">
                          Tipo de Ayuda Económica :
                      </div>
                      <div>
                        <input type="radio" value="id_tipo" id="tipo" name="seleccion">
                        <select name="id_tipo" placeholder="Seleccione un Tipo de Beca" class="<?php if (form_error('id_tipo')): ?>field_error<?php endif ?>" style="width:350px;" tabindex="2">
                          <?php if (!empty($tipos)): ?>
                            <?php foreach ($tipos as $tipo): ?>
                              <option value="<?=$tipo->id?>">
                                <?=$tipo->cod_ayuda?> - <?=$tipo->descrip_ayuda?>
                              </option> 
                            <?php endforeach ?>
                          <?php endif ?>
                        </select>
                      </div>
                    </div>

                    <?php endif ?>
                    
                    <div id="carrera_field_box" class="form-field-box odd">
                      <div id="carrera_display_as_box" class="form-display-as-box">
                          Carrera :
                      </div>
                      <div>
                        <input type="checkbox" value="id_carrera" id="carrera" name="carrera">
                        <select name="id_carrera" placeholder="Seleccione una Carrera" class="" style="width:350px;" tabindex="2">
                          <?php print_r($carreras) ?>
                          <?php if (!empty($carreras)): ?>
                            <?php foreach ($carreras as $carrera): ?>
                              <option value="<?=$carrera->id?>"><?=$carrera->descrip_carrera?></option> 
                            <?php endforeach ?>
                          <?php endif ?>
                        </select>
                      </div>
                    </div>
                    <div id="estatus_field_box" class="form-field-box odd">
                      <div id="estatus_display_as_box" class="form-display-as-box">
                          Estatus :
                      </div>
                      <div>
                        <input type="checkbox" value="status" id="status" name="statuschk">
                        <select name="status" placeholder="Seleccione una Carrera" class="" style="width:350px;" tabindex="2">
                          <option value="Solicitado">Solicitado</option>
                          <option value="En Tránsito">En Tránsito</option>
                          <option value="Aprobado">Aprobado</option>
                          <option value="Suspendido">Suspendido</option>
                          <option value="Rechazado">Rechazado</option>
                        </select>
                      </div>
                    </div>
                    <div id="genero_field_box" class="form-field-box odd">
                      <div id="genero_display_as_box" class="form-display-as-box">
                          Género :
                      </div>
                      <div>
                        <input type="checkbox" value="genero" id="genero" name="generochk">
                        <select name="genero" placeholder="Seleccione un Género" class="" style="width:350px;" tabindex="2">
                          <option value="Masculino">Masculino</option>
                          <option value="Femenino">Femenino</option>
                        </select>
                      </div>
                    </div>
                    <div id="genero_field_box" class="form-field-box odd">
                      <div id="genero_display_as_box" class="form-display-as-box">
                          Por Fecha :
                      </div>
                      <div>
                        <input type="checkbox" value="fecha" id="fecha" name="fecha">
                      </div>
                    </div>
                    <div id="genero_field_box" class="form-field-box odd">
                      <div id="genero_display_as_box" class="form-display-as-box">
                          Desde :
                      </div>
                      <div>
                        <div>
                          <input type="text" id="desde" name="desde" class="span3" readonly style=" width: 100px;">
                        </div>
                      </div>
                    </div>
                    <div id="genero_field_box" class="form-field-box odd">
                      <div id="genero_display_as_box" class="form-display-as-box">
                          Hasta :
                      </div>
                      <div>
                        <div>
                          <input type="text" id="hasta" name="hasta" class="span3" readonly style=" width: 100px;">
                        </div>
                      </div>
                    </div>
                    
                  </div>
                  <div class="pDiv">
                    <div class="form-button-box">
                      <input type="submit" class="btn btn-large" value="Guardar">
                    </div>        
                    <!-- 
                    <div class="form-button-box">
                      <input type="button" class="btn btn-large" onclick="window.location='http://dev.sibe/solicitudBus'" value="Cancelar">
                    </div>
 -->
                    <div class="clear"></div> 
                  </div>


                </form>

            </div>
          </div>


          <?php endif ?>
        </div>
      </div>
</div>