<?php
  $URL_PRIN = $this->urlprin->getUrl();
?>
    <div id="info-contenido">
        <!-- <div width="150" border="0" cellpadding="0" cellspacing="0" bgcolor="#4758a8"> -->
      <div id="datos" class="fila">
        <div id="menu" class="col">
          <?php $this->load->view("menu_view"); ?>
        </div>
        <div id="info" class="col">
          <?php if (!empty($titulo)): ?>
            <h1>
            <?=$titulo?>
            </h1>
          <?php endif ?>
          
          <div class="flexigrid crud-form" style="width: 100%;">  
            <div class="mDiv">
              <div class="ftitle">
                <div class="ftitle-left">
                  Agregar Solicitud de Ayuda
                </div>      
                <div class="clear"></div>
              </div>
            </div>
            <div id="main-table-box">
              <form action="<?=$URL_PRIN?>solicitudAyuda/insert" method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data" accept-charset="utf-8">
                <div class="form-div">
                  <div class="form-field-box odd" id="tp_patologia_field_box">
                    <div class="form-display-as-box" id="tp_patologia_display_as_box">
                        Tipo :
                    </div>
                    <div>
                      <select name="id_tipo" data-placeholder="Seleccione un Tipo de Ayuda" class="chzn-select <?php if (form_error('id_tipo')): ?>field_error<?php endif ?>" style="width:350px;" tabindex="2" >
                        <option value=""></option> 
                        <?php if (!empty($tipos)): ?>
                          <?php foreach ($tipos as $tipo): ?>
                            <option value="<?=$tipo->id?>"><?=$tipo->descrip_ayuda?></option> 
                          <?php endforeach ?>
                        <?php endif ?>
                      </select>
                    </div>
                  </div>

                  <div class="form-field-box odd" id="tp_patologia_field_box">
                    <div class="form-display-as-box" id="tp_patologia_display_as_box">
                        Motivo :
                    </div>
                    <div>
                      <textarea name="motivo" id="motivo" cols="30" rows="10" class="<?php if (form_error('motivo')): ?>field_error<?php endif ?>"><?=set_value("motivo"); ?></textarea>
                    </div>
                  </div>


                </div>
                <?php if (validation_errors()): ?>
                <div id="report-error" class="report-div error" style="display:block;">
                  <?=validation_errors()?>
                </div>
                <?php endif ?>
                <div class="pDiv">
                  <div class="form-button-box">
                    <input type="submit" value="Guardar" class="btn btn-large">
                  </div>        
                  <div class="form-button-box">
                    <input type="button" value="Cancelar" onclick="window.location='<?=$URL_PRIN?>solicitudAyuda'" class="btn btn-large">
                  </div>
                        
                  <div class="clear"></div> 
                </div>
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
  