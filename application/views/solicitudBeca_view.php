<?php 
    $URL_PRIN = $this->urlprin->getUrl();
?>
<div id="info-contenido">
        <!-- <div width="150" border="0" cellpadding="0" cellspacing="0" bgcolor="#4758a8"> -->
      <div id="datos" class="fila">
        <div id="menu" class="col">
          <?php $this->load->view("menu_view"); ?>
        </div>
        <div id="info" class="col">
              <?php if (!empty($titulo)): ?>
                <h1>
                <?=$titulo?>
                </h1>
              <?php endif ?>
<div class="report-div error" id="report-error"></div>
<div class="row">
  <div class="span8">
    <div class="btn-group">
      <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Ver <span class="caret"></span></button>
      <ul class="dropdown-menu">
        <li><a href="<?=$URL_PRIN?>solicitudBeca">Todos</a></li>
        <li><a href="<?=$URL_PRIN?>solicitudBeca/<?php if ($this->session->userdata('tp_usuario')=="Estudiante"): ?>misSolicitados<?php else: ?>verSolicitados<?php endif ?>">Solicitados</a></li>
        <li><a href="<?=$URL_PRIN?>solicitudBeca/<?php if ($this->session->userdata('tp_usuario')=="Estudiante"): ?>misTransitos<?php else: ?>verEnTransito<?php endif ?>">En Tránsito</a></li>
        <li class="divider"></li>
        <li><a href="<?=$URL_PRIN?>solicitudBeca/<?php if ($this->session->userdata('tp_usuario')=="Estudiante"): ?>misAprobados<?php else: ?>verAprobados<?php  endif ?>">Aprobados</a></li>
        <li><a href="<?=$URL_PRIN?>solicitudBeca/<?php if ($this->session->userdata('tp_usuario')=="Estudiante"): ?>misRechazados<?php else: ?>verRechazados<?php  endif ?>">Rechazados</a></li>
        <li class="divider"></li>
        <li><a href="<?=$URL_PRIN?>solicitudBeca/<?php if ($this->session->userdata('tp_usuario')=="Estudiante"): ?>misSuspendidos<?php else: ?>verSuspendidos<?php  endif ?>">Suspendidos</a></li>
      </ul>
    </div>
  </div>
</div>
<div class="row">
  <div class="span9">&nbsp;</div>
</div>
<div id="hidden-operations"></div>
<?php if (!empty($mensaje)) :?>
  <?php if ($mensaje['tipo']=="error") :?>
    <div class="report-div error" id="report-error" style="display:block;"><?=$mensaje['mensaje']?></div>
  <?php endif ?>
  
  <?php if ($mensaje['tipo']=="success") :?>
    <div class="report-div success report-list" style="display:block;" id="report-success"><p><?=$mensaje['mensaje']?></p></div>
  <?php endif ?>

<?php endif ?>

<div style="width: 100%;" class="flexigrid">
  <div class="mDiv">
      <form accept-charset="utf-8" autocomplete="off" id="filtering_form" method="post" action="<?=$URL_PRIN?>solicitudBeca/buscar"> 
    <?php if ($this->session->userdata("tp_usuario") == "Coordinador"): ?>
        <div id="quickSearchBox" class="sDiv_">
          <div class="sDiv2">
            Buscar: <input type="text" id="valor" size="30" name="valor" >
            <select id="campo" name="campo">
              <option value="nro_solicitud_b">Nro de Solicitud</option>
              <option value="ced_estudiante">Cédula del Solicitante</option>
              <option value="fech_solicitud">Fecha de Solicitud</option>
            </select>
            <button style="padding:.3em; padding-bottom:0.15em; margin:0px, line-height:1em; vertical-align:top;">Buscar</button>
          </div>
          <div class="search-div-clear-button">
            <button style="padding:.3em; padding-bottom:0.15em; margin:0px, line-height:1em; vertical-align:top;" onclick="window.location = '<?=$URL_PRIN?>solicitudBeca'">Limpiar Filtrados</button>
          </div>
        </div>
     <?php endif; ?>
      </form>
  
  </div>
  <div id="main-table-box">
    <div class="tDiv">
        <div class="tDiv2">
          <?php if ($this->session->userdata("tp_usuario") == "Estudiante"): ?>
            
            <?php if (!empty($verAgregar)): ?>
              <a class="add-anchor" title="Agregar Solicitud" href="<?=$URL_PRIN?>solicitudBeca/add">
                <div class="fbutton">
                  <div>
                    <span class="add">Agregar Solicitud</span>
                  </div>
                </div>
              </a>
            <?php endif ?>

            <a class="add-anchor" title="Imprimir/Descargar Formato de Solicitud" href="<?=$URL_PRIN?>assets/uploads/documentos/sb_unesr.pdf" target="_blank">
              <div class="fbutton">
                <div>
                  <span class="print">Imprimir/Descargar Formato de Solicitud</span>
                </div>
              </div>
            </a>
          <?php endif; ?>
          <div class="btnseparator"></div>
        </div>

        <!-- <div class="tDiv3">
                <a target="_blank" data-url="http://localhost/usuarios/index/export" class="export-anchor">
        <div class="fbutton">
          <div>
            <span class="export">Export</span>
          </div>
        </div>
            </a>
      <div class="btnseparator"></div>
                      <a data-url="http://localhost/usuarios/index/print" class="print-anchor">
        <div class="fbutton">
          <div>
            <span class="print">Print</span>
          </div>
        </div>
            </a>
      <div class="btnseparator"></div>
                  
    </div> -->
      <div class="clear"></div>
    </div>

    <div id="ajax_list">
      <div class="bDiv">
        <table border="0" cellspacing="0" cellpadding="0" id="flex1">
          <thead>
            <tr class="hDiv">
              <th width="15%">
                <div rel="nro_solicitud_a" class="text-left field-sorting ASC">
                  Nro de Solicitud          
                </div>
              </th>
              <?php if ($this->session->userdata("tp_usuario") == "Coordinador"): ?>
              <th width="20%">
                <div rel="nro_solicitud_a" class="text-left field-sorting ASC">
                  Solicitante
                </div>
              </th>
              <?php endif ?>
              <th width="10%">
                <div rel="fech_solicitud" class="text-left field-sorting ASC">
                  Fecha de Solicitud          
                </div>
              </th>
              <th width="20%">
                <div rel="descrip_ayuda" class="text-left field-sorting ASC">
                  Tipo de Beca          
                </div>
              </th>
              <th width="20%">
                <div rel="status" class="text-left field-sorting ASC">
                  Estatus          
                </div>
              </th>
              <th width="20%" align="left" class="" axis="col1" abbr="tools">
                <div class="text-right">
                  Acciones
                </div>
              </th>
            </tr>
          </thead>    
          <tbody>
            <?php if (!empty($solicitudes)):  $band = false; ?>
              <?php foreach ($solicitudes as $solicitud):?>
                <tr <?php if ($band): ?>class="erow"<?php $band = false; else: $band = true;?><?php endif ?>>
                  <td width="20%" class="sorted">
                    <div class="text-left"><?=$solicitud->nro_solicitud_b?></div>
                  </td>
                  <?php if ($this->session->userdata("tp_usuario") == "Coordinador"): ?>
                  
                  <td width="20%" class="sorted">
                    <?php 
                      $estud = $this->estudiante->getBy("id",$solicitud->id_estudiante);
                    ?>
                    <div class="text-left"><?=$estud[0]->ced_estudiante." ".$estud[0]->nombres." ".$estud[0]->apellidos?></div>
                  </td>
                  <?php endif ?>


                  <td width="20%" class="">
                    <div class="text-left"><?=$this->datemanager->date2normal($solicitud->fech_solicitud)?></div>
                  </td>
                  <td width="20%" class="">
                    <div class="text-left"><?=$solicitud->descrip_beca?></div>
                  </td>
                  <td width="20%" class="">
                    <div class="text-left"><?=$solicitud->status?></div>
                  </td>
                  <td width="20%" align="left">
                    <?php if ($solicitud->status == "Solicitado"): ?>
                      <?php if ($this->session->userdata("tp_usuario") == "Estudiante"): ?>
                        <div class="tools">
                          <a class="delete-row" title="Borrar Solicitud de Beca" href="<?=$URL_PRIN?>solicitudBeca/delete/<?=$solicitud->id_solicitud_b?>">
                            <span class="delete-icon"></span>
                          </a>
                          <a title="Editar Solicitud de beca" href="<?=$URL_PRIN?>solicitudBeca/edit/<?=$solicitud->id_solicitud_b?>">
                            <span class="edit-icon"></span>
                          </a>
                          <a title="Descargar PDF Comprobante de Solicitud de Beca" href="<?=$URL_PRIN?>solicitudBeca/comprobante_pdf/<?=$solicitud->id_solicitud_b?>">
                            <span class="download-icon"></span>
                          </a>
                          <a title="Emitir Comprobante de Solicitud de Beca" href="<?=$URL_PRIN?>solicitudBeca/comprobante/<?=$solicitud->id_solicitud_b?>">
                            <span class="printer-icon"></span>
                          </a>
                          <div class="clear"></div>
                        </div>
                      <?php endif ?>
                      <?php if ($this->session->userdata("tp_usuario") == "Coordinador"): ?>
                        <div class="tools">
                          <a title="Recaudos Solicitud de Beca" href="<?=$URL_PRIN?>solicitudBeca/recaudos/<?=$solicitud->id_solicitud_b?>">
                            <span class="requisito-icon"></span>
                          </a>
                          <div class="clear"></div>
                        </div>
                      <?php endif ?>
                    <?php endif ?>
                    <?php if ($solicitud->status == "En Tránsito"): ?>
                      <?php if ($this->session->userdata("tp_usuario") == "Coordinador"): ?>
                        <div class="tools">
                          <a title="Gestionar Solicitud de Beca" href="<?=$URL_PRIN?>solicitudBeca/gestionar/<?=$solicitud->id_solicitud_b?>">
                            <span class="gestion-icon"></span>
                          </a>
                          <div class="clear"></div>
                        </div>
                      <?php endif ?>
                    <?php endif ?>
                    <?php if ($solicitud->status == "Aprobado"): ?>
                      <?php if ($this->session->userdata("tp_usuario") == "Coordinador"): ?>
                        <div class="tools">
                          <a title="Suspender Beca" href="<?=$URL_PRIN?>solicitudBeca/suspender/<?=$solicitud->id_solicitud_b?>">
                            <span class="suspender-icon"></span>
                          </a>
                          <div class="clear"></div>
                        </div>
                      <?php endif ?>
                    <?php endif ?>
                    
                  </td>
                </tr>
              <?php endforeach ?>
            <?php else: ?>
              <tr>
                <td colspan="6">
                  <br>No Hay Solicitudes de Ayuda registradas<br>
                </td>
              </tr>
            <?php endif ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
        </div>
      </div>
</div>
