<?php
  $URL_PRIN = $this->urlprin->getUrl();
?>
<?php if (!empty($solicitud)): ?>
  <table border="0" align="center" width="90%" cellpadding="5" valign="top">
    <tr>
      <td colspan="2" align="center">
        <img src="<?=base_url()?>img/sibe_header.jpg" alt="">
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <h1>
          <?php if (!empty($titulo)): ?>
            <?=$titulo?>
          <?php endif ?><br>
          <input class="atras" type="image" src="<?=base_url()?>css/themes/flexigrid/css/images/prev.gif" alt="Devolverse" title="Devolverse">
          <input class="imprimir" type="image" src="<?=base_url()?>css/themes/flexigrid/css/images/print.png" alt="Imprimir" title="Imprimir">
        </h1>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center">
        <h2>Comprobante</h2>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Solicitud N°:</strong></td>
      <td><?=$solicitud[0]->nro_solicitud_a?></td>
    </tr>
    <tr>
      <td align="right"><strong>Emitida en Fecha:</strong></td>
      <td><?=$solicitud[0]->fech_solicitud?></td>
    </tr>
    <tr>
      <td align="right"><strong>Cédula:</strong></td>
      <td><?=$estudiante[0]->ced_estudiante?></td>
    </tr>
    <tr>
      <td align="right"><strong>Apellidos:</strong></td>
      <td><?=$estudiante[0]->apellidos?></td>
    </tr>
    <tr>
      <td align="right"><strong>Nombres:</strong></td>
      <td><?=$estudiante[0]->nombres?></td>
    </tr>
    <tr>
      <td align="right"><strong>Tipo:</strong></td>
      <td><?php if (!empty($tipos)): ?>
            <?php foreach ($tipos as $tipo): ?>
              <?php if ($solicitud[0]->id_tipo == $tipo->id): ?> <?=$tipo->descrip_ayuda?> <?php endif ?> 
            <?php endforeach ?>
          <?php endif ?></td>
    </tr>
    <tr>
      <td align="right"><strong>Motivo:</strong></td>
      <td>
        <p>
          <?=$solicitud[0]->motivo; ?>
        </p>
      </td>
    </tr>
    <tr>
      <td align="right"><strong>Recaudos:</strong></td>
      <td>
          <label class="checkbox">
            <input type="checkbox" name="comprobante" id="comprobante" value="1"> Comprobante de Solicitud<br>
          </label>
          <label class="checkbox">
            <input type="checkbox" name="carta" id="carta" value="1"> Carta de Exposición de Motivos<br>
          </label>
          <label class="checkbox">
            <input type="checkbox" name="informe" id="informe" value="1"> Informe Médico<br>
          </label>
          <label class="checkbox">
            <input type="checkbox" name="presupuestos" id="presupuestos" value="1"> Presupuestos (3)<br>
          </label>
          <label class="checkbox">
            <input type="checkbox" name="constancia" id="constancia" value="1"> Constancia de Estudios<br>
          </label>
          <label class="checkbox">
            <input type="checkbox" name="copiacedula" id="copiacedula" value="1"> Fotocopia de Cédula<br>
          </label>
      </td>
    </tr>
    <tr>
      <td colspan="2">
        <img src="<?=base_url()?>img/footer_sibe.jpg" alt="">
      </td>
    </tr>
  </table>
<?php else: ?>
  <h3>El registro no fué encontrado</h3>
<?php endif ?>
<script>
  $(document).ready(function(){
    $(".atras").click(function(){
      window.location = "<?=$URL_PRIN?>/solicitudAyuda"
    });
  });
</script>
  