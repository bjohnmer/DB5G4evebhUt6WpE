<?php 
    $URL_PRIN = $this->urlprin->getUrl();
?>
<?php 
if (!empty($URL_PRIN)) {
?>
<script src="<?=base_url()?>jquery/jquery-1.8.2.js"></script>
<script type="text/javascript">
    $('#save-and-go-back-button, .btn[value=Guardar]').click(function(){
      cedula  = $("#field-ced_chofer").val();
      $.post("<?=$URL_PRIN?>choferes/checkIsUnique", 
        { ced_chofer: cedula } , 
        function(data) {
          if (data.mensaje!="ok") 
          {
            alert(data.mensaje);
            return false;
          };
        },'json');
      });
</script>
<?php 
    }
?>
