<script type="text/javascript">
  $(document).ready(function(){
    $("#monto, input[name='forma_pago']").prop('disabled', true);
    $("input[name='status']").click(function(){
      // alert($(this).val());
      if ($(this).val()=="Aprobado") {
        $("#monto, input[name='forma_pago']").prop('disabled', false);
      }
      else
      {
        $("#monto, input[name='forma_pago']").prop('disabled', true);
      };
    });
  });
</script>