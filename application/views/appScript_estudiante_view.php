<?php 
    $URL_PRIN = $this->urlprin->getUrl();
?>
<?php 
if (!empty($URL_PRIN)) {
?>
<script src="<?=base_url()?>jquery/jquery-1.8.2.js"></script>
<script type="text/javascript">
    $('#save-and-go-back-button,.btn[value=Guardar]').click(function(){
      cedula  = $("#field-ced_estudiante").val();
      $.post("<?=$URL_PRIN?>estudiante/checkIsUnique", 
        { ced_estudiante: cedula } , 
        function(data) {
          if (data.mensaje!="ok") 
          {
            alert(data.mensaje);
            return false;
          };
        },'json');
      });

    $('#save-and-go-back-button,.btn[type=submit]').click(function(){
      <?php if (!empty($advertencia)) { ?>
        alert("Tenga en cuenta que el estudiante tiene Beca Asignada \n La modificación del Status y/o El Índice Académico pudiera suspender la Beca");
      <?php } ?>      
    });


</script>
<?php 
    }
?>
