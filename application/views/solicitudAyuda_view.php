<?php 
    $URL_PRIN = $this->urlprin->getUrl();
?>
<div id="info-contenido">
        <!-- <div width="150" border="0" cellpadding="0" cellspacing="0" bgcolor="#4758a8"> -->
      <div id="datos" class="fila">
        <div id="menu" class="col">
          <?php $this->load->view("menu_view"); ?>
        </div>
        <div id="info" class="col">
              <?php if (!empty($titulo)): ?>
                <h1>
                <?=$titulo?>
                </h1>
              <?php endif ?>


<div class="report-div error" id="report-error"></div>
<div class="row">
  <div class="span8">
    <div class="btn-group">
      <button data-toggle="dropdown" class="btn btn-primary dropdown-toggle">Ver <span class="caret"></span></button>
      <ul class="dropdown-menu">
        <li><a href="<?=$URL_PRIN?>solicitudAyuda">Todos</a></li>
        <li><a href="<?=$URL_PRIN?>solicitudAyuda/<?php if ($this->session->userdata('tp_usuario')=="Estudiante"): ?>misSolicitados<?php else: ?>verSolicitados<?php endif ?>">Solicitados</a></li>
        <li><a href="<?=$URL_PRIN?>solicitudAyuda/<?php if ($this->session->userdata('tp_usuario')=="Estudiante"): ?>misTransitos<?php else: ?>verEnTransito<?php endif ?>">En Tránsito</a></li>
        <li class="divider"></li>
        <li><a href="<?=$URL_PRIN?>solicitudAyuda/<?php if ($this->session->userdata('tp_usuario')=="Estudiante"): ?>misAprobados<?php else: ?>verAprobados<?php  endif ?>">Aprobados</a></li>
        <li><a href="<?=$URL_PRIN?>solicitudAyuda/<?php if ($this->session->userdata('tp_usuario')=="Estudiante"): ?>misRechazados<?php else: ?>verRechazados<?php  endif ?>">Rechazados</a></li>
      </ul>
    </div>
  </div>
</div>
<div class="row">
  <div class="span9">&nbsp;</div>
</div>
<div id="hidden-operations"></div>
<?php if (!empty($mensaje)) :?>
  <?php if ($mensaje['tipo']=="error") :?>
    <div class="report-div error" id="report-error" style="display:block;"><?=$mensaje['mensaje']?></div>
  <?php endif ?>
  
  <?php if ($mensaje['tipo']=="success") :?>
    <div class="report-div success report-list" style="display:block;" id="report-success"><p><?=$mensaje['mensaje']?></p></div>
  <?php endif ?>

<?php endif ?>
<div style="width: 100%;" class="flexigrid">
  <div class="mDiv">
    <div class="ftitle">
      &nbsp;
    </div>
  </div>
  <div id="main-table-box">
    <div class="tDiv">
        <div class="tDiv2">
          <?php if ($this->session->userdata("tp_usuario") == "Estudiante"): ?>
            <a class="add-anchor" title="Agregar Solicitud" href="<?=$URL_PRIN?>solicitudAyuda/add">
              <div class="fbutton">
                <div>
                  <span class="add">Agregar Solicitud</span>
                </div>
              </div>
            </a>
          <?php endif; ?>
          <div class="btnseparator"></div>
        </div>

        <!-- <div class="tDiv3">
                <a target="_blank" data-url="http://localhost/usuarios/index/export" class="export-anchor">
        <div class="fbutton">
          <div>
            <span class="export">Export</span>
          </div>
        </div>
            </a>
      <div class="btnseparator"></div>
                      <a data-url="http://localhost/usuarios/index/print" class="print-anchor">
        <div class="fbutton">
          <div>
            <span class="print">Print</span>
          </div>
        </div>
            </a>
      <div class="btnseparator"></div>
                  
    </div> -->
      <div class="clear"></div>
    </div>
    
    <div id="ajax_list">
      <div class="bDiv">
        <table border="0" cellspacing="0" cellpadding="0" id="flex1">
          <thead>
            <tr class="hDiv">
              <th width="20%">
                <div rel="nro_solicitud_a" class="text-left field-sorting ASC">
                  Nro de Solicitud          
                </div>
              </th>
              <th width="20%">
                <div rel="fech_solicitud" class="text-left field-sorting ASC">
                  Fecha de Solicitud          
                </div>
              </th>
              <th width="20%">
                <div rel="descrip_ayuda" class="text-left field-sorting ASC">
                  Tipo de Ayuda          
                </div>
              </th>
              <th width="20%">
                <div rel="status" class="text-left field-sorting ASC">
                  Estatus          
                </div>
              </th>
              <th width="20%" align="left" class="" axis="col1" abbr="tools">
                <div class="text-right">
                  Acciones
                </div>
              </th>
            </tr>
          </thead>    
          <tbody>
            <?php if (!empty($solicitudes)):  $band = false; ?>
              <?php foreach ($solicitudes as $solicitud):?>
                <tr <?php if ($band): ?>class="erow"<?php $band = false; else: $band = true;?><?php endif ?>>
                  <td width="20%" class="sorted">
                    <div class="text-left"><?=$solicitud->nro_solicitud_a?></div>
                  </td>
                  <td width="20%" class="">
                    <div class="text-left"><?=$solicitud->fech_solicitud?></div>
                  </td>
                  <td width="20%" class="">
                    <div class="text-left"><?=$solicitud->descrip_ayuda?></div>
                  </td>
                  <td width="20%" class="">
                    <div class="text-left"><?=$solicitud->status?></div>
                  </td>
                  <td width="20%" align="left">
                    <?php if ($solicitud->status == "Solicitado"): ?>
                      
                      <?php if ($this->session->userdata("tp_usuario") == "Estudiante"): ?>
                      <div class="tools">
                        <a class="delete-row" title="Borrar Solicitud de Ayuda" href="<?=$URL_PRIN?>solicitudAyuda/delete/<?=$solicitud->id_solicitud_ayuda?>">
                          <span class="delete-icon"></span>
                        </a>
                        <a title="Editar Solicitud de Ayuda" href="<?=$URL_PRIN?>solicitudAyuda/edit/<?=$solicitud->id_solicitud_ayuda?>">
                          <span class="edit-icon"></span>
                        </a>
                        <a title="Descargar PDF Comprobante de Solicitud de Ayuda" href="<?=$URL_PRIN?>solicitudAyuda/comprobante_pdf/<?=$solicitud->id_solicitud_ayuda?>">
                          <span class="download-icon"></span>
                        </a>
                        <a title="Emitir Comprobante de Solicitud de Ayuda" href="<?=$URL_PRIN?>solicitudAyuda/comprobante/<?=$solicitud->id_solicitud_ayuda?>">
                          <span class="printer-icon"></span>
                        </a>
                        <div class="clear"></div>
                      </div>
                      <?php endif ?>
                      <?php if ($this->session->userdata("tp_usuario") == "Coordinador"): ?>
                        <div class="tools">
                          <a title="Recaudos Solicitud de Ayuda" href="<?=$URL_PRIN?>solicitudAyuda/recaudos/<?=$solicitud->id_solicitud_ayuda?>">
                            <span class="requisito-icon"></span>
                          </a>
                          <div class="clear"></div>
                        </div>
                      <?php endif ?>

                    <?php elseif ($solicitud->status == "En Tránsito"): ?>
                      <?php if ($this->session->userdata("tp_usuario") == "Coordinador"): ?>
                        <div class="tools">
                          <a title="Gestionar Solicitud de Ayuda" href="<?=$URL_PRIN?>solicitudAyuda/gestionar/<?=$solicitud->id_solicitud_ayuda?>">
                            <span class="gestion-icon"></span>
                          </a>
                          <div class="clear"></div>
                        </div>
                      <?php endif ?>
                      
                    <?php endif ?>

                  </td>
                </tr>
              <?php endforeach ?>
            <?php else: ?>
              <tr>
                <td colspan="6">
                  <br>No Hay Solicitudes de Ayuda registradas<br>
                </td>
              </tr>
            <?php endif ?>
          </tbody>
        </table>
      </div>
    </div>
  <!-- 
  <form accept-charset="utf-8" autocomplete="off" id="filtering_form" method="post" action="http://localhost/usuarios/index/ajax_list"> 
  <div id="quickSearchBox" class="sDiv">
    <div class="sDiv2">
      Buscar: <input type="text" id="search_text" size="30" name="search_text" class="qsbsearch_fieldox">
      <select id="search_field" name="search_field">
        <option value="">Buscar todos</option>
                <option value="cedula">Cédula&nbsp;&nbsp;</option>
                <option value="nombre">Nombre&nbsp;&nbsp;</option>
                <option value="usuario">Usuario&nbsp;&nbsp;</option>
                <option value="tp_usuario">Tipo&nbsp;&nbsp;</option>
              </select>
            <input type="button" id="crud_search" value="Buscar"> 
    </div>
        <div class="search-div-clear-button">
          <input type="button" id="search_clear" value="Limpiar filtrados">
        </div>
  </div>
  <div class="pDiv">
    <div class="pDiv2">
      <div class="pGroup">
        <div title="Buscar" id="quickSearchButton" class="pSearch pButton">
          <span></span>
        </div>
      </div>
      <div class="btnseparator">
      </div>
      <div class="pGroup">
        <select id="per_page" name="per_page">
                      <option value="10">10&nbsp;&nbsp;</option>
                      <option selected="selected" value="25">25&nbsp;&nbsp;</option>
                      <option value="50">50&nbsp;&nbsp;</option>
                      <option value="100">100&nbsp;&nbsp;</option>
                  </select>
        <input type="hidden" value="cedula" id="hidden-sorting" name="order_by[0]">
        <input type="hidden" value="ASC" id="hidden-ordering" name="order_by[1]">
      </div>
      <div class="btnseparator">
      </div>
      <div class="pGroup">
        <div class="pFirst pButton first-button">
          <span></span>
        </div>
        <div class="pPrev pButton prev-button">
          <span></span>
        </div>
      </div>
      <div class="btnseparator">
      </div>
      <div class="pGroup">
        <span class="pcontrol">Pagina <input type="text" id="crud_page" size="4" value="1" name="page"> 
        de 
        <span id="last-page-number">1</span></span>
      </div>
      <div class="btnseparator">
      </div>
      <div class="pGroup">
        <div class="pNext pButton next-button">
          <span></span>
        </div>
        <div class="pLast pButton last-button">
          <span></span>
        </div>
      </div>
      <div class="btnseparator">
      </div>
      <div class="pGroup">
        <div id="ajax_refresh_and_loading" class="pReload pButton">
          <span></span>
        </div>
      </div>
      <div class="btnseparator">
      </div>
      <div class="pGroup">
        <span class="pPageStat">
            Mostrando <span id="page-starts-from">1</span> a <span id="page-ends-to">5</span> de <span id="total_items">5</span> registros            
        </span>
      </div>
    </div>
    <div style="clear: both;">
    </div>
  </div>
  </form> -->
  </div>
</div>
        </div>
      </div>
</div>
