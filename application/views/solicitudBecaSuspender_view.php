<?php
  $URL_PRIN = $this->urlprin->getUrl();
?>
    <div id="info-contenido">
        <!-- <div width="150" border="0" cellpadding="0" cellspacing="0" bgcolor="#4758a8"> -->
      <div id="datos" class="fila">
        <div id="menu" class="col">
          <?php $this->load->view("menu_view"); ?>
        </div>
        <div id="info" class="col">
          <?php if (!empty($titulo)): ?>
            <h1>
            <?=$titulo?>
            </h1>
          <?php endif ?>
          
          <div class="flexigrid crud-form" style="width: 100%;">  
            <div class="mDiv">
              <div class="ftitle">
                <div class="ftitle-left">
                  Suspender Beca
                </div>      
                <div class="clear"></div>
              </div>
            </div>
            <div id="main-table-box">

              <?php if (!empty($solicitud)): ?>
                <form action="<?=$URL_PRIN?>solicitudBeca/updSuspender" method="post" id="crudForm" autocomplete="off" enctype="multipart/form-data" accept-charset="utf-8">
                  <div class="form-div">
                    <div class="form-field-box odd" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Solicitud N°:
                      </div>
                      <div>
                        <input type="text" name="nro_solicitud_b" id="nro_solicitud_b" value="<?=$solicitud[0]->nro_solicitud_b?>" disabled>
                      </div>
                    </div>
                    
                    <div class="form-field-box even" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Emitida en Fecha:
                      </div>
                      <div>
                        <input type="text" name="fech_solicitud" id="fech_solicitud" value="<?=$this->datemanager->date2normal($solicitud[0]->fech_solicitud)?>" disabled>
                      </div>
                    </div>

                    <div class="form-field-box odd" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Cédula :
                      </div>
                      <div>
                        <input type="text" name="ced_estudiante" id="ced_estudiante" value="<?=$estudiante[0]->ced_estudiante?>" disabled>
                      </div>
                    </div>
                    <div class="form-field-box even" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Apellidos :
                      </div>
                      <div>
                        <input type="text" name="apellidos" id="apellidos" value="<?=$estudiante[0]->apellidos?>" disabled>
                      </div>
                    </div>
                    <div class="form-field-box odd" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Nombres :
                      </div>
                      <div>
                        <input type="text" name="nombres" id="nombres" value="<?=$estudiante[0]->nombres?>" disabled>
                      </div>
                    </div>

                    <div class="form-field-box even" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Tipo :
                      </div>
                      <div>
                        <select name="id_tipo" data-placeholder="Seleccione un Tipo de Beca" class="chzn-select <?php if (form_error('id_tipo')): ?>field_error<?php endif ?>" style="width:350px;" tabindex="2" disabled>
                          <option value=""></option> 
                          <?php if (!empty($tipos)): ?>
                            <?php foreach ($tipos as $tipo): ?>
                              <option value="<?=$tipo->id?>" <?php if ($solicitud[0]->id_tipo == $tipo->id): ?> selected <?php endif ?>><?=$tipo->descrip_beca?></option> 
                            <?php endforeach ?>
                          <?php endif ?>
                        </select>
                      </div>
                    </div>
                    
                    <div class="form-field-box odd" id="tp_patologia_field_box">
                      <div class="form-display-as-box" id="tp_patologia_display_as_box">
                          Motivo :
                      </div>
                      <div>
                        <textarea name="motivo_suspension" id="motivo_suspension" cols="30" rows="10" class="<?php if (form_error('motivo_suspension')): ?>field_error<?php endif ?>"><?=set_value("motivo_suspension"); ?></textarea>
                      </div>
                    </div>
                    
                    
                  </div>
                
                  <?php if (validation_errors()): ?>
                    <div id="report-error" class="report-div error" style="display:block;">
                      <?=validation_errors()?>
                    </div>
                  <?php endif ?>
                  <div class="pDiv">
                    <div class="form-button-box">
                      <input type="submit" value="Guardar" class="btn btn-large">
                    </div>        
                    <div class="form-button-box">
                      <input type="button" value="Cancelar" onclick="window.location='<?=$URL_PRIN?>solicitudBeca'" class="btn btn-large">
                    </div>
                          
                    <div class="clear"></div> 
                  </div>
                  <input type="hidden" name="id_solicitud_b" value="<?=$solicitud[0]->id_solicitud_b?>">
                </form>
              <?php else: ?>
                <h3>El registro no fué encontrado</h3>
              <?php endif ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
  