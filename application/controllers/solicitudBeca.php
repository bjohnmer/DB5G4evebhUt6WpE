<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SolicitudBeca extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('urlprin');
		$this->load->library('datemanager');

		$this->load->model('estudiante_model', 'estudiante');	
		$this->load->model('solicitudbeca_model', 'solicitudbeca');	
		$this->load->model('tpbecas_model', 'tiposBeca');	

	}


	public function index($data = null)
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				// $estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudbeca->getAll("id_estudiante",$this->session->userdata("id_estudiante"),"fech_solicitud","DESC");
				/*
					No solicitudes
					suspendidos o rechazadas
				*/
				$verNoSolicitudes = $this->solicitudbeca->getAll("id_estudiante",$this->session->userdata("id_estudiante"),"fech_solicitud","DESC");
				
				if (!$verNoSolicitudes) {
					$data['verAgregar']	= true;
				}
				else
				{
					$wheres = array(
								'id_estudiante' => $this->session->userdata("id_estudiante"),
								'status =' => "Aprobado"
						);
					$verA = $this->solicitudbeca->getWhere($wheres);
					
					$wheres = array(
								'id_estudiante' => $this->session->userdata("id_estudiante"),
								'status =' => "En Tránsito"
						);
					$verT = $this->solicitudbeca->getWhere($wheres);
					$wheres = array(
								'id_estudiante' => $this->session->userdata("id_estudiante"),
								'status =' => "Solicitado"
						);
					$verS = $this->solicitudbeca->getWhere($wheres);

					if (!$verS && !$verT && !$verA) {
						$data['verAgregar']	= true;
					}
				}
				
			elseif ($this->session->userdata("tp_usuario") == "Coordinador"):
				$solicitudes = $this->solicitudbeca->getAll(null,null,"fech_solicitud","DESC");
			endif;
			$data['solicitudes'] = $solicitudes;

			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}


	public function misSolicitados()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudbeca->getMy("status","Solicitado",$estudiante[0]->id);
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function verSolicitados()
	{

		try {
			$solicitudes = $this->solicitudbeca->getAll("status","Solicitado","fech_solicitud","DESC");
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function misTransitos()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudbeca->getMy("status","En Tránsito",$estudiante[0]->id);
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function verEnTransito()
	{

		try {
			$solicitudes = $this->solicitudbeca->getAll("status","En Tránsito","fech_solicitud","DESC");
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function verAprobados()
	{

		try {
			$solicitudes = $this->solicitudbeca->getAll("status","Aprobado","fech_solicitud","DESC");
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function misAprobados()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudbeca->getMy("status","Aprobado",$estudiante[0]->id);
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}


	public function verRechazados()
	{

		try {
			$solicitudes = $this->solicitudbeca->getAll("status","Rechazado","fech_solicitud","DESC");
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function misRechazados()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudbeca->getMy("status","Rechazado",$estudiante[0]->id);
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}


	public function misSuspendidos()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudbeca->getMy("status","Suspendido",$estudiante[0]->id);
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function buscar()
	{
		
		try {
			
			$this->load->library('form_validation');
		
			$this->form_validation->set_rules('valor', 'Valor de Búsqueda', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->index();
			}
			else
			{
				$solicitudes = null;
				switch ($this->input->post('campo')) {
					case 'ced_estudiante':
						$estudiante = $this->estudiante->getBy("ced_estudiante",$this->input->post('valor'));
						$solicitudes = $this->solicitudbeca->getBy("id_estudiante",$estudiante[0]->id);
						break;
					case 'fech_solicitud':
						$fech_solicitud = $this->input->post('valor');
						$esfecha = $this->datemanager->is_date($fech_solicitud);
						if ($esfecha) 
						{
							$campo = $this->input->post('campo');
							// echo "$campo";
							$fecha = $this->datemanager->date2mySQL($fech_solicitud);
							// echo "$fecha";
							$solicitudes = $this->solicitudbeca->getBy($this->input->post('campo'),$fecha);
							// print_r($solicitudes);
						}
						break;
					default:
						$solicitudes = $this->solicitudbeca->getBy($this->input->post('campo'),$this->input->post('valor'));
						break;
				}
				$data['solicitudes'] = $solicitudes;
				$this->salida($data);
			}
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function verSuspendidos()
	{

		try {
			$solicitudes = $this->solicitudbeca->getAll("status","Suspendido","fech_solicitud","DESC");
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}



	public function add($data = null)
	{

		try {

			$data['tipos'] = $this->tiposBeca->getAll();
			$this->salida($data,"solicitudBecaAdd_view");

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function insert()
	{

		try {

			$this->load->library('form_validation');
		
			$this->form_validation->set_rules('id_tipo', 'Tipo de Ayuda', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->add();
			}
			else
			{
				$tipo = $this->tiposBeca->getBy("id",$this->input->post("id_tipo"));
				$datos = array("promedio >="=>$tipo[0]->condicion, 'id' => $this->session->userdata("id_estudiante"));
				$puedeSolicitar = $this->estudiante->getWhere($datos);
				if ($puedeSolicitar) 
				{
					$correlativo = $this->solicitudbeca->maxNroSolicitudB() + 1;
					$data = array(
									'nro_solicitud_b' => $correlativo,
									'id_estudiante'=> $this->session->userdata("id_estudiante"),
									'id_tipo' 	=> $_POST['id_tipo'],
									'fech_solicitud'	=> date("Y-m-d")
									);
					$respuesta = $this->solicitudbeca->create($data);

					if ($respuesta)
					{
						$data["mensaje"]['tipo'] = "success";
						$data["mensaje"]['mensaje'] = "Los datos se almacenaron correctamente";
						$this->index($data);
					} 
					else
					{
						$data["mensaje"]['tipo'] = "error";
						$data['mensaje']['mensaje'] = 'Error: No se ha podido guardar';
						$this->add($data);
					}
				}
				else
				{
					$data["mensaje"]['tipo'] = "error";
					$data['mensaje']['mensaje'] = 'Error: Usted no puede solicitar éste tipo de beca, su promedio no es suficiente';
					$this->add($data);
				}
			}

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function edit($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudbeca->getBy("id_solicitud_b", $id);
			$data['tipos'] = $this->tiposBeca->getAll();
			$this->salida($data,"solicitudBecaEdit_view");
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function recaudos($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudbeca->getBy("id_solicitud_b", $id);
			$data['estudiante'] = $this->estudiante->getBy("id",$data["solicitud"][0]->id_estudiante);
			$data['tipos'] = $this->tiposBeca->getAll();
			$data['titulo'] = "Solicitud de Beca";
			$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
			$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
			$this->load->view('header_view', $estilos);
			$this->load->view("solicitudBecaRecaudos_view",$data);
			$this->load->view('footer_view',$jss);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function updateRecaudos()
	{

		try {

			$this->load->library('form_validation');

      $this->form_validation->set_rules("comprobante","Comprobante de Solicitud",'required');
      $this->form_validation->set_rules("solicitud","Formato de Solicitud",'required');
      $this->form_validation->set_rules("partida","Partida de Nacimiento",'required');
      $this->form_validation->set_rules("cedulas","Copias de Cédula",'required');
      $this->form_validation->set_rules("cartares","Carta de Residencia",'required');
      $this->form_validation->set_rules("fotos","Fotos",'required');
      $this->form_validation->set_rules("rusnies","Planilla RUSNIES",'required');
      $this->form_validation->set_rules("notas","Constancia de Notas",'required');
      $this->form_validation->set_rules("buenacond","Constancias de Buena Conducta",'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->recaudos($_POST['id_solicitud_b']);
			}
			else
			{

				$data = array(
										'id_solicitud_b'	=> $this->input->post('id_solicitud_b'),
										'comprobante' => $this->input->post('comprobante'),
										'solicitud' => $this->input->post('solicitud'),
										'partida' => $this->input->post('partida'),
										'cedulas' => $this->input->post('cedulas'),
										'cartares' => $this->input->post('cartares'),
										'fotos' => $this->input->post('fotos'),
										'rusnies' => $this->input->post('rusnies'),
										'notas' => $this->input->post('notas'),
										'buenacond' => $this->input->post('buenacond'),
										'status' => "En Tránsito"
								);

				$respuesta = $this->solicitudbeca->updateRecaudos($data);

				if ($respuesta) {
					$data["mensaje"]['tipo'] = "success";
					$data["mensaje"]['mensaje'] = "Los datos se modificaron correctamente";

					$data['tipos'] = $this->tiposBeca->getAll();
					
					$data['solicitudes'] = $this->solicitudbeca->getAll(null,null,"fech_solicitud","DESC");
					$data['tipos'] = $this->tiposBeca->getAll();
					$data['titulo'] = "Solicitud de Beca";
					$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
					$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
					$this->load->view('header_view', $estilos);
					$this->load->view("solicitudBeca_view",$data);
					$this->load->view('footer_view',$jss);

				} else {
					$data['mensaje'] = 'Error: No se ha podido guardar';
					$this->add_form($data);
				}
			}

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function gestionar($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudbeca->getBy("id_solicitud_b", $id);
			$data['estudiante'] = $this->estudiante->getBy("id",$data["solicitud"][0]->id_estudiante);
			$data['tipos'] = $this->tiposBeca->getAll();
			$data['montoBeca'] = $this->tiposBeca->getBy("id",$data['solicitud'][0]->id_tipo);
			$data['titulo'] = "Solicitud de Beca";
			$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
			$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
			$this->load->view('header_view', $estilos);
			$this->load->view("solicitudBecaGestion_view",$data);
			$this->load->view('footer_view',$jss);
			$this->load->view('appScriptAyuda_view',$jss);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function updGestionar()
	{

		try {

			$this->load->library('form_validation');

			$this->form_validation->set_rules('status','Estado de la Solicitud','required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->gestionar($_POST['id_solicitud_b']);
			}
			else
			{
				$data['status'] = $this->input->post('status');
				$respuesta = $this->solicitudbeca->update($data,$_POST['id_solicitud_b']);

				if ($respuesta) {
					$data["mensaje"]['tipo'] = "success";
					$data["mensaje"]['mensaje'] = "Los datos se modificaron correctamente";

					$data['tipos'] = $this->tiposBeca->getAll();
					
					$data['solicitudes'] = $this->solicitudbeca->getAll(null,null,"fech_solicitud","DESC");
					$data['tipos'] = $this->tiposBeca->getAll();
					$data['titulo'] = "Solicitud de Beca";
					$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
					$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
					$this->load->view('header_view', $estilos);
					$this->load->view("solicitudBeca_view",$data);
					$this->load->view('footer_view',$jss);

				} else {
					$data['mensaje'] = 'Error: No se ha podido guardar';
					$this->add_form($data);
				}
			}

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function suspender($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudbeca->getBy("id_solicitud_b", $id);
			$data['estudiante'] = $this->estudiante->getBy("id",$data["solicitud"][0]->id_estudiante);
			$data['tipos'] = $this->tiposBeca->getAll();
			$data['titulo'] = "Solicitud de Beca";
			$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
			$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
			$this->load->view('header_view', $estilos);
			$this->load->view("solicitudBecaSuspender_view",$data);
			$this->load->view('footer_view',$jss);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function updSuspender()
	{

		try {

			$this->load->library('form_validation');

      $this->form_validation->set_rules("motivo_suspension","Motivo de Suspensión",'required|min_length[5]');

			if ($this->form_validation->run() == FALSE)
			{
				$this->suspender($_POST['id_solicitud_b']);
			}
			else
			{

				$data = array(
										'motivo_suspension' => $this->input->post('motivo_suspension'),
										'status' => "Suspendido"
								);

				$respuesta = $this->solicitudbeca->update($data,$this->input->post('id_solicitud_b'));

				if ($respuesta) {
					$data["mensaje"]['tipo'] = "success";
					$data["mensaje"]['mensaje'] = "Los datos se modificaron correctamente";

					$data['tipos'] = $this->tiposBeca->getAll();
					
					$data['solicitudes'] = $this->solicitudbeca->getAll(null,null,"fech_solicitud","DESC");
					$data['tipos'] = $this->tiposBeca->getAll();
					$data['titulo'] = "Solicitud de Beca";
					$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
					$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
					$this->load->view('header_view', $estilos);
					$this->load->view("solicitudBeca_view",$data);
					$this->load->view('footer_view',$jss);

				} else {
					$data['mensaje'] = 'Error: No se ha podido guardar';
					$this->add_form($data);
				}
			}

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}


	// public function validmonto_check($str)
	// {
	// 	if (!preg_match('/^\d*+(?:\.\d*)?+$/', $str))
	// 	{
	// 		$this->form_validation->set_message('validmonto_check', 'El campo %s debe contener una cantidad válida');
	// 		return FALSE;
	// 	}
	// 	else
	// 	{
	// 		return TRUE;
	// 	}
	// }

	public function update()
	{

		try {

			$this->load->library('form_validation');
		
			$this->form_validation->set_rules('id_tipo', 'Tipo de Beca', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->edit($this->input->post('id_solicitud_b'));
			}
			else
			{
				$data = array('id_tipo' => $_POST['id_tipo']);

				$respuesta = $this->solicitudbeca->update($data,$_POST['id_solicitud_b']);

				if ($respuesta) {
					$data["mensaje"]['tipo'] = "success";
					$data["mensaje"]['mensaje'] = "Los datos se modificaron correctamente";

					$this->index($data);

				} else {
					$data["mensaje"]['tipo'] = "error";
					$data['mensaje']['mensaje'] = 'Error: No se ha podido guardar';
					$this->edit($data);
				}
			}
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function delete()
	{

		try {
			$solicitud = $this->solicitudbeca->destroy($this->uri->segment(3));
			if ($solicitud) {
				$data['mensaje']['tipo']="success";
				$data['mensaje']['mensaje'] = "La solicitud se ha eliminado correctamente";
			}
			else
			{
				$data['mensaje']['tipo']="error";
				$data['mensaje']['mensaje'] = "Error: No se ha podido eliminar";
			}
			
			$this->index($data);
		
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}


	function salida($data = null, $vista = "solicitudBeca_view")
	{
		$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
		$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
		$data['titulo'] = "Solicitud de Beca";
		$this->load->view('header_view', $estilos);
		$this->load->view($vista,$data);
		$this->load->view('appScript_tpayudas_view');
		$this->load->view('footer_view',$jss);
		
	}


	public function comprobante($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudbeca->getBy("id_solicitud_b", $id);
			$data['estudiante'] = $this->estudiante->getBy("id",$data["solicitud"][0]->id_estudiante);
			$data['tipos'] = $this->tiposBeca->getAll();
			$data['titulo'] = "Solicitud de Beca";
			$this->load->view('header_reportes_view');
			$this->load->view("comprobanteBeca_view",$data);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function comprobante_pdf($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudbeca->getBy("id_solicitud_b", $id);
			// print_r($data);
			$data['solicitud'][0]->fech_solicitud=$this->datemanager->date2normal($data['solicitud'][0]->fech_solicitud);

			$data['estudiante'] = $this->estudiante->getBy("id",$data["solicitud"][0]->id_estudiante);

			$data['tipos'] = $this->tiposBeca->getAll();
			$data['titulo'] = "Solicitud de Beca";

			$this->load->library('pdf');
			$this->pdf->load_view("comprobanteBecaPDF_view",$data);
			$this->pdf->render();
			$this->pdf->stream('solicBeca_'.$data['solicitud'][0]->nro_solicitud_b.'.pdf');

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	
}