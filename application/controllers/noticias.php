<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Noticias extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('grocery_CRUD');	
		$this->load->library('urlprin');
	}

	public function index()
	{

		try {
			$crud = new grocery_CRUD();
			$crud->set_table('noticias');
			$crud->set_subject('Noticia');
			$crud->columns('fecha_noticia', 'titulo_noticia', 'img_noticia');
			
			$crud->order_by('fecha_noticia','DESC');
			
			$crud->display_as('titulo_noticia', 'Título');
			$crud->display_as('descripcion_noticia', 'Descripción');
			$crud->display_as('img_noticia', 'Imágen');
			$crud->display_as('fecha_noticia', 'Fecha');
			
			$crud->set_rules('titulo_noticia', 'Título de la Noticia', 'required|min_length[5]');
			$crud->set_rules('descripcion_noticia', 'Descripción de la Noticia', 'required');
			
			$crud->field_type('fecha_noticia', 'hidden', date('Y-m-d h:i:s'));

			$crud->set_field_upload('img_noticia','assets/uploads/files');

			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->salida($output);
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function ver()
	{

		try {

			$seg = $this->uri->segment(3);
			if (!empty($seg)) 
			{

				$data['noticias'] = $this->db->where("id_noticia", $this->uri->segment(3))->get("noticias")->result();
				$data['completo'] = true;

			}
			else
			{

				$data['noticias'] = $this->db->limit(3)->order_by("fecha_noticia","DESC")->get("noticias")->result();

			}
			$this->load->view('header_view');
			$this->load->view('sesion_entrada_view',$data);
			$this->load->view('footer_view');

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function anteriores()
	{

		try {

			$data['noticias'] = $this->db->order_by("fecha_noticia","DESC")->get("noticias")->result();
			$this->load->view('header_view');
			$this->load->view('sesion_entrada_view',$data);
			$this->load->view('footer_view');
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	function salida($output = null)
	{
		
		$data['titulo'] = "Noticias";
		$this->load->view('header_view', $output);
		$this->load->view('sesion_entrada_view', $data);
		$this->load->view('footer_view');
		
	}

}