<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SolicitudBus extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('urlprin');
		$this->load->model('estudiante_model', 'estudiante');	
		$this->load->model('solicitudbus_model', 'solicitudbus');	
		$this->load->model('autobus_model', 'bus');	
		$this->load->model('choferes_model', 'chofer');

		$this->chofer->updateStatus();
		$this->bus->updateStatus();
		$this->solicitudbus->updateStatus();
	}

	public function index()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudbus->getAll("id_solicitante",$estudiante[0]->id,"fech_solicitud","DESC");
			elseif ($this->session->userdata("tp_usuario") == "Coordinador"):
				$solicitudes = $this->solicitudbus->getAll(null,null,"fech_solicitud","DESC");
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function misSolicitados()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudbus->getMy("bus_solicitud.status","Solicitado",$estudiante[0]->id);
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function verSolicitados()
	{

		try {
			$solicitudes = $this->solicitudbus->getAll("bus_solicitud.status","Solicitado","fech_solicitud","DESC");
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}


	public function verAprobados()
	{

		try {
			$solicitudes = $this->solicitudbus->getAll("bus_solicitud.status","Aprobado","fech_solicitud","DESC");
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function misAprobados()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudbus->getMy("bus_solicitud.status","Aprobado",$estudiante[0]->id);
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}


	public function verRechazados()
	{

		try {
			$solicitudes = $this->solicitudbus->getAll("bus_solicitud.status","Rechazado","fech_solicitud","DESC");
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function misRechazados()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudbus->getMy("bus_solicitud.status","Rechazado",$estudiante[0]->id);
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}


	public function add()
	{

		try {
			$data['titulo'] = "Solicitud de Transporte";
			$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
			$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
			$this->load->view('header_view', $estilos);
			$this->load->view("solicitudBusAdd_view",$data);
			$this->load->view('footer_view',$jss);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function date2mySQL($date)
	{
		$fecha = explode("/", $date);
		return $fecha[2]."-".$fecha[1]."-".$fecha[0];
	}

	public function date2normal($date)
	{
		$fecha = explode("-", $date);
		return $fecha[2]."/".$fecha[1]."/".$fecha[0];
	}

	public function insert()
	{

		try {

			$this->load->library('form_validation');
		
			$this->form_validation->set_rules('organizacion', 'Organización', 'required|max_length[75]');
			$this->form_validation->set_rules('ruta_destino', 'Ruta', 'required|max_length[75]');
			$this->form_validation->set_rules('motivo', 'Motivo', 'required|min_length[10]');
			$this->form_validation->set_rules('fech_salida', 'Fecha de Salida', 'required');
			$this->form_validation->set_rules('hora_salida', 'Hora de Salida', 'required|min_length[5]');
			$this->form_validation->set_rules('fech_regreso', 'Fecha de Regreso', 'required');

			if ($this->form_validation->run() == FALSE)
			{
				$this->add();
			}
			else
			{
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$correlativo = $this->solicitudbus->maxNroSolicitudT() + 1;
				$data = array( 'nro_solicitud_t'=> $correlativo,
											 'id_solicitante'	=> $estudiante[0]->id,
											 'organizacion'		=> $_POST['organizacion'],
											 'ruta_destino'		=> $_POST['ruta_destino'],
											 'motivo'					=> $_POST['motivo'],
											 'fech_salida'		=> $this->date2mySQL($_POST['fech_salida']),
											 'hora_salida'		=> $_POST['hora_salida'],
											 'fech_regreso'		=> $this->date2mySQL($_POST['fech_regreso']),
											 'fech_solicitud'	=> date("Y-m-d"));
				$respuesta = $this->solicitudbus->create($data);

				if ($respuesta) {
					$data["mensaje"]['tipo'] = "success";
					$data["mensaje"]['mensaje'] = "Los datos se almacenaron correctamente";

					$solicitudes = $this->solicitudbus->getBy("id_solicitante",$estudiante[0]->id);
					
					$data['titulo'] = "Solicitud de Transporte";
					$data['solicitudes'] = $solicitudes;
					$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
					$jss['js_estilos'][0]	= base_url()."css/themes/flexigrid/js/flexigrid.js";
					$this->load->view('header_view', $estilos);
					$this->load->view("solicitudBus_view",$data);
					$this->load->view('footer_view',$jss);

				} else {
					$data['mensaje'] = 'Error: No se ha podido guardar';
					$this->add($data);
				}
			}

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function edit($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudbus->getBy("id_bus_solicitud", $id);
			$data['solicitud'][0]->fech_regreso=$this->date2normal($data['solicitud'][0]->fech_regreso);
			$data['solicitud'][0]->fech_salida=$this->date2normal($data['solicitud'][0]->fech_salida);
			$data['titulo'] = "Solicitud de Transporte";
			$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
			$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
			$this->load->view('header_view', $estilos);
			$this->load->view("solicitudBusEdit_view",$data);
			$this->load->view('footer_view',$jss);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function gestionar($segment = null, $data = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudbus->getBy("id_bus_solicitud", $id);
			$data['choferes'] = $this->solicitudbus->getChoferNotIn($data['solicitud'][0]->fech_salida,$data['solicitud'][0]->fech_regreso);
			// print_r($data['choferes']);
			$data['buses'] = $this->solicitudbus->getBusNotIn($data['solicitud'][0]->fech_salida,$data['solicitud'][0]->fech_regreso);
			
			$data['solicitud'][0]->fech_solicitud=$this->date2normal($data['solicitud'][0]->fech_solicitud);
			$data['solicitud'][0]->fech_salida=$this->date2normal($data['solicitud'][0]->fech_salida);
			$data['solicitud'][0]->fech_regreso=$this->date2normal($data['solicitud'][0]->fech_regreso);
			
			$data['estudiante'] = $this->estudiante->getBy("id",$data["solicitud"][0]->id_solicitante);
			
			$data['titulo'] = "Solicitud de Transporte";
			$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
			$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
			$this->load->view('header_view', $estilos);
			$this->load->view("solicitudBusGestion_view",$data);
			$this->load->view('footer_view',$jss);
			$this->load->view('appScriptAyudaBus_view',$jss);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function updGestionar()
	{

		try {

			$this->load->library('form_validation');
			// $this->form_validation->set_message('decimal', 'El campo %s debe contener una cantidad válida');
			$this->form_validation->set_rules('status','Estado de la Solicitud','required');
			if (!empty($_POS['status'])) :
				if ($_POS['status'] == 'Aprobado'):
					$this->form_validation->set_rules('id_bus','Bus','required');
					$this->form_validation->set_rules('id_chofer','Chofer','required');
				endif;
			endif;

			if ($this->form_validation->run() == FALSE)
			{
				$this->gestionar($_POST['id_bus_solicitud']);
			}
			else
			{

				$data['status'] = $_POST['status'];
				if ($_POST['status']=="Aprobado") { 
					$data['id_bus'] = $_POST['id_bus']; 
					$data['id_chofer'] = $_POST['id_chofer'];
				}

				$respuesta = $this->solicitudbus->update($data,$_POST['id_bus_solicitud']);

				if ($respuesta) {

					$data["mensaje"]['tipo'] = "success";
					$data["mensaje"]['mensaje'] = "La Solicitud se ha gestionado correctamente";
					$data['solicitudes'] = $this->solicitudbus->getAll(null,null,"fech_solicitud","DESC");
					$data['titulo'] = "Solicitud de Transporte";
					$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
					$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
					$this->load->view('header_view', $estilos);
					$this->load->view("solicitudBus_view",$data);
					$this->load->view('footer_view',$jss);

				} else {
					$data['mensaje'] = 'Error: No se ha podido guardar';
					$this->gestionar($_POST['id_solicitud_ayuda'],$data);
				}
			}

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function update()
	{

		try {

			$this->load->library('form_validation');
		
			$this->form_validation->set_rules('organizacion', 'Organización', 'required|max_length[75]');
			$this->form_validation->set_rules('ruta_destino', 'Ruta', 'required|max_length[75]');
			$this->form_validation->set_rules('motivo', 'Motivo', 'required|min_length[10]');
			$this->form_validation->set_rules('fech_salida', 'Fecha de Salida', 'required');
			$this->form_validation->set_rules('hora_salida', 'Hora de Salida', 'required|min_length[5]');
			$this->form_validation->set_rules('fech_regreso', 'Fecha de Regreso', 'required');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->edit($_POST['id_bus_solicitud']);
			}
			else
			{
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$data = array( 'organizacion'		=> $_POST['organizacion'],
											 'ruta_destino'		=> $_POST['ruta_destino'],
											 'motivo'					=> $_POST['motivo'],
											 'fech_salida'		=> $this->date2mySQL($_POST['fech_salida']),
											 'hora_salida'		=> $_POST['hora_salida'],
											 'fech_regreso'		=> $this->date2mySQL($_POST['fech_regreso']),
											 'fech_solicitud'	=> date("Y-m-d"));
				$respuesta = $this->solicitudbus->update($data,$_POST['id_bus_solicitud']);

				if ($respuesta) {
					$data["mensaje"]['tipo'] = "success";
					$data["mensaje"]['mensaje'] = "Los datos se almacenaron correctamente";

					$solicitudes = $this->solicitudbus->getBy("id_solicitante",$estudiante[0]->id);
					
					$data['titulo'] = "Solicitud de Transporte";
					$data['solicitudes'] = $solicitudes;
					$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
					$jss['js_estilos'][0]	= base_url()."css/themes/flexigrid/js/flexigrid.js";
					$this->load->view('header_view', $estilos);
					$this->load->view("solicitudBus_view",$data);
					$this->load->view('footer_view',$jss);

				} else {
					$data['mensaje'] = 'Error: No se ha podido guardar';
					$this->add($data);
				}
			}
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function delete()
	{

		try {
			$solicitud = $this->solicitudbus->destroy($this->uri->segment(3));
			if ($solicitud) {
				$data['mensaje']['tipo']="success";
				$data['mensaje']['mensaje'] = "La solicitud se ha eliminado correctamente";
			}
			else
			{
				$data['mensaje']['tipo']="error";
				$data['mensaje']['mensaje'] = "Error: No se ha podido eliminar";
			}
			$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
			$solicitudes = $this->solicitudbus->getBy("id_solicitante",$estudiante[0]->id);

			$data['solicitudes'] = $solicitudes;

			$this->salida($data);
		
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}


	function salida($data = null, $vista = "solicitudBus_view")
	{
		$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
		$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
		$data['titulo'] = "Solicitud de Transporte";
		$this->load->view('header_view', $estilos);
		$this->load->view($vista,$data);
		$this->load->view('appScript_tpayudas_view');
		$this->load->view('footer_view',$jss);
		
	}

	public function comprobante($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudbus->getBy("id_bus_solicitud", $id);
			$data['solicitud'][0]->fech_regreso=$this->date2normal($data['solicitud'][0]->fech_regreso);
			$data['solicitud'][0]->fech_salida=$this->date2normal($data['solicitud'][0]->fech_salida);
			$data['solicitud'][0]->fech_solicitud=$this->date2normal($data['solicitud'][0]->fech_solicitud);

			$data['estudiante'] = $this->estudiante->getBy("id",$data["solicitud"][0]->id_solicitante);
			$data['titulo'] = "Solicitud de Transporte";
			$this->load->view('header_reportes_view');
			$this->load->view("comprobanteTransporte_view",$data);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}	

	public function comprobante_pdf($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudbus->getBy("id_bus_solicitud", $id);
			$data['solicitud'][0]->fech_regreso=$this->date2normal($data['solicitud'][0]->fech_regreso);
			$data['solicitud'][0]->fech_salida=$this->date2normal($data['solicitud'][0]->fech_salida);
			$data['solicitud'][0]->fech_solicitud=$this->date2normal($data['solicitud'][0]->fech_solicitud);

			$data['estudiante'] = $this->estudiante->getBy("id",$data["solicitud"][0]->id_solicitante);
			$data['titulo'] = "Solicitud de Transporte";
			
			//mb_internal_encoding("UTF-8"); 
			$this->load->library('pdf');
			$this->pdf->load_view("comprobanteTransportePDF_view",$data);
			$this->pdf->render();
			$this->pdf->stream('solicTransporte_'.$data['solicitud'][0]->nro_solicitud_t.'.pdf');

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}	
}