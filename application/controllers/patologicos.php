<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Patologicos extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('grocery_CRUD');
		$this->load->library('urlprin');
		$this->load->model('patologicos_model', 'patologicos');
	}

	public function index()
	{

		try {
			$crud = new grocery_CRUD();
			$crud->set_table('patologia');
			$crud->set_subject('Patologias');
			//$crud->columns('tp_patologia', 'cdo_patologia', 'descrip_patologia');
			$crud->columns('id','tp_patologia','descrip_patologia');

			$crud->add_fields('tp_patologia','descrip_patologia');
			$crud->edit_fields('tp_patologia','descrip_patologia');

			$crud->order_by('tp_patologia','ASC');
			$crud->display_as('tp_patologia', 'Tipo');
			//$crud->display_as('cdo_patologia', 'Codigo');
			$crud->display_as('id', 'Codigo');
			$crud->display_as('tp_patologia', 'Tipo de Patologia');
			$crud->display_as('descrip_patologia', 'Descripción');

			$crud->set_rules('tp_patologia', 'Tipo de Patologia', 'required');
			//$crud->set_rules('cdo_patologia', 'Codigo de la Patologia', 'required|alpha');
			$crud->set_rules('descrip_patologia', 'Descripción de la Patologia', 'required|min_length[5]');

			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->salida($output);
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function salida($output = null)
	{
		$data['titulo'] = "Patologias";
		$this->load->view('header_view', $output);
		$this->load->view('sesion_entrada_view', $data);
		//$this->load->view('appScript_patologicos_view');
		$this->load->view('footer_view');
	}

/*	function checkIsUnique()
	{
		extract($_POST);
		$dato['mensaje'] = "Ocurrió un error";
		if($this->patologicos->isUnique("cdo_patologia",$cdo_patologia))
		{
			$dato['mensaje'] = 'ok';
		}
		else
		{
			$dato['mensaje'] = 'Codigo de la Patologia ya se encuentra Registrada. Por favor verifique...';
		}
		echo json_encode($dato);
	}*/

	
}