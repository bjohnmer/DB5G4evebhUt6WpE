<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Estudiante extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('grocery_CRUD');
		$this->load->library('urlprin');

		$this->load->model("estados_model","estados");
		$this->load->model("municipios_model","municipios");
		$this->load->model("parroquias_model","parroquias");
		$this->load->model("carreras_model","carreras");
		$this->load->model("oficios_model","oficios");
		$this->load->model("patologicos_model","patologias");
		$this->load->model("estudiante_model","estudiantes");
		$this->load->model("academico_model","academicos");
		$this->load->model("familia_model","familias");
		$this->load->model("psicosocial_model","psicosocial");
		$this->load->model("destrezas_model","destrezas");
		$this->load->model("salud_model","salud");

		$this->load->model('estudiante_model', 'estudiante');
		$this->load->model('solicitudbeca_model', 'beca');
		$this->load->model('tpbecas_model', 'tiposBeca');
	}

	public function index()
	{

		try {
			$data = null;
			$wheres = array(
				"id_estudiante" => $this->uri->segment(4),
				"status" => "Aprobado"
				);
			$becado = $this->beca->getWhere($wheres);

			if ($becado) 
			{
				$data['advertencia'] = "ok"; 
			}

			$crud = new grocery_CRUD();
			
			$crud->set_table('estudiante');
			$crud->set_subject('Estudiante');
			$crud->columns('ced_estudiante','apellidos','nombres', 'fechanac','celular', 'email', 'promedio', 'estatus_estudiante');
			$crud->add_fields('ced_estudiante','apellidos','nombres','fechanac','celular', 'email', 'promedio');
			$crud->edit_fields('apellidos','nombres','fechanac','celular', 'email', 'promedio','estatus_estudiante');

			//$crud->add_action('Procesar', 'http://www.grocerycrud.com/assets/uploads/general/smiley.png', 'controllers/autobus');
 
			$crud->order_by('ced_estudiante','ASC');
			$crud->display_as('ced_estudiante', 'Cédula');
			$crud->display_as('apellidos', 'Apellidos');
			$crud->display_as('nombres', 'Nombres');
			$crud->display_as('fechanac', 'Fecha de Nacimiento');
			$crud->display_as('celular', 'Nº Celular');
			$crud->display_as('email', 'E-mail');
			$crud->display_as('promedio', 'Índice Académico');
			$crud->display_as('estatus_estudiante', 'Estatus');

			$crud->set_rules('ced_estudiante', 'Cédula del Estudiante', 'required|numeric|min_length[7]|max_length[8]');
			$crud->set_rules('apellidos', 'Apellidos del Estudiante', 'required|alpha_space|min_length[5]');
			$crud->set_rules('nombres', 'Nombres del Estudiante', 'required|alpha_space|min_length[5]');
			$crud->set_rules('fechanac', 'Fecha de Nacimiento', 'required|regex_match[/^\d{1,2}\/\d{1,2}\/\d{2,4}$/]');
			$crud->set_rules('celular', 'Nº del Celular del Estudiante', 'required|numeric|max_length[15]');
			$crud->set_rules('email', 'Correo Electronico', 'required|valid_email');
			$crud->set_rules('promedio', 'Índice Académico', 'decimal');

			$crud->callback_before_update(array($this,'statusBeca'));
			
			$crud->add_action('Censo', '', "estudiante/verCenso",'censo-icon');

			$output = $crud->render();
			
			$this->salida($output, $data);
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function verCenso()
	{
		$data['titulo'] = "Censo";
      $data["carreras"] = $this->carreras->getAll();
   $data["actividades"] = $this->oficios->getBy("tipo_oficio","Actividad");
   $data["habilidades"] = $this->oficios->getBy("tipo_oficio","Habilidad");
      $data["padecido"] = $this->patologias->getBy("tp_patologia","Enfermedad que ha padecido");
       $data["padeces"] = $this->patologias->getBy("tp_patologia","Enfermedad que padece");
       $data["vacunas"] = $this->patologias->getBy("tp_patologia","Vacuna");
    $data["estudiante"] = $this->estudiantes->getBy("id",$this->uri->segment(3));
     $data["academico"] = $this->academicos->getBy("id_estudiante",$this->uri->segment(3));
     if (!empty($data["academico"])) {
$data["academico"][0]->fech_ingreso = $this->datemanager->date2normal($data["academico"][0]->fech_ingreso);
     }

       $data["estados"] = $this->estados->getAll();
$data["municipios_hab"] = $this->municipios->getBy("id_estado",$data["estudiante"][0]->drhab_edo);
$data["parroquias_hab"] = $this->parroquias->getBy("id_municipio",$data["estudiante"][0]->drhab_mun);
$data["municipios_res"] = $this->municipios->getBy("id_estado",$data["estudiante"][0]->drres_edo);
$data["parroquias_res"] = $this->parroquias->getBy("id_municipio",$data["estudiante"][0]->drres_mun);

$bfam = array('id_estudiante' => $this->uri->segment(3), 'depende' => "Si");
// if (!empty($data['depende_familiar'])) {
  $data['depende_familiar'] = $this->familias->getByW($bfam);
    if ($data['depende_familiar']) {
        $data['depende_familiar'][0]->fech_nac_fam = $this->datemanager->date2normal($data["depende_familiar"][0]->fech_nac_fam);
    }
// }
// print_r($data['depende_familiar']);
$bfam = array('id_estudiante' => $this->uri->segment(3), 'depende' => "No");
$data['familia'] = $this->familias->getDistinctByW($bfam);


$bfam = array('id_estudiante' => $this->uri->segment(3), 'oficios.tipo_oficio' => "Actividad");
$data['destrezas_a'] = $this->destrezas->getByW($bfam);
$bfam = array('id_estudiante' => $this->uri->segment(3), 'oficios.tipo_oficio' => "Habilidad");
$data['destrezas_h'] = $this->destrezas->getByW($bfam);

$bfam = array('id_estudiante' => $this->uri->segment(3));
$data['psicosocial'] = $this->psicosocial->getByW($bfam);


$bfam = array(
	'id_estudiante' => $this->uri->segment(3),
	'tp_patologia' => 'Enfermedad que ha padecido'
	);
$data['pdcdo'] = $this->salud->getByW($bfam);

$bfam = array(
	'id_estudiante' => $this->uri->segment(3),
	'tp_patologia' => 'Enfermedad que padece'
	);
$data['pdc'] = $this->salud->getByW($bfam);

$bfam = array(
	'id_estudiante' => $this->uri->segment(3),
	'tp_patologia' => 'Vacuna'
	);
$data['vac'] = $this->salud->getByW($bfam);

$data['consultar'] = "Si";

$this->load->view('header_view', null);
$this->load->view('censo_view', $data);
$this->load->view('footer_view');
$this->load->view('appScriptCenso_view');


	}

	function statusBeca($post_array) 
	{

		$wheres = array(
			"id_estudiante" => $this->uri->segment(4),
			"status" => "Aprobado"
			);
		$becado = $this->beca->getWhere($wheres);

		if ($becado) 
		{
			$prom = $this->tiposBeca->getBy("id",$becado[0]->id_tipo);
			if ($post_array['promedio'] < $prom[0]->condicion)
			{
				$data= array(
					'motivo_suspension' => "Promedio Insuficiente",
					'status' => "Suspendido" 
					);
				$suspender = $this->beca->update($data,$becado[0]->id_solicitud_b);
			}
			else
			{
				if ($post_array['status'] == "Retirado")
				{
					$data= array(
					'motivo_suspension' => "Retirado de la Institución",
					'status' => "Suspendido" 
					);
					$suspender = $this->beca->update($data,$becado[0]->id_solicitud_b);		
				}
				else
				{
					if ($post_array['status'] == "Egresado")
					{
						$data= array(
							'motivo_suspension' => "Egresado de la Institución",
							'status' => "Suspendido" 
						);
						$suspender = $this->beca->update($data,$becado[0]->id_solicitud_b);
					}

				}
			}

		}

		
	  return $post_array;
	} 

	function salida($output = null,$data = null)
	{
		$data['titulo'] = "Estudiantes";
		$this->load->view('header_view', $output);
		$this->load->view('sesion_entrada_view',$data);
		$this->load->view('appScript_estudiante_view',$data);
		$this->load->view('footer_view');
	}
	
	function checkIsUnique()
	{
		extract($_POST);
		$dato['mensaje'] = "Ocurrió un error";
		if($this->estudiante->isUnique("ced_estudiante",$ced_estudiante))
		{
			$dato['mensaje'] = 'ok';
		}
		else
		{
			$dato['mensaje'] = 'Cédula del Estudiante ya registrada. Por favor verifique.';
		}
		echo json_encode($dato);
	}
	

}