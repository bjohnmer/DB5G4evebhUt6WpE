<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tpayudas extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('grocery_CRUD');
		$this->load->library('urlprin');
		$this->load->model('tpayudas_model', 'tpayudas');
	}

	public function index()
	{

		try {
			$crud = new grocery_CRUD();
			$crud->set_table('tipo_ayuda');
			$crud->set_subject('Ayuda');
			$crud->columns('id', 'descrip_ayuda');

			$crud->add_fields('descrip_ayuda');
			$crud->edit_fields('descrip_ayuda');

			//$crud->order_by('cod_ayuda','ASC');
			$crud->order_by('id','ASC');
			//$crud->display_as('cod_ayuda', 'Código');
			$crud->display_as('id', 'Código');
			$crud->display_as('descrip_ayuda', 'Descripción');

			//$crud->set_rules('cod_ayuda', 'Codigo de la Ayuda', 'required|alpha');
			$crud->set_rules('descrip_ayuda', 'Descripción de la Ayuda', 'required|alpha_space|min_length[5]');
					
			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->salida($output);
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	function salida($output = null)
	{
		$data['titulo'] = "Tipo de Ayudas";
		$this->load->view('header_view', $output);
		$this->load->view('sesion_entrada_view',$data);
		//$this->load->view('appScript_tpayudas_view');
		$this->load->view('footer_view');
	}
/*	function checkIsUnique()
	{
		extract($_POST);
		$dato['mensaje'] = "Ocurrió un errorrr";
		if($this->tpayudas->isUnique("cod_ayuda",$cod_ayuda))
		{
			$dato['mensaje'] = 'ok';
		}
		else
		{
			$dato['mensaje'] = 'Codigo de Tipo de Ayuda ya esta Registrado. Por favor verifique.';
		}
		echo json_encode($dato);
	}*/
	
}