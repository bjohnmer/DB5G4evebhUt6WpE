<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Usuarios extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');
		$this->load->library('Form_validation');
		$this->load->library('grocery_CRUD');
		$this->load->model('usuarios_model', 'usuarios');
		$this->load->library('urlprin');
	}

	public function index()
	{

		try {
			$crud = new grocery_CRUD();
			
			$crud->set_table('usuario');
			$crud->set_subject('Usuario');
			$crud->columns('cedula', 'nombre', 'usuario', 'tp_usuario');
			$crud->edit_fields('nombre','clave','confir');

			$crud->order_by('cedula','ASC');
			$crud->display_as('cedula', 'Cédula');
			$crud->display_as('tp_usuario', 'Tipo');
			$crud->display_as('confir', 'Confirmación');
			$crud->field_type('clave', 'password');
			$crud->field_type('confir', 'password');
			$crud->field_type('tp_usuario','enum', array('Administrador', 'Coordinador'));

			$crud->set_rules('cedula', 'Cédula del Usuario', 'required|numeric|min_length[7]|max_length[8]');
			$crud->set_rules('nombre', 'Nombre del Usuario', 'required|alpha_space|min_length[5]');
			$crud->set_rules('usuario', 'Login del Usuario', 'required|trim|alpha||min_length[5]|max_length[15]');
			$crud->set_rules('clave', 'Clave del Usuario', 'required|matches[confir]||min_length[5]');
			$crud->set_rules('confir', 'Confirmación de Clave del Usuario', 'required|matches[clave]');
			$crud->set_rules('tp_usuario', 'Tipo de Usuario', 'required');
			$crud->callback_before_insert(array($this,'encrypt_password'));
			$crud->callback_before_update(array($this,'encrypt_password'));

			$crud->callback_edit_field('clave',array($this,'set_clave_input_to_empty'));
  		$crud->callback_add_field('clave',array($this,'set_clave_input_to_empty'));

			$crud->callback_edit_field('confir',array($this,'set_confir_input_to_empty'));
  			$crud->callback_add_field('confir',array($this,'set_confir_input_to_empty'));
			
			$output = $crud->render();
			
			$this->salida($output);
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	function encrypt_password($post_array) {

	  $post_array['clave']  = md5($post_array['clave']);
	  $post_array['confir'] = md5($post_array['confir']);
	 
	  return $post_array;
	} 

	// Función a ejecutarse para que al momento de Agregar y Modificar el campo de Clave esté Vacío
  	function set_clave_input_to_empty() {
	  return "<input type='password' name='clave' value='' />";
	}

	// Función a ejecutarse para que al momento de Agregar y Modificar el campo de Confirmación de Clave esté Vacío
	function set_confir_input_to_empty() {
	    return "<input type='password' name='confir' value='' />";
	}

	function salida($output = null)
	{
		$data['titulo'] = "Usuarios";
		$this->load->view('header_view', $output);
		$this->load->view('sesion_entrada_view', $data);
		$this->load->view('appScript_usuarios_view');
		$this->load->view('footer_view');
	}

	function checkIsUnique()
	{
		extract($_POST);
		if($this->usuarios->isUnique("cedula",$cedula))
		{
			if($this->usuarios->isUnique("usuario",$usuario))
			{
				$dato['mensaje'] = 'ok';
			}
			else
			{
				$dato['mensaje'] = 'Nombre de Usuario ya se encuentra Registrado. Por favor intente con otro.';
			}
		}
		else
		{
			$dato['mensaje'] = 'Cédula de Usuario ya se encuentra registrada, por favor verifique...';
		}
		echo json_encode($dato);
	}

	
}