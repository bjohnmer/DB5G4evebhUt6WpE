<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Carreras extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('grocery_CRUD');	
		$this->load->library('urlprin');
		$this->load->model('carreras_model', 'carreras');	
	}

	public function index()
	{

		try {
			$crud = new grocery_CRUD();
			$crud->set_table('carreras');
			$crud->set_subject('Carrera');
			//$crud->columns('cod_carrera', 'descrip_carrera');
			$crud->columns('id', 'descrip_carrera');

			$crud->add_fields('descrip_carrera');
			$crud->edit_fields('descrip_carrera');

			//$crud->order_by('cod_carrera','ASC');
			$crud->order_by('id','ASC');
			//$crud->display_as('cod_carrera', 'Codigo');
			$crud->display_as('id', 'Codigo');
			$crud->display_as('descrip_carrera', 'Descripción');

			//$crud->set_rules('cod_carrera', 'Codigo de la Carrera', 'required|alpha');
			$crud->set_rules('descrip_carrera', 'Descripción de la Carrera', 'required|alpha_space|min_length[5]');

			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->salida($output);
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}


	}

	function salida($output = null)
	{
		$data['titulo'] = "Carreras";
		$this->load->view('header_view', $output);
		$this->load->view('sesion_entrada_view', $data);
		//$this->load->view('appScript_carreras_view');
		$this->load->view('footer_view');
	}
/*	function checkIsUnique()
	{
		extract($_POST);
		$dato['mensaje'] = "Ocurrió un error";
		if($this->carreras->isUnique("cod_carrera",$cod_carrera))
		{
			$dato['mensaje'] = 'ok';
		}
		else
		{
			$dato['mensaje'] = 'Codigo de Carrera ya registrado. Por favor verifique.';
		}
		echo json_encode($dato);
	}*/
	
}