<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Censo extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('grocery_CRUD');
		$this->load->library('urlprin');
		$this->load->library('datemanager');

		$this->load->model("estados_model","estados");
		$this->load->model("municipios_model","municipios");
		$this->load->model("parroquias_model","parroquias");
		$this->load->model("carreras_model","carreras");
		$this->load->model("oficios_model","oficios");
		$this->load->model("patologicos_model","patologias");
		$this->load->model("estudiante_model","estudiantes");
		$this->load->model("academico_model","academicos");
		$this->load->model("familia_model","familias");
		$this->load->model("psicosocial_model","psicosocial");
		$this->load->model("destrezas_model","destrezas");
		$this->load->model("salud_model","salud");

	}

	public function index($data = null)
	{
		try {

			$this->salida($data);
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	function salida($output = null)
	{

                    $data['titulo'] = "Censo";
                  $data["carreras"] = $this->carreras->getAll();
               $data["actividades"] = $this->oficios->getBy("tipo_oficio","Actividad");
               $data["habilidades"] = $this->oficios->getBy("tipo_oficio","Habilidad");
                  $data["padecido"] = $this->patologias->getBy("tp_patologia","Enfermedad que ha padecido");
                   $data["padeces"] = $this->patologias->getBy("tp_patologia","Enfermedad que padece");
                   $data["vacunas"] = $this->patologias->getBy("tp_patologia","Vacuna");
                
                $data["estudiante"] = $this->estudiantes->getBy("id",$this->session->userdata("id_estudiante"));
/*
                if (!empty($data["estudiante"][0]->id_discapacidad))
                {
                	
                }
*/


                 $data["academico"] = $this->academicos->getBy("id_estudiante",$this->session->userdata("id_estudiante"));
                 if (!empty($data["academico"])) {
$data["academico"][0]->fech_ingreso = $this->datemanager->date2normal($data["academico"][0]->fech_ingreso);
                 }

                   $data["estados"] = $this->estados->getAll();
            $data["municipios_hab"] = $this->municipios->getBy("id_estado",$data["estudiante"][0]->drhab_edo);
            $data["parroquias_hab"] = $this->parroquias->getBy("id_municipio",$data["estudiante"][0]->drhab_mun);
            $data["municipios_res"] = $this->municipios->getBy("id_estado",$data["estudiante"][0]->drres_edo);
            $data["parroquias_res"] = $this->parroquias->getBy("id_municipio",$data["estudiante"][0]->drres_mun);
            
            $bfam = array('id_estudiante' => $this->session->userdata("id_estudiante"), 'depende' => "Si");
          // if (!empty($data['depende_familiar'])) {
	          $data['depende_familiar'] = $this->familias->getByW($bfam);
  	        if ($data['depende_familiar']) {
	  	        $data['depende_familiar'][0]->fech_nac_fam = $this->datemanager->date2normal($data["depende_familiar"][0]->fech_nac_fam);
  	        }
          // }
          // print_r($data['depende_familiar']);
            $bfam = array('id_estudiante' => $this->session->userdata("id_estudiante"), 'depende' => "No");
          $data['familia'] = $this->familias->getDistinctByW($bfam);


            $bfam = array('id_estudiante' => $this->session->userdata("id_estudiante"), 'oficios.tipo_oficio' => "Actividad");
          $data['destrezas_a'] = $this->destrezas->getByW($bfam);
          $bfam = array('id_estudiante' => $this->session->userdata("id_estudiante"), 'oficios.tipo_oficio' => "Habilidad");
          $data['destrezas_h'] = $this->destrezas->getByW($bfam);

            $bfam = array('id_estudiante' => $this->session->userdata("id_estudiante"));
          $data['psicosocial'] = $this->psicosocial->getByW($bfam);


            $bfam = array(
            	'id_estudiante' => $this->session->userdata("id_estudiante"),
            	'tp_patologia' => 'Enfermedad que ha padecido'
            	);
          $data['pdcdo'] = $this->salud->getByW($bfam);

          $bfam = array(
            	'id_estudiante' => $this->session->userdata("id_estudiante"),
            	'tp_patologia' => 'Enfermedad que padece'
            	);
          $data['pdc'] = $this->salud->getByW($bfam);

          $bfam = array(
            	'id_estudiante' => $this->session->userdata("id_estudiante"),
            	'tp_patologia' => 'Vacuna'
            	);
          $data['vac'] = $this->salud->getByW($bfam);


		$this->load->view('header_view', $output);
		$this->load->view('censo_view', $data);
		$this->load->view('footer_view');
		$this->load->view('appScriptCenso_view');
	}

	public function getMunicipios()
	{
		$data = $this->municipios->getBy("id_estado",$this->input->post("estado"));
		$opciones="";
		foreach ($data as $opcion) {
			$opciones .= "<option value='".$opcion->id_municipio."'>".$opcion->nombre_municipio."</option>";
		}

		echo json_encode($opciones);
	}

	public function getParroquias()
	{
		$data = $this->parroquias->getBy("id_municipio",$this->input->post("municipio"));
		$opciones="";
		foreach ($data as $opcion) {
			$opciones .= "<option value='".$opcion->id_parroquia."'>".$opcion->nombre_parroquia."</option>";
		}
		echo json_encode($opciones);
	}

	public function getDiscap()
	{
		$data = $this->db
				->where('tipo_discapacidad',$this->input->post('tipo_discapacidad'))
				->get('discapacidad')
				->result();

		$opciones="";
		foreach ($data as $opcion) {
			$opciones .= "<option value='".$opcion->id_discapacidad."'>".$opcion->subtipo_discapacidad."</option>";
		}
		echo json_encode($opciones);
	}

	public function insert()
	{
		try {

			$this->form_validation->set_rules('edad','Edad','integer|greater_than[15]|max_length[2]');
			$this->form_validation->set_rules('lugnac','Lugar de Nacimiento','alpha_space|min_length[5]');
			$this->form_validation->set_rules('estatura','Estatura','decimal|max_length[4]');
			$this->form_validation->set_rules('peso','Peso','decimal|max_length[3]');
			$this->form_validation->set_rules('drhab_tipo1_desc','(Dirección de Habitación) '.$this->input->post('drhab_tipo1'),'min_length[1]');
			$this->form_validation->set_rules('drhab_tipo2_desc','(Dirección de Habitación) '.$this->input->post('drhab_tipo2'),'min_length[1]');
			$this->form_validation->set_rules('drhab_tipo3_desc','(Dirección de Habitación) '.$this->input->post('drhab_tipo3'),'min_length[1]');
			$this->form_validation->set_rules('drhab_ciudad','(Dirección de Habitación) Ciudad','min_length[5]');
			$this->form_validation->set_rules('telef_hab','(Dirección de Habitación) Teléfono','integer|min_length[7]|max_length[11]');
			$this->form_validation->set_rules('drres_tipo1_desc','(Dirección de Residencia) '.$this->input->post('drres_tipo1'),'min_length[1]');
			$this->form_validation->set_rules('drres_tipo2_desc','(Dirección de Residencia) '.$this->input->post('drres_tipo2'),'min_length[1]');
			$this->form_validation->set_rules('drres_tipo3_desc','(Dirección de Residencia) '.$this->input->post('drres_tipo3'),'min_length[1]');
			$this->form_validation->set_rules('drres_ciudad','(Dirección de Residencia) Ciudad','min_length[5]');
			$this->form_validation->set_rules('telef_res','(Dirección de Residencia) Teléfono','integer|min_length[7]|max_length[11]');
			$this->form_validation->set_rules('emerg_nomape','(En caso de Emergencia) Nombre/Apellidos','alpha_space|min_length[5]');
			$this->form_validation->set_rules('emerg_telef','(En caso de Emergencia) Teléfono','integer|min_length[7]|max_length[11]');
			$this->form_validation->set_rules('emerg_otelef','(En caso de emergencia) Otro Teléfono','integer|min_length[7]|max_length[11]');
			$this->form_validation->set_rules('ced_familia','(¿Depende Economicamente de Alguien?) Cédula','integer|min_length[7]|max_length[8]');
			$this->form_validation->set_rules('nom_familia','(¿Depende Economicamente de Alguien?) Nombres','alpha_space|min_length[3]');
			$this->form_validation->set_rules('ape_familia','(¿Depende Economicamente de Alguien?) Apellidos','alpha_space|min_length[3]');
			$this->form_validation->set_rules('edad_familia','(¿Depende Economicamente de Alguien?) Edad','integer|greater_than[15]|min_length[2]');
			$this->form_validation->set_rules('ocupacion','(¿Depende Economicamente de Alguien?) Ocupación','alpha_space|min_length[3]');
			$this->form_validation->set_rules('nom_cc','Nombre del Concejo Comunal al que perteneces','min_length[3]');
			$this->form_validation->set_rules('dir_cc','Dirección del Concejo Comunal al que perteneces','min_length[3]');
			if ($this->form_validation->run() == FALSE) 
			{
				$this->index();
			}
			else
			{

				// Datos del estudiante

				$estu = array(
		          'genero' => $this->input->post('genero'),
		            'edad' => $this->input->post('edad'),
		        'edocivil' => $this->input->post('edocivil'),
		          'lugnac' => $this->input->post('lugnac'),
		        'estatura' => $this->input->post('estatura'),
		            'peso' => $this->input->post('peso'),
		           'talla' => $this->input->post('talla'),
		           'email' => $this->input->post('email'),
		           'twitter' => $this->input->post('twitter'),
		           'facebook' => $this->input->post('facebook'),
		           'id_discapacidad' => $this->input->post('subtipo_discapacidad'),
		           'detalle_discapacidad' => $this->input->post('detalle_discapacidad'),
		       'drhab_edo' => $this->input->post('drhab_edo'),
		       'drhab_mun' => $this->input->post('drhab_mun'),
		      'drhab_parr' => $this->input->post('drhab_parr'),
		     'drhab_tipo1' => $this->input->post('drhab_tipo1'),
		'drhab_tipo1_desc' => $this->input->post('drhab_tipo1_desc'),
		     'drhab_tipo2' => $this->input->post('drhab_tipo2'),
		'drhab_tipo2_desc' => $this->input->post('drhab_tipo2_desc'),
		     'drhab_tipo3' => $this->input->post('drhab_tipo3'),
		'drhab_tipo3_desc' => $this->input->post('drhab_tipo3_desc'),
		    'drhab_ciudad' => $this->input->post('drhab_ciudad'),
		       'telef_hab' => $this->input->post('telef_hab'),
		 'cond_habitacion' => $this->input->post('cond_habitacion'),
		       'drres_edo' => $this->input->post('drres_edo'),
		       'drres_mun' => $this->input->post('drres_mun'),
		      'drres_parr' => $this->input->post('drres_parr'),
		     'drres_tipo1' => $this->input->post('drres_tipo1'),
		'drres_tipo1_desc' => $this->input->post('drres_tipo1_desc'),
		     'drres_tipo2' => $this->input->post('drres_tipo2'),
		'drres_tipo2_desc' => $this->input->post('drres_tipo2_desc'),
		     'drres_tipo3' => $this->input->post('drres_tipo3'),
		'drres_tipo3_desc' => $this->input->post('drres_tipo3_desc'),
		    'drres_ciudad' => $this->input->post('drres_ciudad'),
		       'telef_res' => $this->input->post('telef_res'),
		 'cond_residencia' => $this->input->post('cond_residencia'),
		    'emerg_nomape' => $this->input->post('emerg_nomape'),
		    'emerg_parent' => $this->input->post('emerg_parent'),
		     'emerg_telef' => $this->input->post('emerg_telef'),
		    'emerg_otelef' => $this->input->post('emerg_otelef'),
		      'id_carrera' => $this->input->post('id_carrera')
								);
				
				// Datos académicos
				$fi = "";
				if (!empty($_POST['fech_ingreso']))
				{
					$fi = $this->datemanager->date2mySQL($this->input->post('fech_ingreso'));
				}

				$aca = array(
			 'id_estudiante' => $this->session->userdata("id_estudiante"),
				    'semestre' => $this->input->post('semestre'),
				'fech_ingreso' => $fi,
				       'turno' => $this->input->post('turno')
								);

				// Datos de dependencia
				$fn = "";
				if (!empty($_POST['fech_nac_fam']))
				{
					$fn = $this->datemanager->date2mySQL($this->input->post('fech_nac_fam'));
				}

				if ($_POST['depende']=="Si") {
					$fam = array(
				 'id_estudiante' => $this->session->userdata("id_estudiante"),
					 'ced_familia' => $this->input->post('ced_familia'),
					 'nom_familia' => $this->input->post('nom_familia'),
					 'ape_familia' => $this->input->post('ape_familia'),
					  'parentesco' => $this->input->post('parentesco'),
					'fech_nac_fam' => $fn,
					'edad_familia' => $this->input->post('edad_familia'),
					'edocivil_fam' => $this->input->post('edocivil_fam'),
					   'ocupacion' => $this->input->post('ocupacion'),
				 'g_instruccion' => $this->input->post('g_instruccion'),
				    'lg_trabajo' => $this->input->post('lg_trabajo'),
					 'ing_mensual' => $this->input->post('ing_mensual'),
					     'depende' => $this->input->post('depende')
								);
				}
				
				if(!empty($_POST['grupof']))
				{
					$i = 0;
					foreach ($_POST['grupof'] as $miembro) {
						$fams[$i]['id_estudiante'] = $this->session->userdata("id_estudiante");	
						$fams[$i]['parentesco'] = $miembro;
						$i++;
					}
				}

				$psico = array(
			 'id_estudiante' => $this->session->userdata("id_estudiante"),
			'siente_carrera' => $this->input->post('siente_carrera'),
			  'agrado_coord' => $this->input->post('agrado_coord'),
			     'apoyo_fam' => $this->input->post('apoyo_fam'),
			     'ctd_hijos' => $this->input->post('ctd_hijos'),
			        'nom_cc' => $this->input->post('nom_cc'),
			        'dir_cc' => $this->input->post('dir_cc'),
			'gusta_horrario' => $this->input->post('gusta_horrario'),
			     'guarderia' => $this->input->post('guarderia')
							);

				if(!empty($_POST['id_oficio'])){
					$i = 0;
					foreach ($this->input->post('id_oficio') as $oficio) {
						$activs[$i]['id_estudiante'] = $this->session->userdata("id_estudiante");	
						$activs[$i]['id_oficio'] = $oficio;
						$i++;
					}
				}
				if(!empty($_POST['id_habilidad']))
				{
					$i = 0;
					foreach ($this->input->post('id_habilidad') as $oficio) {
						$habis[$i]['id_estudiante'] = $this->session->userdata("id_estudiante");	
						$habis[$i]['id_oficio'] = $oficio;
						$i++;
					}
				}
				

				if(!empty($_POST['id_patologia_padecido']))
				{
					$i = 0;
					foreach ($this->input->post('id_patologia_padecido') as $enf) {
						$patos[$i]['id_estudiante'] = $this->session->userdata("id_estudiante");	
						$patos[$i]['id_patologia'] = $enf;
						$i++;
					}
				}
				// print_r($patos);

				if(!empty($_POST['id_patologia_padeces']))
				{
					$i = 0;
					foreach ($this->input->post('id_patologia_padeces') as $enf2) {
						$pades[$i]['id_estudiante'] = $this->session->userdata("id_estudiante");	
						$pades[$i]['id_patologia'] = $enf2;
						$i++;
					}
				}
				if(!empty($_POST['id_patologia_vacunas']))
				{
					$i = 0;
					foreach ($this->input->post('id_patologia_vacunas') as $enf3) {
						$vacas[$i]['id_estudiante'] = $this->session->userdata("id_estudiante");	
						$vacas[$i]['id_patologia'] = $enf3;
						$i++;
					}
				}
				
				$estudiante = $this->estudiantes->update($this->session->userdata("id_estudiante"),$estu);
				
				$hay_academico = $this->academicos->getBy("id_estudiante",$this->session->userdata("id_estudiante"));

				if ($hay_academico) {
					$academico = $this->academicos->update($this->session->userdata("id_estudiante"),$aca);
				}
				else
				{
					$academico = $this->academicos->create($aca);
				}


				$del_familia = $this->familias->destroyAllBy("id_estudiante",$this->session->userdata("id_estudiante"));
				
				if ($_POST['depende']=="Si") {
					$familiar = $this->familias->create($fam);
				}
				if(!empty($fams)){
					$familiares = $this->familias->create_batch($fams);
				}
				

				$psicosociales = $this->psicosocial->update($this->session->userdata("id_estudiante"), $psico);
				$del_activis = $this->destrezas->destroyAllBy("id_estudiante",$this->session->userdata("id_estudiante"));
				if(!empty($activs) && !empty($habis))
				{
					$activis = $this->destrezas->create_batch($activs);
					$habilis = $this->destrezas->create_batch($habis);
				}
				
				$del_saludes = $this->salud->destroyAllBy("id_estudiante",$this->session->userdata("id_estudiante"));
        
        if(!empty($patos))
				{
					$saludes = $this->salud->create_batch($patos);
				}				

        if(!empty($pades)){
					$saludesp = $this->salud->create_batch($pades);
        }

        if(!empty($vacas)){
					$vacus = $this->salud->create_batch($vacas);
				}

        // id_patologia_padecido
				$data = array();
				if ($estudiante) {
					$data["mensaje"]['tipo'] = "success";
					$data["mensaje"]['mensaje'] = "Los datos se modificaron correctamente";
				} else {
					$data["mensaje"]['tipo'] = "error";
					$data['mensaje']['mensaje'] = 'Error: No se ha podido guardar';
				}
				$this->index($data);
			}
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}	
}