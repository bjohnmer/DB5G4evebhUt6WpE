<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tpbecas extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('grocery_CRUD');
		$this->load->library('urlprin');
		$this->load->model('tpbecas_model', 'tpbecas');
	}

	public function index()
	{

		try {
			$crud = new grocery_CRUD();
			$crud->set_table('tipo_becas');
			$crud->set_subject('Becas');
			//$crud->columns('cod_beca', 'descrip_beca');
			$crud->columns('descrip_beca','condicion','monto');

			$crud->add_fields('descrip_beca','condicion','monto');
			$crud->edit_fields('descrip_beca','condicion','monto');

			//$crud->order_by('cod_beca','ASC');
			$crud->order_by('id','ASC');
			//$crud->display_as('cod_beca', 'Código');
			$crud->display_as('id', 'Código');
			$crud->display_as('descrip_beca', 'Descripción');
			$crud->display_as('condicion', 'Condición');
			$crud->display_as('monto', 'Monto de Beca');

			//$crud->set_rules('cod_beca', 'Codigo de la Beca', 'required|alpha');
			$crud->set_rules('descrip_beca', 'Descripción de la Beca', 'required|alpha_space|min_length[5]');
			$crud->set_rules('condicion', 'Indice Academico Requerido', 'required|decimal|greater_than[0]');
			$crud->set_rules('monto', 'Monto de Beca', 'required|decimal|greater_than[0]');

			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->salida($output);
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}


	}

	function salida($output = null)
	{
		$data['titulo'] = "Tipo de Becas";
		$this->load->view('header_view', $output);
		$this->load->view('sesion_entrada_view', $data);
		//$this->load->view('appScript_tpbecas_view');
		$this->load->view('footer_view');
	}
/*	function checkIsUnique()
	{
		extract($_POST);
		$dato['mensaje'] = "Ocurrió un errorrr";
		if($this->tpbecas->isUnique("cod_beca",$cod_beca))
		{
			$dato['mensaje'] = 'ok';
		}
		else
		{
			$dato['mensaje'] = 'Codigo de Tipo de Beca ya esta Registrado. Por favor verifique.';
		}
		echo json_encode($dato);
	}*/
	
}