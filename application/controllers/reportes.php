<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Reportes extends CI_Controller {

	function __construct()
	{
		
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('urlprin');
		$this->load->library('form_validation');

		$this->load->model('estudiante_model', 'estudiante');	
		$this->load->model('solicitudayuda_model', 'solicitudayuda');	
		$this->load->model('solicitudbeca_model', 'solicitudbeca');	
		$this->load->model('solicitudbus_model', 'solicitudbus');	
		$this->load->model('tpbecas_model', 'tiposBeca');	
		
	}

	public function index()
	{

		try {
			
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function becarios()
	{

		$data['titulo'] = "REPORTE DE BECARIOS";
		$data['reportes'] = TRUE;
		$data['tipos'] = $this->db->get('tipo_becas')->result();
		$data['carreras'] = $this->db->get('carreras')->result();
		$this->load->view('header_view');
		$this->load->view('sesion_entrada_view',$data);
		$this->load->view('footer_view');

	}

	public function verReporteBecarios()
	{
		
		$data['titulo'] = "Lista de Becarios <br>";
		$solicitudes = null;
		
		switch ($this->input->post("seleccion")) {
			case 'general':
				$solicitudes = $this->solicitudbeca
									->getAll(null,null,"fech_solicitud","DESC");
				$data['titulo'] .= "<h4>General</h4>";
				break;
			case 'detallado':
				$this->db
					->join('estudiante','beca_solicitud.id_estudiante = estudiante.id')
					->join('tipo_becas','beca_solicitud.id_tipo = tipo_becas.id');
				/*
					Tipo
				*/
				if (!empty($_POST['tipo'])) {
					$this->db->where("id_tipo",$this->input->post("id_tipo"));
				}
				
				/*
					Carrera
				*/
				
				if (!empty($_POST['carrera'])) {
					$this->db->where("id_estudiante IN ( SELECT id FROM estudiante WHERE id_carrera = ".$this->input->post("id_carrera")." ) ", false, false);
				}

				/*
					Estatus
				*/
				if (!empty($_POST['statuschk'])) {
					$this->db->where("status",$this->input->post("status"));
				}

				/*
					Género
				*/
				if (!empty($_POST['generochk'])) {
					$this->db->where("id_estudiante IN ( SELECT id FROM estudiante WHERE genero = '".$this->input->post("genero")."' ) ", false, false);
				}
				
				/*
					Fecha
				*/
				if (!empty($_POST['fecha'])) {
					$this->db->where("fech_solicitud >=",$this->datemanager->date2mySQL( $this->input->post("desde") ));
					$this->db->where("fech_solicitud <=",$this->datemanager->date2mySQL( $this->input->post("hasta") ));
				}
				
				$this->db->order_by("id_solicitud_b","DESC");

				$solicitudes = $this->db->get("beca_solicitud")->result();

				
				/*echo "<pre>";
				print_r($THIS->DB->last_query());
				echo "</pre>";
				
*/
				// $solicitudes = $consulta;
				$tipob = $this->db
								->where("id",$this->input->post("id_tipo"))
								->get("tipo_becas")
								->row(0);

				$data['titulo'] .= "<h4>Detallado: ".$tipob->descrip_beca."</h4>";

				break;
		/*
			
			case 'id_carrera':
				$es = $this->db
							->where("id_carrera",$this->input->post("id_carrera"))
							->get("estudiante");
				
				$carrera = $this->db
								->where("id",$this->input->post("id_carrera"))
								->get("carreras")
								->row(0);
								
				if ( !empty( $es->num_rows() ) ) {
					$estudiantes = $es->result();
					foreach ($estudiantes as $estudiante) {
						$sol = $this->db
									->join('tipo_becas','beca_solicitud.id_tipo = tipo_becas.id')
									->join('estudiante','beca_solicitud.id_estudiante = estudiante.id')
									->where("id_estudiante",$estudiante->id)
									->get("beca_solicitud");
						if ( !empty( $sol->num_rows() ) ) {
							$solicitudes[] = $sol->row(0);
						}
						
					}
					$data['titulo'] .= "<h4>Carrera: ".$carrera->descrip_carrera."</h4>";
				}
				break;
			case 'status':
				$solicitudes = $this->db
									->join('tipo_becas','beca_solicitud.id_tipo = tipo_becas.id')
									->join('estudiante','beca_solicitud.id_estudiante = estudiante.id')
									->where("status",$this->input->post("status"))
									->order_by("id_solicitud_b","DESC")
									->get("beca_solicitud")
									->result();
				$data['titulo'] .= "<h4>Estado: ".$this->input->post("status")."</h4>";
				break;
			case 'genero':
				$es = $this->db
							->where("genero",$this->input->post("genero"))
							->get("estudiante");
				if ( !empty( $es->num_rows() ) ) {
					$estudiantes = $es->result();
					foreach ($estudiantes as $estudiante) {
						$sol = $this->db
									->join('tipo_becas','beca_solicitud.id_tipo = tipo_becas.id')
									->join('estudiante','beca_solicitud.id_estudiante = estudiante.id')
									->where("id_estudiante",$estudiante->id)
									->get("beca_solicitud");
						if ( !empty( $sol->num_rows() ) ) {
							$solicitudes[] = $sol->row(0);
						}
					}
					$data['titulo'] .= "<h4>Género: ".$this->input->post("genero")."</h4>";
				}
				break;
			case 'fecha':
				$solicitudes = $this->db
									->join('tipo_becas','beca_solicitud.id_tipo = tipo_becas.id')
									->join('estudiante','beca_solicitud.id_estudiante = estudiante.id')
									->where("fech_solicitud >=",$this->datemanager->date2mySQL( $this->input->post("desde") ))
									->where("fech_solicitud <=",$this->datemanager->date2mySQL( $this->input->post("hasta") ))
									->order_by("id_solicitud_b","DESC")
									->get("beca_solicitud")
									->result();
				$data['titulo'] .= "<h4>Desde: ".$this->input->post("desde")." - Hasta: ".$this->input->post("hasta")."</h4>";
				break;
			
		*/
		}
		
		$data['solicitudes'] = $solicitudes;
		/*
		echo "<pre>";
		print_r($data['solicitudes']);
		echo "</pre>";
		*/
		$this->load->library('pdf');
		// $this->pdf->load_view("becariosPDF_view",$data);
		$this->pdf->load_view("bpdf",$data);
		$this->pdf->render();
		$this->pdf->stream('listaBecarios.pdf');
		// $this->load->view('bpdf',$data);

	}
	
	public function ayudasEconomicas()
	{

		try {
			$data['titulo'] = "REPORTE DE AYUDAS ECONÓMICAS";
			$data['reportes'] = TRUE;
			
			$data['tipos'] = $this->db->get('tipo_ayuda')->result();
			$data['carreras'] = $this->db->get('carreras')->result();

			$this->load->view('header_view');
			$this->load->view('sesion_entrada_view',$data);
			$this->load->view('footer_view');

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function verReporteAE()
	{
		$data['titulo'] = "Lista de Ayudas Económicas <br>";
		$solicitudes = null;
		switch ($this->input->post("seleccion")) {
			case 'general':
				$solicitudes = $this->db
									->join('tipo_ayuda','ayuda_solicitud.id_tipo = tipo_ayuda.id')
									->join('estudiante','ayuda_solicitud.id_estudiante = estudiante.id')
									->order_by("id_solicitud_ayuda","DESC")
									->get("ayuda_solicitud")
									->result();
				$data['titulo'] .= "<h4>General</h4>";
				break;
			case 'id_tipo':
				$solicitudes = $this->db
									->join('tipo_ayuda','ayuda_solicitud.id_tipo = tipo_ayuda.id')
									->join('estudiante','ayuda_solicitud.id_estudiante = estudiante.id')
									->where("id_tipo",$this->input->post("id_tipo"))
									->order_by("id_solicitud_ayuda","DESC")
									->get("ayuda_solicitud")
									->result();
				$data['titulo'] .= "<h4>Tipo de Ayuda: ".$solicitudes[0]->cod_ayuda." ".$solicitudes[0]->descrip_ayuda."</h4>";
				break;
			case 'id_carrera':
				$es = $this->db
							->where("id_carrera",$this->input->post("id_carrera"))
							->get("estudiante");
				
				$carrera = $this->db
								->where("id",$this->input->post("id_carrera"))
								->get("carreras")
								->row(0);
				
				$var = $es->num_rows();
				if ( !empty( $var ) ) {
					$estudiantes = $es->result();
					foreach ($estudiantes as $estudiante) {
						$sol = $this->db
									->join('tipo_ayuda','ayuda_solicitud.id_tipo = tipo_ayuda.id')
									->join('estudiante','ayuda_solicitud.id_estudiante = estudiante.id')
									->where("id_estudiante",$estudiante->id)
									->get("ayuda_solicitud");
						$var2 = $sol->num_rows();
						if ( !empty( $var2 ) ) {
							$solicitudes[] = $sol->row(0);
						}
						
					}
					$data['titulo'] .= "<h4>Carrera: ".$carrera->descrip_carrera."</h4>";
				}
				break;
			case 'status':
				$solicitudes = $this->db
									->join('tipo_ayuda','ayuda_solicitud.id_tipo = tipo_ayuda.id')
									->join('estudiante','ayuda_solicitud.id_estudiante = estudiante.id')
									->where("status",$this->input->post("status"))
									->order_by("id_solicitud_ayuda","DESC")
									->get("ayuda_solicitud")
									->result();
				$data['titulo'] .= "<h4>Estado: ".$this->input->post("status")."</h4>";
				break;
			case 'genero':
				$es = $this->db
							->where("genero",$this->input->post("genero"))
							->get("estudiante");
				$var3 = $es->num_rows();
				if ( !empty( $var3 ) ) {
					$estudiantes = $es->result();
					foreach ($estudiantes as $estudiante) {
						$sol = $this->db
									->join('tipo_ayuda','ayuda_solicitud.id_tipo = tipo_ayuda.id')
									->join('estudiante','ayuda_solicitud.id_estudiante = estudiante.id')
									->where("id_estudiante",$estudiante->id)
									->get("ayuda_solicitud");
						$var3 = $sol->num_rows();
						if ( !empty( $var3 ) ) {
							$solicitudes[] = $sol->row(0);
						}
					}
					$data['titulo'] .= "<h4>Género: ".$this->input->post("genero")."</h4>";
				}
				break;
			case 'fecha':
				$solicitudes = $this->db
									->join('tipo_ayuda','ayuda_solicitud.id_tipo = tipo_ayuda.id')
									->join('estudiante','ayuda_solicitud.id_estudiante = estudiante.id')
									->where("fech_solicitud >=",$this->datemanager->date2mySQL( $this->input->post("desde") ))
									->where("fech_solicitud <=",$this->datemanager->date2mySQL( $this->input->post("hasta") ))
									->order_by("id_solicitud_ayuda","DESC")
									->get("ayuda_solicitud")
									->result();
				$data['titulo'] .= "<h4>Desde: ".$this->input->post("desde")." - Hasta: ".$this->input->post("hasta")."</h4>";
				break;
			
		}
		
		$data['solicitudes'] = $solicitudes;
		$this->load->library('pdf');
		$this->pdf->load_view("ayudaPDF_view",$data);
		$this->pdf->render();
		$this->pdf->stream('listaAyudas.pdf');
	}
	
	public function solicitudBus()
	{

		try {
			$data['titulo'] = "REPORTE DE SOLICITUDES DE BUS";
			$data['reportes'] = TRUE;
			
			// $data['tipos'] = $this->db->get('tipo_ayuda')->result();
			// $data['carreras'] = $this->db->get('carreras')->result();

			$this->load->view('header_view');
			$this->load->view('RsolicitudBus_view',$data);
			$this->load->view('footer_view');

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function verReporteSB()
	{
		$data['titulo'] = "Lista de Solicitudes de Bus <br>";
		$solicitudes = null;
		switch ($this->input->post("seleccion")) {
			case 'general':
				$solicitudes = $this->db
									->join('bus','bus_solicitud.id_bus = bus.id')
									->join('chofer','bus_solicitud.id_chofer = chofer.id')
									->order_by("id_bus_solicitud","DESC")
									->get("bus_solicitud")
									->result();
				$data['titulo'] .= "<h4>General</h4>";
				break;
			case 'id_bus':
				$solicitudes = $this->db
									->join('bus','bus_solicitud.id_bus = bus.id')
									->join('chofer','bus_solicitud.id_chofer = chofer.id')
									->where('bus.placa',$this->input->post("placa"))
									->order_by("id_bus_solicitud","DESC")
									->get("bus_solicitud")
									->result();
				$data['titulo'] .= "<h4>Placa: ".$this->input->post("placa")."</h4>";
				break;
			
			case 'fecha_status':
				$solicitudes = $this->db
									->join('bus','bus_solicitud.id_bus = bus.id')
									->join('chofer','bus_solicitud.id_chofer = chofer.id')
									->where("fech_solicitud >=",$this->datemanager->date2mySQL( $this->input->post("desde") ))
									->where("fech_solicitud <=",$this->datemanager->date2mySQL( $this->input->post("hasta") ))
									->order_by("id_bus_solicitud","DESC")
									->get("bus_solicitud")
									->result();
				$data['titulo'] .= "<h4>Desde: ".$this->input->post("desde")." - Hasta: ".$this->input->post("hasta")."</h4>";
				break;
			
		}
		
		$data['solicitudes'] = $solicitudes;
		$this->load->library('pdf');
		$this->pdf->load_view("busPDF_view",$data);
		$this->pdf->render();
		$this->pdf->stream('listaSolicitudBus.pdf');
	}

	public function censados()
	{
		try {
			$data['titulo'] = "REPORTE DE ESTUDIANTES CENSADOS";
			$data['reportes'] = TRUE;
			
			// $data['tipos'] = $this->db->get('tipo_ayuda')->result();
			$data['carreras'] = $this->db->get('carreras')->result();

			$this->load->view('header_view');
			$this->load->view('RCensados_view',$data);
			$this->load->view('footer_view');

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function verReporteCensados()
	{
		$data['titulo'] = "Lista de Estudiantes Censados<br>";
		$solicitudes = null;
		switch ($this->input->post("seleccion")) {
			case 'general':
				$solicitudes = $this->db
									// ->join('tipo_ayuda','ayuda_solicitud.id_tipo = tipo_ayuda.id')
									->where("genero IS NOT NULL", false, false)
									->order_by("ced_estudiante","DESC")
									->get("estudiante")
									->result();
				$data['titulo'] .= "<h4>General</h4>";
				break;
			case 'ubicacion':
				$solicitudes = $this->db
							->join('estados','estudiante.drhab_edo = estados.id_estado')
							->join('municipios','estudiante.drhab_mun = municipios.id_municipio')
							->join('parroquias','estudiante.drhab_mun = parroquias.id_parroquia')
							->where("(estudiante.genero IS NOT NULL) AND (estados.nombre_estado LIKE '%".$this->input->post('direccion')."%' OR municipios.nombre_municipio LIKE '%".$this->input->post('direccion')."%' OR parroquias.nombre_parroquia LIKE '%".$this->input->post('direccion')."%' OR estudiante.drhab_ciudad LIKE '%".$this->input->post('direccion')."%' )", false, false)
							->get("estudiante")
							->result();
							
				$data['titulo'] .= "<h4>Ubicación de Habitación: ".$this->input->post('direccion')."</h4>";
				// print_r($this->db->last_query());
				break;
			case 'id_carrera':
				$solicitudes = $this->db
							->where("genero IS NOT NULL", false, false)
							->where("id_carrera",$this->input->post("id_carrera"))
							->get("estudiante")
							->result();
				
				$carrera = $this->db
								->where("id",$this->input->post("id_carrera"))
								->get("carreras")
								->row(0);
								
				$data['titulo'] .= "<h4>Carrera: ".$carrera->descrip_carrera."</h4>";

				break;
			case 'status':
				$solicitudes = $this->db
							->where("genero IS NOT NULL", false, false)
							->where("estatus_estudiante",$this->input->post("status"))
							->get("estudiante")
							->result();
				
				
				$data['titulo'] .= "<h4>Estatus: ".$this->input->post("status")."</h4>";

				break;
			case 'genero':
				$solicitudes = $this->db
							->where("genero IS NOT NULL", false, false)
							->where("genero",$this->input->post("genero"))
							->get("estudiante")
							->result();
				
				
				$data['titulo'] .= "<h4>Género: ".$this->input->post("genero")."</h4>";

				break;
			
			
		}
		
		$data['solicitudes'] = $solicitudes;
		$this->load->library('pdf');
		$this->pdf->load_view("censadosPDF_view",$data);
		$this->pdf->render();
		$this->pdf->stream('listaCensados.pdf');

	}

	public function discapacidad()
	{
	}
	function salida($output = null)
	{
		$data['titulo'] = "Oficios";
		$this->load->view('header_view', $output);
		$this->load->view('sesion_entrada_view', $data);
		$this->load->view('footer_view');
	}
	
}