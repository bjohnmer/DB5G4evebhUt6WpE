<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Autobus extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('grocery_CRUD');
		$this->load->library('urlprin');
		$this->load->model('autobus_model', 'autobus');
		$this->load->model('solicitudbus_model', 'solicitudbus');	
		$this->load->model('choferes_model', 'chofer');

		$this->chofer->updateStatus();
		$this->autobus->updateStatus();
		$this->solicitudbus->updateStatus();

	}

	public function index()
	{

		try {
			$crud = new grocery_CRUD();
			$crud->set_table('bus');
			$crud->set_subject('Transporte');
			//$crud->columns('cod_bus', 'placa', 'marca', 'modelo', 'puestos','status');
			$crud->columns('id', 'placa', 'marca', 'modelo', 'puestos','status');
			
			//$crud->add_fields('cod_bus','placa','marca', 'modelo','puestos');
			$crud->add_fields('placa','marca', 'modelo','puestos');
			$crud->edit_fields('marca','modelo','puestos','status');

			//$crud->order_by('cod_bus','ASC');
			$crud->order_by('id','ASC');
			//$crud->display_as('cod_bus', 'Codigo');
			$crud->display_as('id', 'Codigo');
			$crud->display_as('placa', 'Placa');
			$crud->display_as('marca', 'Marca');
			$crud->display_as('modelo', 'Modelo');
			$crud->display_as('puestos', 'Cantidad de Puestos');
			$crud->display_as('status', 'Condición');
			$crud->field_type('status', 'enum', array('Dañado','Reparación','Desincorporado'));

			$crud->callback_before_update(array($this,'check_status_callback'));

			//$crud->set_rules('cod_bus', 'Codigo del Autobus', 'required|alpha');
			$crud->set_rules('placa', 'Placa del Autobus', 'required|trim|alpha_dash|min_length[6]');
			$crud->set_rules('marca', 'Marca del Autobus', 'required|alpha|min_length[4]');
			$crud->set_rules('modelo', 'Modelo del Autobus', 'required|alpha_dash|min_length[3]');
			$crud->set_rules('puestos', 'Cantidad de Puestos del Autobus', 'required|numeric|greater_than[0]|max_length[2]');
			// $crud->set_rules('status', 'Condición de Autobus', 'required');

			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->salida($output);
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}


	function check_status_callback($post_array)
	{
		if(empty($post_array['status']))
		{
        unset($post_array['status']);
    }
    return $post_array;
	}


	function salida($output = null)
	{
		$data['titulo'] = "Transportes";
		$this->load->view('header_view', $output);
		$this->load->view('sesion_entrada_view',$data);
		$this->load->view('appScript_autobus_view');
		$this->load->view('footer_view');
		
	}

	function checkIsUnique()
	{
		extract($_POST);
		//if($this->autobus->isUnique("cod_bus",$cod_bus))
		//{
			if($this->autobus->isUnique("placa",$placa))
			{
				$dato['mensaje'] = 'ok';
			}
			else
			{
				$dato['mensaje'] = 'Placa del Vehiculo, ya registrada, Verifique por favor...';
			}
		//}
		//else
		//{
		//	$dato['mensaje'] = 'Codigo del Vehiculo, ya registrado, Verifique por favor...';
		//}
		echo json_encode($dato);
	}
	
}