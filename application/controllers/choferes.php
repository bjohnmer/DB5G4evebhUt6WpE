<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Choferes extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('grocery_CRUD');
		$this->load->library('urlprin');
		$this->load->model('choferes_model', 'choferes');
		$this->load->model('autobus_model', 'autobus');
		$this->load->model('solicitudbus_model', 'solicitudbus');	

		$this->choferes->updateStatus();
		$this->autobus->updateStatus();
		$this->solicitudbus->updateStatus();

	}

	public function index()
	{

		try {
			$crud = new grocery_CRUD();
			$crud->set_table('chofer');
			$crud->set_subject('Conductor');
			$crud->columns('ced_chofer', 'nombre', 'apellido', 'telefono','status');

			$crud->add_fields('ced_chofer','nombre','apellido','telefono');
			$crud->edit_fields('nombre','apellido','telefono','status');

			$crud->order_by('ced_chofer','ASC');
			$crud->display_as('ced_chofer', 'Cédula');
			$crud->display_as('nombre', 'Nombres');
			$crud->display_as('apellido', 'Apellidos');
			$crud->display_as('telefono', 'Teléfono');

			$crud->field_type('status','enum',array('Inactivo'));

			$crud->set_rules('ced_chofer', 'Cédula del Conductor', 'required|numeric|min_length[7]|max_length[8]');
			$crud->set_rules('nombre', 'Nombre del Conductor', 'required|alpha_space|min_length[5]');
			$crud->set_rules('apellido', 'Apellido del Conductor', 'required|alpha_space|min_length[5]');
			$crud->set_rules('telefono', 'Numero de telefono del Conductor', 'required|trim|numeric|exact_length[11]');
			// $crud->set_rules('status', 'Condición del Conductor', 'required');
			
			$crud->callback_before_update(array($this,'check_status_callback'));

			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->salida($output);
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function check_status_callback($post_array)
	{
		if(empty($post_array['status']))
		{
        unset($post_array['status']);
    }
    return $post_array;
	}


	function salida($output = null)
	{
		$data['titulo'] = "Choferes";
		$this->load->view('header_view', $output);
		$this->load->view('sesion_entrada_view', $data);
		$this->load->view('appScript_choferes_view');
		$this->load->view('footer_view');
	}

	function checkIsUnique()
	{
		extract($_POST);
		$dato['mensaje'] = "Ocurrió un error";
		if($this->choferes->isUnique("ced_chofer",$ced_chofer))
		{
			$dato['mensaje'] = 'ok';
		}
		else
		{
			$dato['mensaje'] = 'Cédula del Conductor ya se encuentra Registrada. Por favor verifique...';
		}
		echo json_encode($dato);
	}

	
}