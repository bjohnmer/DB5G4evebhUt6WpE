<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Oficios extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('grocery_CRUD');
		$this->load->library('urlprin');
		$this->load->model('oficios_model', 'oficios');
	}

	public function index()
	{

		try {
			$crud = new grocery_CRUD();
			$crud->set_table('oficios');
			$crud->set_subject('oficio');
			//$crud->columns('cod_oficio', 'descrip_oficio');
			$crud->columns('id', 'tipo_oficio','descrip_oficio');

			$crud->add_fields('tipo_oficio','descrip_oficio');
			$crud->edit_fields('tipo_oficio','descrip_oficio');

			//$crud->order_by('cod_oficio','ASC');
			$crud->order_by('id','ASC');
			//$crud->display_as('cod_oficio', 'Codigo');
			$crud->display_as('id', 'Código');
			$crud->display_as('tipo_oficio', 'Tipo de Oficio');
			$crud->display_as('descrip_oficio', 'Descripción');

			//$crud->set_rules('cod_oficio', 'Codigo del Oficio', 'required|alpha');
			$crud->set_rules('tipo_oficio', 'Tipo de Oficio', 'required');
			$crud->set_rules('descrip_oficio', 'Descripción del Oficio', 'required|alpha_space|min_length[5]');

			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->salida($output);
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function salida($output = null)
	{
		$data['titulo'] = "Oficios";
		$this->load->view('header_view', $output);
		$this->load->view('sesion_entrada_view', $data);
		//$this->load->view('appScript_oficios_view');
		$this->load->view('footer_view');
	}

/*	function checkIsUnique()
	{
		extract($_POST);
		$dato['mensaje'] = "Ocurrió un error";
		if($this->oficios->isUnique("cod_oficio",$cod_oficio))
		{
			$dato['mensaje'] = 'ok';
		}
		else
		{
			$dato['mensaje'] = 'Codigo del Oficio ya se encuentra Registrado. Por favor verifique...';
		}
		echo json_encode($dato);
	}*/

	
}