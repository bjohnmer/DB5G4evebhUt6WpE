<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->library('Form_validation');
		$this->load->model("login_model");
		$this->load->library('urlprin');
	}

	public function index()
	{
		$data['noticias'] = $this->db->limit(3)->order_by("fecha_noticia","DESC")->get("noticias")->result();
		$this->load->view('header_view');
		$this->load->view('sesion_entrada_view',$data);
		$this->load->view('footer_view');
	}
	
}