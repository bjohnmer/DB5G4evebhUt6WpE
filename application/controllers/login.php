<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		$this->load->library('Form_validation');
		$this->load->model("login_model");
		$this->load->model('estudiante_model', 'estudiantes');
		$this->load->model('usuarios_model', 'usuarios');

		$this->load->library('urlprin');
	}

	public function index($data = null)
	{
		$this->load->view('header_view');
		$this->load->view('login_view',$data);
		$this->load->view('footer_view');
		$this->load->view('appScriptLogin_view');
	}
	
	public function entrar()
	{
		$this->form_validation->set_rules("usuario", "Usuario", "required|trim|max_length[15]");
		$this->form_validation->set_rules("clave", "Clave", "required|trim|max_length[50]");

		if ($this->form_validation->run() == FALSE) {
			$this->load->view('header_view');
			$this->load->view('login_view');
			$this->load->view('footer_view');
			$this->load->view('appScriptLogin_view');
		

		} 
		else 
		{
			$this->load->library('encrypt');
			$clave = md5(set_value("clave"));
			$data = $this->login_model->check_login(set_value("usuario"), $clave);
			if ($data) {
				$this->session->set_userdata("logged_in" , TRUE);
				$this->session->set_userdata("id" , $data->id);
				$this->session->set_userdata("cedula" , $data->cedula);
				$this->session->set_userdata("nombre", $data->nombre);
				$this->session->set_userdata("usuario" , $data->usuario);
				$this->session->set_userdata("tp_usuario"	, $data->tp_usuario);
				if ($data->tp_usuario == "Estudiante") 
				{
						
					$estudiante = $this->estudiantes->getBy('ced_estudiante',$data->cedula);
					if ($estudiante[0]->estatus_estudiante == 'Activo') 
					{
						$this->session->set_userdata("id_estudiante", $estudiante[0]->id);
						$this->session->set_userdata("promedio", $estudiante[0]->promedio);
					}
					else
					{
						redirect('/');
					}
				}
				redirect('admin/');

			}  else  {
				$data['logged_in_fail'] = TRUE;
				$this->load->view('header_view');
				$this->load->view('login_view',$data);
				$this->load->view('footer_view');
				$this->load->view('appScriptLogin_view');
			}
		}
	}

	public function logout(){
		$this->session->set_userdata("logged_in", FALSE);
		$this->session->sess_destroy();
		redirect('/');
	}

	public function registro()
	{

		try {
			
			$data["mensaje"]['from'] = "registro";

			$this->load->library('form_validation');
			
			$this->form_validation->set_rules('cedula','Cédula','required|numeric');
			$this->form_validation->set_rules('fechanac','Fecha de Nacimiento','required');
			$this->form_validation->set_rules('usuario','Nombre de Usuario (Login)','required|trim|alpha_numeric|max_length[15]');
			$this->form_validation->set_rules('clave','Clave','required|matches[confir]');
			$this->form_validation->set_rules('confir','Confirmación de Clave','required|matches[clave]');
			if ($this->form_validation->run() == FALSE)
			{
				$this->index($data);
			}
			else
			{
				
				$estudiante = $this->estudiantes->getBy('ced_estudiante',$_POST['cedula']);
				if ($estudiante) {
					if ($estudiante[0]->fechanac == $_POST['fechanac']) 
					{
						$cedula = $this->usuarios->getBy('cedula',$_POST['cedula']);
						if (!$cedula) {

							$usuario = $this->usuarios->isUnique("usuario",$_POST['usuario']);
							if ($usuario) {
								$data = array('cedula' => $_POST['cedula'],
															'nombre' => $estudiante[0]->apellidos." ".$estudiante[0]->nombres,
															'usuario' => $_POST['usuario'],
															'clave' => md5($_POST['clave']),
															'confir' => md5($_POST['confir']),
															'tp_usuario' => 'Estudiante'
															);
								$respuesta = $this->usuarios->create($data);
								if ($respuesta) {
									$data["mensaje"]['tipo'] = "success";
									$data["mensaje"]['mensaje'] = "Usted ha sido registrado correctamente";

								} else {
									$data["mensaje"]['tipo'] = "error";
									$data['mensaje']['mensaje'] = 'Error Interno: No se ha podido registrar';
								}
							}
							else
							{
								$data["mensaje"]['tipo'] = "error";
								$data['mensaje']['mensaje'] = 'Error: El el nombre de usuario ya ha sido registrado, intente con otro';
							}

						}
						else
						{
							$data["mensaje"]['tipo'] = "error";
							$data['mensaje']['mensaje'] = 'Error: El usuario ya ha sido registrado con ésta cédula <br>Diríjase al administrador del sistema';
						}
					}
					else
					{
						$data["mensaje"]['tipo'] = "error";
						$data['mensaje']['mensaje'] = 'Error: Los datos proporcionados son errados';
					}
				}
				else
				{
						$data["mensaje"]['tipo'] = "error";
						$data['mensaje']['mensaje'] = 'Error: Usted no se encuentra registrado en el sistema';
				}

				$this->index($data);
			}
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}