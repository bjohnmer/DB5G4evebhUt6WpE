<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Ctabalance extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('grocery_CRUD');
		$this->load->library('urlprin');
		$this->load->model('ctabalance_model', 'ctabalance');
	}

	public function index()
	{

		try {
			$crud = new grocery_CRUD();
			$crud->set_table('cuentas_balance');
			$crud->set_subject('Cuentas de Balance');
			$crud->columns('cod_cuenta', 'tp_cuenta', 'descrip_cuenta');
			$crud->edit_fields('tp_cuenta', 'descrip_cuenta');

			$crud->order_by('cod_cuenta','ASC');
			$crud->display_as('cod_cuenta', 'Codigo');
			$crud->display_as('tp_cuenta', 'Tipo');
			$crud->display_as('descrip_cuenta', 'Descripción');

			$crud->set_rules('tp_cuenta', 'Tipo de cuenta', 'required');
			$crud->set_rules('cod_cuenta', 'Codigo de la cuenta', 'required|alpha');
			$crud->set_rules('descrip_cuenta', 'Descripción de la cuenta', 'required|alpha_space');

			// Renderiza la Vista
			$output = $crud->render();
			
			// Llama a la función que va a mostrar la Vista
			$this->salida($output);
				
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function salida($output = null)
	{
		$data['titulo'] = "Cuentas de Balance";
		$this->load->view('header_view', $output);
		$this->load->view('sesion_entrada_view', $data);
		$this->load->view('appScript_ctabalance_view');
		$this->load->view('footer_view');
	}

	function checkIsUnique()
	{
		extract($_POST);
		$dato['mensaje'] = "Ocurrió un error";
		if($this->ctabalance->isUnique("cod_cuenta",$cod_cuenta))
		{
			$dato['mensaje'] = 'ok';
		}
		else
		{
			$dato['mensaje'] = 'Codigo de la Cuenta ya se encuentra Registrada. Por favor verifique...';
		}
		echo json_encode($dato);
	}

	
}