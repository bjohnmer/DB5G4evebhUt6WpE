<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class SolicitudAyuda extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library('session');
		if (!$this->session->userdata("logged_in")){
			redirect('/');
		}
		$this->load->helper('url');		
		$this->load->library('Form_validation');
		$this->load->library('urlprin');
		$this->load->model('estudiante_model', 'estudiante');	
		$this->load->model('solicitudayuda_model', 'solicitudayuda');	
		$this->load->model('tpayudas_model', 'tiposAyuda');	
	}

	public function date2mySQL($date)
	{
		$fecha = explode("/", $date);
		return $fecha[2]."-".$fecha[1]."-".$fecha[0];
	}

	public function date2normal($date)
	{
		$fecha = explode("-", $date);
		return $fecha[2]."/".$fecha[1]."/".$fecha[0];
	}
	
	public function index()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudayuda->getAll("id_estudiante",$estudiante[0]->id,"fech_solicitud","DESC");
			elseif ($this->session->userdata("tp_usuario") == "Coordinador"):
				$solicitudes = $this->solicitudayuda->getAll(null,null,"fech_solicitud","DESC");
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function misSolicitados()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudayuda->getMy("status","Solicitado",$estudiante[0]->id);
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function verSolicitados()
	{

		try {
			$solicitudes = $this->solicitudayuda->getAll("status","Solicitado","fech_solicitud","DESC");
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function misTransitos()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudayuda->getMy("status","En Tránsito",$estudiante[0]->id);
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function verEnTransito()
	{

		try {
			$solicitudes = $this->solicitudayuda->getAll("status","En Tránsito","fech_solicitud","DESC");
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function verAprobados()
	{

		try {
			$solicitudes = $this->solicitudayuda->getAll("status","Aprobado","fech_solicitud","DESC");
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function misAprobados()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudayuda->getMy("status","Aprobado",$estudiante[0]->id);
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}


	public function verRechazados()
	{

		try {
			$solicitudes = $this->solicitudayuda->getAll("status","Rechazado","fech_solicitud","DESC");
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function misRechazados()
	{

		try {
			if ($this->session->userdata("tp_usuario") == "Estudiante"):
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$solicitudes = $this->solicitudayuda->getMy("status","Rechazado",$estudiante[0]->id);
			endif;
			$data['solicitudes'] = $solicitudes;
			$this->salida($data);
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}


	public function add()
	{

		try {

			$data['tipos'] = $this->tiposAyuda->getAll();
			$data['titulo'] = "Solicitud de Ayuda";
			$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
			$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
			$this->load->view('header_view', $estilos);
			$this->load->view("solicitudAyudaAdd_view",$data);
			$this->load->view('footer_view',$jss);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function insert()
	{

		try {

			$this->load->library('form_validation');
		
			$this->form_validation->set_rules('id_tipo', 'Tipo de Ayuda', 'required');
			$this->form_validation->set_rules('motivo', 'Motivo', 'required|min_length[10]');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->add();
			}
			else
			{
				$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
				$data = array('id_estudiante'=> $estudiante[0]->id,
											'id_tipo' 	=> $_POST['id_tipo'],
											'motivo'		=>$_POST['motivo']);
				$respuesta = $this->solicitudayuda->create($data);

				if ($respuesta) {
					$data["mensaje"]['tipo'] = "success";
					$data["mensaje"]['mensaje'] = "Los datos se almacenaron correctamente";

					$data['tipos'] = $this->tiposAyuda->getAll();
					

					$solicitudes = $this->solicitudayuda->getBy("ayuda_solicitud.id_estudiante",$estudiante[0]->id);
					
					$data['titulo'] = "Solicitud de Ayuda";
					$data['solicitudes'] = $solicitudes;
					$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
					$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
					$this->load->view('header_view', $estilos);
					$this->load->view("solicitudAyuda_view",$data);
					$this->load->view('footer_view',$jss);

				} else {
					$data['mensaje'] = 'Error: No se ha podido guardar';
					$this->add_form($data);
				}
			}

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function edit($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudayuda->getBy("id_solicitud_ayuda", $id);
			$data['tipos'] = $this->tiposAyuda->getAll();
			$data['titulo'] = "Solicitud de Ayuda";
			$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
			$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
			$this->load->view('header_view', $estilos);
			$this->load->view("solicitudAyudaEdit_view",$data);
			$this->load->view('footer_view',$jss);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function recaudos($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudayuda->getBy("id_solicitud_ayuda", $id);
			$data['estudiante'] = $this->estudiante->getBy("id",$data["solicitud"][0]->id_estudiante);
			$data['tipos'] = $this->tiposAyuda->getAll();
			$data['titulo'] = "Solicitud de Ayuda";
			$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
			$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
			$this->load->view('header_view', $estilos);
			$this->load->view("solicitudAyudaRecaudos_view",$data);
			$this->load->view('footer_view',$jss);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function updateRecaudos()
	{

		try {

			$this->load->library('form_validation');
		
			$this->form_validation->set_rules('comprobante','Comprobante de Solicitud','required');
			$this->form_validation->set_rules('carta','Motivos','required');
			$this->form_validation->set_rules('informe','Informe Médico','required');
			$this->form_validation->set_rules('presupuestos','Presupuestos (3)','required');
			$this->form_validation->set_rules('constancia','Constancia de Estudios','required');
			$this->form_validation->set_rules('copiacedula','Fotocopia de Cédula','required');


			if ($this->form_validation->run() == FALSE)
			{
				$this->recaudos($_POST['id_solicitud_ayuda']);
			}
			else
			{

				$data = array(
										'id_solicitud_ayuda'	=> $_POST['id_solicitud_ayuda'],
										'comprobante' => $_POST['comprobante'],
										'carta' => $_POST['carta'],
										'informe' => $_POST['informe'],
										'presupuestos' => $_POST['presupuestos'],
										'constancia' => $_POST['constancia'],
										'copiacedula' => $_POST['copiacedula']
								);

				$respuesta = $this->solicitudayuda->updateRecaudos($data);

				if ($respuesta) {
					$data["mensaje"]['tipo'] = "success";
					$data["mensaje"]['mensaje'] = "Los datos se modificaron correctamente";

					$data['tipos'] = $this->tiposAyuda->getAll();
					
					$data['solicitudes'] = $this->solicitudayuda->getAll(null,null,"fech_solicitud","DESC");
					$data['tipos'] = $this->tiposAyuda->getAll();
					$data['titulo'] = "Solicitud de Ayuda";
					$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
					$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
					$this->load->view('header_view', $estilos);
					$this->load->view("solicitudAyuda_view",$data);
					$this->load->view('footer_view',$jss);

				} else {
					$data['mensaje'] = 'Error: No se ha podido guardar';
					$this->add_form($data);
				}
			}

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function gestionar($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudayuda->getBy("id_solicitud_ayuda", $id);
			$data['estudiante'] = $this->estudiante->getBy("id",$data["solicitud"][0]->id_estudiante);
			$data['tipos'] = $this->tiposAyuda->getAll();
			$data['titulo'] = "Solicitud de Ayuda";
			$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
			$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
			$this->load->view('header_view', $estilos);
			$this->load->view("solicitudAyudaGestion_view",$data);
			$this->load->view('footer_view',$jss);
			$this->load->view('appScriptAyuda_view',$jss);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function updGestionar()
	{

		try {

			$this->load->library('form_validation');
			$this->form_validation->set_message('decimal', 'El campo %s debe contener una cantidad válida');

			$this->form_validation->set_rules('status','Estado de la Solicitud','required');
			
			if (!empty($_POST['status'])) {
				if ($_POST['status']=="Aprobado") {
					$this->form_validation->set_rules('forma_pago','Forma de Pago','required');
					$this->form_validation->set_rules('monto','Monto','required|decimal|greater_than[0]');
				}
			}

			if ($this->form_validation->run() == FALSE)
			{
				$this->gestionar($_POST['id_solicitud_ayuda']);
			}
			else
			{

				$data['status'] = $_POST['status'];
				if ($_POST['status']=="Aprobado") { 
					$data['forma_pago'] = $_POST['forma_pago']; 
					$data['monto'] = $_POST['monto'];
				}

				$respuesta = $this->solicitudayuda->update($data,$_POST['id_solicitud_ayuda']);

				if ($respuesta) {
					$data["mensaje"]['tipo'] = "success";
					$data["mensaje"]['mensaje'] = "Los datos se modificaron correctamente";

					$data['tipos'] = $this->tiposAyuda->getAll();
					
					$data['solicitudes'] = $this->solicitudayuda->getAll(null,null,"fech_solicitud","DESC");
					$data['tipos'] = $this->tiposAyuda->getAll();
					$data['titulo'] = "Solicitud de Ayuda";
					$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
					$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
					$this->load->view('header_view', $estilos);
					$this->load->view("solicitudAyuda_view",$data);
					$this->load->view('footer_view',$jss);

				} else {
					$data['mensaje'] = 'Error: No se ha podido guardar';
					$this->add_form($data);
				}
			}

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function validmonto_check($str)
	{
		if (!preg_match('/^\d*+(?:\.\d*)?+$/', $str))
		{
			$this->form_validation->set_message('validmonto_check', 'El campo %s debe contener una cantidad válida');
			return FALSE;
		}
		else
		{
			return TRUE;
		}
	}

	public function update()
	{

		try {

			$this->load->library('form_validation');
		
			$this->form_validation->set_rules('id_tipo', 'Tipo de Ayuda', 'required');
			$this->form_validation->set_rules('motivo', 'Motivo', 'required|min_length[10]');
			
			if ($this->form_validation->run() == FALSE)
			{
				$this->edit($_POST['id_solicitud_ayuda']);
			}
			else
			{
				$data = array('id_tipo' 	=> $_POST['id_tipo'],
											'motivo'		=>$_POST['motivo']);

				$respuesta = $this->solicitudayuda->update($data,$_POST['id_solicitud_ayuda']);

				if ($respuesta) {
					$data["mensaje"]['tipo'] = "success";
					$data["mensaje"]['mensaje'] = "Los datos se modificaron correctamente";

					$data['tipos'] = $this->tiposAyuda->getAll();
					
					$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
					$solicitudes = $this->solicitudayuda->getBy("ayuda_solicitud.id_estudiante",$estudiante[0]->id);
					
					$data['titulo'] = "Solicitud de Ayuda";
					$data['solicitudes'] = $solicitudes;
					$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
					$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
					$this->load->view('header_view', $estilos);
					$this->load->view("solicitudAyuda_view",$data);
					$this->load->view('footer_view',$jss);

				} else {
					$data['mensaje'] = 'Error: No se ha podido guardar';
					$this->add_form($data);
				}
			}
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function delete()
	{

		try {
			$solicitud = $this->solicitudayuda->destroy($this->uri->segment(3));
			if ($solicitud) {
				$data['mensaje']['tipo']="success";
				$data['mensaje']['mensaje'] = "La solicitud se ha eliminado correctamente";
			}
			else
			{
				$data['mensaje']['tipo']="error";
				$data['mensaje']['mensaje'] = "Error: No se ha podido eliminar";
			}
			$estudiante = $this->estudiante->getBy("ced_estudiante",$this->session->userdata("cedula"));
			$solicitudes = $this->solicitudayuda->getBy("ayuda_solicitud.id_estudiante",$estudiante[0]->id);

			$data['solicitudes'] = $solicitudes;

			$this->salida($data);
		
		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}


	function salida($data = null, $vista = "solicitudAyuda_view")
	{
		$estilos['css_estilos'][0] 	= base_url()."css/themes/flexigrid/css/flexigrid.css";
		$jss['js_estilos'][0] 	= base_url()."css/themes/flexigrid/js/flexigrid.js";
		$data['titulo'] = "Solicitud de Ayuda";
		$this->load->view('header_view', $estilos);
		$this->load->view($vista,$data);
		$this->load->view('appScript_tpayudas_view');
		$this->load->view('footer_view',$jss);
		
	}


	public function comprobante($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudayuda->getBy("id_solicitud_ayuda", $id);
			$data['estudiante'] = $this->estudiante->getBy("id",$data["solicitud"][0]->id_estudiante);
			$data['tipos'] = $this->tiposAyuda->getAll();
			$data['titulo'] = "Solicitud de Ayuda";
			$this->load->view('header_reportes_view');
			$this->load->view("comprobanteAyuda_view",$data);

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	public function comprobante_pdf($segment = null)
	{

		try {
			if (!empty($segment)) {
				$id = $segment;
			}
			else
			{
				$id = $this->uri->segment(3);
			}
			$data['solicitud'] = $this->solicitudayuda->getBy("id_solicitud_ayuda", $id);
			// print_r($data);
			$data['solicitud'][0]->fech_solicitud=$this->date2normal($data['solicitud'][0]->fech_solicitud);

			$data['estudiante'] = $this->estudiante->getBy("id",$data["solicitud"][0]->id_estudiante);
			$data['tipos'] = $this->tiposAyuda->getAll();
			$data['titulo'] = "Solicitud de Ayuda";

			// mb_internal_encoding("UTF-8");
			$this->load->library('pdf');
			$this->pdf->load_view("comprobanteAyudaPDF_view",$data);
			$this->pdf->render();
			$this->pdf->stream('solicAyuda_'.$data['solicitud'][0]->nro_solicitud_a.'.pdf');

		} catch(Exception $e) {
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}

	}

	
}