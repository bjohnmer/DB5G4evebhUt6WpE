<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Urlprin
{
  public function getUrl()
  {
    $url = base_url();
    $index = index_page();
    if (!empty($index)) 
    {
      $url .= $index."/";
    }
    return $url;
  }
}