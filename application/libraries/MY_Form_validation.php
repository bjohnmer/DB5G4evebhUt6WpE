<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

class MY_Form_validation extends CI_Form_validation
{
	
	function __construct() 
	{
		parent::__construct();
	}

	/**
	 * Alpha-space with spaces and spanish chars
	 *
	 * @access	public
	 * @param	string
	 * @return	bool
	 */
	public function alpha_space($str)
	{
		return ( ! preg_match("/^([a-z������ ])+$/i", $str)) ? FALSE : TRUE;
	}

	// --------------------------------------------------------------------

	/**
	 * Alpha with spanish chars
	 *
	 * @access	public
	 * @param	string
	 * @return	bool
	 */
	public function alpha($str)
	{
		return ( ! preg_match("/^([a-z������])+$/i", $str)) ? FALSE : TRUE;
	}

	// --------------------------------------------------------------------
}