$(function() {
    //Funciones de Validaciones
    //$( "#dialog:ui-dialog" ).dialog( "destroy" );
    
    var ci = $( "#ci" ),
      fechanac = $( "#fechanac" ),
      usr = $( "#usr" ),
      pass = $( "#pass" ),
      allFields = $( [] ).add( ci ).add( fechanac ).add( usr ).add( pass ),
      tips = $( ".validateTips" );
    function updateTips( t ) {
      tips
        .text( t )
        .addClass( "ui-state-highlight" );
      setTimeout(function() {
        tips.removeClass( "ui-state-highlight", 1500 );
      }, 500 );
    }
    function checkLength( o, n, min, max ) {
      if ( o.val().length > max || o.val().length < min ) {
        o.addClass( "ui-state-error" );
        updateTips( "Longitud de " + n + " debe estar entre " +
          min + " y " + max + "." );
        return false;
      } else {
        return true;
      }
    }
    function checkRegexp( o, regexp, n ) {
      if ( !( regexp.test( o.val() ) ) ) {
        o.addClass( "ui-state-error" );
        updateTips( n );
        return false;
      } else {
        return true;
      }
    }
    
    $( "#registro" )
      .click(function() {
        $( "#form_registro" ).dialog( "open" );
      });
    $(function() {
      $( "#datepicker" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "yy-mm-dd",
        yearRange: "-60:"
      });
      $( "#fech_salida, #fech_regreso" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
        yearRange: "-60:"
      });

      $( "#fech_ingreso, #fech_nac_fam" ).datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: "dd/mm/yy",
        yearRange: "-60:",
        maxDate: new Date()
      });

    });

});


// var bValid = true;
          // allFields.removeClass( "ui-state-error" );
          // //Validadacion de logitud
          // bValid = bValid && checkLength( ci, "Cedula", 7, 8 );
          // //bValid = bValid && checkLength( fechanac, "Fecha de Nacimiento", 10, 10 );
          // bValid = bValid && checkLength( usr, "Usuario", 3, 16 );
          // bValid = bValid && checkLength( pass, "Clave", 5, 16 );
          // bValid = bValid && checkRegexp( ci, /^([0-9])+$/, "Debe colocar solo numeros, Ej:12865231" );
          // //Valicacion de expresion
          // //bValid = bValid && checkRegexp( fechanac, /^\d{1,2}\/\d{1,2}\/\d{2,4}$/, "Formato de Fecha incorrecto" );
          // //bValid = bValid && checkRegexp( fechanac, /^\d{2}\/\d{2}\/\d{4}$/, "Formato de Fecha incorrecto" );
          // bValid = bValid && checkRegexp( usr, /^[a-z]([0-9a-z_])+$/i, "Usuario debe contener: a-z, 0-9, guion( _ ) y comenzar con una letra." );
          // bValid = bValid && checkRegexp( pass, /^([0-9a-zA-Z])+$/, "Clave solo Permite : a-z 0-9" );
          // if ( bValid ) {
            // document.form_registro1.submit();
            // $( this ).dialog( "close" );